from typing import Iterable

import psycopg2
import psycopg2.errors
import pytest
import testing.postgresql
from sqlalchemy import schema

from polyteia.table_cache import (
    MARKER_SCHEMA,
    CacheTableEntry,
    MetadatabaseConnection,
    MetadatabaseTransaction,
)
from polyteia.transaction import PsycopgConnection, PsycopgTransaction


@pytest.fixture
def psycopg_transaction() -> Iterable[PsycopgTransaction]:
    with testing.postgresql.Postgresql() as postgresql:
        db_connection = PsycopgConnection(psycopg2.connect(**postgresql.dsn()))
        db_connection.ensure_schema_exists("extract")
        with db_connection.transaction() as transaction:
            yield transaction


@pytest.fixture
def psycopg_connection() -> Iterable[PsycopgConnection]:
    with testing.postgresql.Postgresql() as postgresql:
        yield PsycopgConnection(psycopg2.connect(**postgresql.dsn()))


@pytest.fixture
def metadatabase_transaction() -> Iterable[MetadatabaseTransaction]:
    with testing.postgresql.Postgresql() as postgresql:
        db_connection = MetadatabaseConnection(**postgresql.dsn())
        with db_connection._engine.begin() as connection:
            connection.execute(schema.CreateSchema(MARKER_SCHEMA, quote=True))
            CacheTableEntry.__table__.create(bind=connection, checkfirst=True)
        with db_connection.transaction() as transaction:
            yield transaction


@pytest.fixture
def metadatabase_connection() -> Iterable[MetadatabaseConnection]:
    with testing.postgresql.Postgresql() as postgresql:
        yield MetadatabaseConnection(**postgresql.dsn())
