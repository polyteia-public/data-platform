import datetime

import pytz
from sqlalchemy import schema

from polyteia.parser_rule import CsvParserRule
from polyteia.table_cache import (
    MARKER_SCHEMA,
    CacheTableEntry,
    MetadatabaseConnection,
    MetadatabaseTransaction,
)

DUMMY_CSV_PARSER_RULE = CsvParserRule()


def test_table_is_up_to_date_after_update(metadatabase_transaction: MetadatabaseTransaction):
    exported_at = datetime.datetime(2022, 2, 28, tzinfo=pytz.UTC)
    last_modified = datetime.datetime(2022, 2, 27, tzinfo=pytz.UTC)
    file_e_tag = "a_dummy_e_tag"
    metadatabase_transaction.update_table_cache_entry(
        "some_table", exported_at, DUMMY_CSV_PARSER_RULE, last_modified, file_e_tag
    )

    assert (
        metadatabase_transaction.table_is_up_to_date(
            "some_table", exported_at, DUMMY_CSV_PARSER_RULE, last_modified, file_e_tag
        )
        is True
    )


def test_table_is_not_up_to_date_for_later_timestamp_and_different_e_tag(
    metadatabase_transaction: MetadatabaseTransaction,
):
    exported_at = datetime.datetime(2022, 2, 28, tzinfo=pytz.UTC)
    last_modified = datetime.datetime(2022, 2, 27, tzinfo=pytz.UTC)
    old_file_e_tag = "a_dummy_e_tag"
    new_file_e_tag = "b_dummy_e_tag"
    metadatabase_transaction.update_table_cache_entry(
        "some_table", exported_at, DUMMY_CSV_PARSER_RULE, last_modified, old_file_e_tag
    )

    assert (
        metadatabase_transaction.table_is_up_to_date(
            "some_table",
            exported_at,
            DUMMY_CSV_PARSER_RULE,
            last_modified + datetime.timedelta(hours=1),
            new_file_e_tag,
        )
        is False
    )


def test_table_is_not_up_to_date_for_missing_table_entry(metadatabase_transaction: MetadatabaseTransaction):
    exported_at = datetime.datetime(2022, 2, 28, tzinfo=pytz.UTC)
    last_modified = datetime.datetime(2022, 2, 27, tzinfo=pytz.UTC)
    file_e_tag = "a_dummy_e_tag"

    assert (
        metadatabase_transaction.table_is_up_to_date(
            "non_existing_table", exported_at, DUMMY_CSV_PARSER_RULE, last_modified, file_e_tag
        )
        is False
    )


def test_table_is_not_up_to_date_for_different_export_date_and_different_e_tag(
    metadatabase_transaction: MetadatabaseTransaction,
):
    exported_at = datetime.datetime(2022, 2, 28, tzinfo=pytz.UTC)
    last_modified = datetime.datetime(2022, 2, 27, tzinfo=pytz.UTC)
    old_file_e_tag = "a_dummy_e_tag"
    new_file_e_tag = "b_dummy_e_tag"
    metadatabase_transaction.update_table_cache_entry(
        "some_table", exported_at, DUMMY_CSV_PARSER_RULE, last_modified, old_file_e_tag
    )

    assert (
        metadatabase_transaction.table_is_up_to_date(
            "some_table",
            exported_at + datetime.timedelta(hours=1),
            DUMMY_CSV_PARSER_RULE,
            last_modified,
            new_file_e_tag,
        )
        is False
    )


def test_table_is_up_to_date_after_update_with_preexisting_entry(metadatabase_transaction: MetadatabaseTransaction):
    exported_at = datetime.datetime(2022, 2, 28, tzinfo=pytz.UTC)
    last_modified = datetime.datetime(2022, 2, 27, tzinfo=pytz.UTC)
    later_last_modified = last_modified + datetime.timedelta(days=1)
    old_file_e_tag = "a_dummy_e_tag"
    new_file_e_tag = "b_dummy_e_tag"

    metadatabase_transaction.update_table_cache_entry(
        "some_table", exported_at, DUMMY_CSV_PARSER_RULE, last_modified, old_file_e_tag
    )
    metadatabase_transaction.update_table_cache_entry(
        "some_table", exported_at, DUMMY_CSV_PARSER_RULE, later_last_modified, new_file_e_tag
    )

    assert (
        metadatabase_transaction.table_is_up_to_date(
            "some_table", exported_at, DUMMY_CSV_PARSER_RULE, later_last_modified, new_file_e_tag
        )
        is True
    )


def test_table_is_not_up_to_date_when_exported_at_does_not_match(metadatabase_transaction: MetadatabaseTransaction):
    exported_at = datetime.datetime(2022, 1, 2, tzinfo=pytz.UTC)
    earlier_exported_at = datetime.datetime(2022, 1, 1, tzinfo=pytz.UTC)
    last_modified = datetime.datetime(2022, 1, 1, tzinfo=pytz.UTC)
    file_e_tag = "a_dummy_e_tag"

    metadatabase_transaction.update_table_cache_entry(
        "some_table", exported_at, DUMMY_CSV_PARSER_RULE, last_modified, file_e_tag
    )

    assert (
        metadatabase_transaction.table_is_up_to_date(
            "some_table", earlier_exported_at, DUMMY_CSV_PARSER_RULE, last_modified, file_e_tag
        )
        is False
    )


def test_table_is_not_up_to_date_when_parser_rule_does_not_match(metadatabase_transaction: MetadatabaseTransaction):
    exported_at = datetime.datetime(2022, 1, 2, tzinfo=pytz.UTC)
    last_modified = datetime.datetime(2022, 1, 1, tzinfo=pytz.UTC)
    parser_rule = CsvParserRule(pattern="foo.csv")
    another_parser_rule = CsvParserRule(pattern="bar.csv")
    file_e_tag = "a_dummy_e_tag"

    metadatabase_transaction.update_table_cache_entry("some_table", exported_at, parser_rule, last_modified, file_e_tag)

    assert (
        metadatabase_transaction.table_is_up_to_date(
            "some_table", exported_at, another_parser_rule, last_modified, file_e_tag
        )
        is False
    )


def test_table_is_up_to_date_when_no_etag_and_last_modified_equal(metadatabase_transaction: MetadatabaseTransaction):
    exported_at = datetime.datetime(2022, 1, 2, tzinfo=pytz.UTC)
    last_modified = datetime.datetime(2022, 1, 1, tzinfo=pytz.UTC)
    file_e_tag = "a_dummy_e_tag"

    metadatabase_transaction.update_table_cache_entry(
        "some_table", exported_at, DUMMY_CSV_PARSER_RULE, last_modified, None
    )

    assert (
        metadatabase_transaction.table_is_up_to_date(
            "some_table", exported_at, DUMMY_CSV_PARSER_RULE, last_modified, file_e_tag
        )
        is True
    )


def test_table_is_up_to_date_when_no_etag_and_new_last_modified_is_earlier(
    metadatabase_transaction: MetadatabaseTransaction,
):
    exported_at = datetime.datetime(2022, 1, 2, tzinfo=pytz.UTC)
    last_modified = datetime.datetime(2022, 1, 1, tzinfo=pytz.UTC)
    file_e_tag = "a_dummy_e_tag"

    metadatabase_transaction.update_table_cache_entry(
        "some_table", exported_at, DUMMY_CSV_PARSER_RULE, last_modified, None
    )

    assert (
        metadatabase_transaction.table_is_up_to_date(
            "some_table",
            exported_at,
            DUMMY_CSV_PARSER_RULE,
            last_modified - datetime.timedelta(hours=1),
            file_e_tag,
        )
        is True
    )


def test_table_is_up_to_date_if_etag_same_and_new_last_modified_later(
    metadatabase_transaction: MetadatabaseTransaction,
):
    exported_at = datetime.datetime(2022, 1, 2, tzinfo=pytz.UTC)
    last_modified = datetime.datetime(2022, 1, 1, tzinfo=pytz.UTC)
    file_e_tag = "a_dummy_e_tag"

    metadatabase_transaction.update_table_cache_entry(
        "some_table", exported_at, DUMMY_CSV_PARSER_RULE, last_modified, file_e_tag
    )

    assert (
        metadatabase_transaction.table_is_up_to_date(
            "some_table",
            exported_at,
            DUMMY_CSV_PARSER_RULE,
            last_modified + datetime.timedelta(hours=1),
            file_e_tag,
        )
        is True
    )

    pass


def test_table_is_up_to_date_if_etag_same_and_last_modified_equal(metadatabase_transaction: MetadatabaseTransaction):
    exported_at = datetime.datetime(2022, 1, 2, tzinfo=pytz.UTC)
    last_modified = datetime.datetime(2022, 1, 1, tzinfo=pytz.UTC)
    file_e_tag = "a_dummy_e_tag"

    metadatabase_transaction.update_table_cache_entry(
        "some_table", exported_at, DUMMY_CSV_PARSER_RULE, last_modified, file_e_tag
    )

    assert (
        metadatabase_transaction.table_is_up_to_date(
            "some_table", exported_at, DUMMY_CSV_PARSER_RULE, last_modified, file_e_tag
        )
        is True
    )


def test_table_is_not_up_to_date_if_etag_different_and_last_modified_equal(
    metadatabase_transaction: MetadatabaseTransaction,
):
    exported_at = datetime.datetime(2022, 1, 2, tzinfo=pytz.UTC)
    last_modified = datetime.datetime(2022, 1, 1, tzinfo=pytz.UTC)
    old_file_e_tag = "a_dummy_e_tag"
    new_file_e_tag = "b_dummey_e_tag"

    metadatabase_transaction.update_table_cache_entry(
        "some_table", exported_at, DUMMY_CSV_PARSER_RULE, last_modified, old_file_e_tag
    )

    assert (
        metadatabase_transaction.table_is_up_to_date(
            "some_table", exported_at, DUMMY_CSV_PARSER_RULE, last_modified, new_file_e_tag
        )
        is False
    )


def test_table_is_not_up_to_date_if_etag_different_and_last_modified_different(
    metadatabase_transaction: MetadatabaseTransaction,
):
    exported_at = datetime.datetime(2022, 1, 2, tzinfo=pytz.UTC)
    last_modified = datetime.datetime(2022, 1, 1, tzinfo=pytz.UTC)
    old_file_e_tag = "a_dummy_e_tag"
    new_file_e_tag = "b_dummey_e_tag"

    metadatabase_transaction.update_table_cache_entry(
        "some_table", exported_at, DUMMY_CSV_PARSER_RULE, last_modified, old_file_e_tag
    )

    assert (
        metadatabase_transaction.table_is_up_to_date(
            "some_table",
            exported_at,
            DUMMY_CSV_PARSER_RULE,
            last_modified + datetime.timedelta(hours=1),
            new_file_e_tag,
        )
        is False
    )


def test_table_is_not_up_to_date_when_no_etag_and_last_modified_different(
    metadatabase_transaction: MetadatabaseTransaction,
):
    exported_at = datetime.datetime(2022, 1, 2, tzinfo=pytz.UTC)
    last_modified = datetime.datetime(2022, 1, 1, tzinfo=pytz.UTC)
    file_e_tag = None

    metadatabase_transaction.update_table_cache_entry(
        "some_table", exported_at, DUMMY_CSV_PARSER_RULE, last_modified, file_e_tag
    )

    assert (
        metadatabase_transaction.table_is_up_to_date(
            "some_table",
            exported_at,
            DUMMY_CSV_PARSER_RULE,
            last_modified + datetime.timedelta(hours=1),
            file_e_tag,
        )
        is False
    )


def test_update_table_cache_entry_changes_are_actually_committed(metadatabase_connection: MetadatabaseConnection):
    exported_at = datetime.datetime(2022, 2, 28, tzinfo=pytz.UTC)
    last_modified = datetime.datetime(2022, 2, 27, tzinfo=pytz.UTC)
    file_e_tag = "a_dummy_e_tag"

    with metadatabase_connection._engine.begin() as connection:
        connection.execute(schema.CreateSchema(MARKER_SCHEMA, quote=True))
        CacheTableEntry.__table__.create(bind=connection, checkfirst=True)

    with metadatabase_connection.transaction() as metadatabase_transaction:
        metadatabase_transaction.update_table_cache_entry(
            "some_table", exported_at, DUMMY_CSV_PARSER_RULE, last_modified, file_e_tag
        )

    with metadatabase_connection.transaction() as metadatabase_transaction:
        assert (
            metadatabase_transaction.table_is_up_to_date(
                "some_table", exported_at, DUMMY_CSV_PARSER_RULE, last_modified, file_e_tag
            )
            is True
        )


def test_table_is_up_to_date_ensures_exported_at_is_utc(metadatabase_transaction: MetadatabaseTransaction):
    old_exported_at = datetime.datetime(2022, 1, 2, tzinfo=datetime.timezone.utc)
    new_exported_at = datetime.datetime(2022, 1, 2)
    last_modified = datetime.datetime(2022, 1, 1, tzinfo=datetime.timezone.utc)
    file_e_tag = '"297b99c94e4ac3939f84e7b74f345d72"'

    metadatabase_transaction.update_table_cache_entry(
        "some_table", old_exported_at, DUMMY_CSV_PARSER_RULE, last_modified, file_e_tag
    )

    assert (
        metadatabase_transaction.table_is_up_to_date(
            "some_table", new_exported_at, DUMMY_CSV_PARSER_RULE, last_modified, file_e_tag
        )
        is True
    )


def test_update_table_cache_entry_ensures_exported_at_is_utc(metadatabase_transaction: MetadatabaseTransaction):
    old_exported_at = datetime.datetime(2022, 1, 2)
    new_exported_at = datetime.datetime(2022, 1, 2, tzinfo=datetime.timezone.utc)
    last_modified = datetime.datetime(2022, 1, 1, tzinfo=datetime.timezone.utc)
    file_e_tag = '"297b99c94e4ac3939f84e7b74f345d72"'

    metadatabase_transaction.update_table_cache_entry(
        "some_table", old_exported_at, DUMMY_CSV_PARSER_RULE, last_modified, file_e_tag
    )

    assert (
        metadatabase_transaction.table_is_up_to_date(
            "some_table", new_exported_at, DUMMY_CSV_PARSER_RULE, last_modified, file_e_tag
        )
        is True
    )
