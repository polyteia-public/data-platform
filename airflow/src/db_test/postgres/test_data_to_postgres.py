import json
from datetime import datetime, timezone

import pytest

from polyteia.store import data_to_postgres
from polyteia.transaction import PsycopgTransaction, ViewMapsToDifferentTable

DATA = {"a": 1}
EXPORTED_AT = datetime.fromtimestamp(0, timezone.utc)


def test_data_to_postgres_writes_data_to_postgres(psycopg_transaction: PsycopgTransaction):
    data_to_postgres(psycopg_transaction, EXPORTED_AT, [json.dumps(DATA)], "some_table", None)

    with psycopg_transaction.cursor() as cursor:
        cursor.execute("SELECT data FROM extract.some_table")
        assert cursor.fetchone() == (DATA,)


def test_data_to_postgres_writes_exported_at_to_postgres(psycopg_transaction: PsycopgTransaction):
    data_to_postgres(psycopg_transaction, EXPORTED_AT, [json.dumps(DATA)], "some_table", None)

    with psycopg_transaction.cursor() as cursor:
        cursor.execute("SELECT exported_at FROM extract.some_table")
        assert cursor.fetchone() == (EXPORTED_AT,)


def test_data_to_postgres_writes_extracted_at_to_postgres(psycopg_transaction: PsycopgTransaction, mocker):
    extracted_at = datetime.fromtimestamp(0, timezone.utc)

    datetime_mock = mocker.patch("polyteia.transaction.datetime")
    extracted_at_without_tz = datetime.utcfromtimestamp(0)
    datetime_mock.utcnow.return_value = extracted_at_without_tz

    data_to_postgres(psycopg_transaction, EXPORTED_AT, [json.dumps(DATA)], "some_table", None)

    with psycopg_transaction.cursor() as cursor:
        cursor.execute("SELECT extracted_at FROM extract.some_table")
        assert cursor.fetchone() == (extracted_at,)


def test_data_to_postgres_creates_views(psycopg_transaction: PsycopgTransaction):
    data_to_postgres(psycopg_transaction, EXPORTED_AT, [json.dumps(DATA)], "some_table", None, ["some_view"])

    with psycopg_transaction.cursor() as cursor:
        cursor.execute("SELECT data FROM extract.some_view")
        assert cursor.fetchone() == (DATA,)


def test_data_to_postgres_succeeds_if_view_already_exists_and_has_same_select(psycopg_transaction: PsycopgTransaction):
    data_to_postgres(psycopg_transaction, EXPORTED_AT, [json.dumps(DATA)], "some_table", None, ["some_view"])

    data_to_postgres(psycopg_transaction, EXPORTED_AT, [json.dumps(DATA)], "some_table", None, ["some_view"])


def test_data_to_postgres_fails_if_view_already_exists_but_maps_to_different_table(
    psycopg_transaction: PsycopgTransaction,
):
    data_to_postgres(psycopg_transaction, EXPORTED_AT, [json.dumps(DATA)], "some_other_table", None, ["some_view"])

    with pytest.raises(ViewMapsToDifferentTable):
        data_to_postgres(psycopg_transaction, EXPORTED_AT, [json.dumps(DATA)], "some_table", None, ["some_view"])


def test_data_to_postgres_replaces_existing_table_with_view_name_with_view(psycopg_transaction: PsycopgTransaction):
    data_to_postgres(psycopg_transaction, EXPORTED_AT, [json.dumps({"a": 2})], None, "some_view")

    data_to_postgres(psycopg_transaction, EXPORTED_AT, [json.dumps(DATA)], "some_table", None, ["some_view"])

    with psycopg_transaction.cursor() as cursor:
        cursor.execute("SELECT data FROM extract.some_view")
        assert cursor.fetchone() == (DATA,)
