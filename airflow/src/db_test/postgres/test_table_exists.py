from polyteia.transaction import PsycopgTransaction


def test_table_exists_returns_true_if_table_exists(psycopg_transaction: PsycopgTransaction):
    with psycopg_transaction.cursor() as cursor:
        cursor.execute("CREATE TABLE some_table (some_column varchar(255))")

    assert psycopg_transaction.table_exists("public", "some_table")


def test_table_exists_returns_false_if_table_does_not_exist(psycopg_transaction: PsycopgTransaction):
    with psycopg_transaction.cursor() as cursor:
        cursor.execute("CREATE TABLE some_table (some_column varchar(255))")

    assert not psycopg_transaction.table_exists("public", "some_other_table")


def test_table_exists_returns_false_if_view_with_table_name_exists(psycopg_transaction: PsycopgTransaction):
    with psycopg_transaction.cursor() as cursor:
        cursor.execute("CREATE TABLE some_table (some_column varchar(255))")
        cursor.execute("CREATE VIEW some_view AS SELECT * FROM some_table")

    assert not psycopg_transaction.table_exists("public", "some_view")
