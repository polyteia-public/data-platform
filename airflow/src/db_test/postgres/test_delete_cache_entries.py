import datetime

import pytz

from polyteia.parser_rule import CsvParserRule
from polyteia.table_cache import MetadatabaseTransaction

KWARGS = {
    "table_name": "some_table_name",
    "exported_at": datetime.datetime(2022, 1, 2, tzinfo=pytz.UTC),
    "parser_rule": CsvParserRule(),
    "file_last_modified": datetime.datetime(2022, 1, 1, tzinfo=pytz.UTC),
    "file_e_tag": "some_e_tag",
}
OTHER_EXPORTED_AT = (KWARGS["exported_at"] + datetime.timedelta(hours=1)).astimezone(pytz.UTC)


def test_deletes_entry_if_table_name_equal_and_exported_at_not_equal(metadatabase_transaction: MetadatabaseTransaction):
    metadatabase_transaction.update_table_cache_entry(**KWARGS)

    metadatabase_transaction.delete_cache_entries(KWARGS["table_name"], OTHER_EXPORTED_AT)

    assert metadatabase_transaction.table_is_up_to_date(**KWARGS) is False


def test_deletes_entry_if_table_name_equal_and_exported_at_is_none(metadatabase_transaction: MetadatabaseTransaction):
    metadatabase_transaction.update_table_cache_entry(**KWARGS)

    metadatabase_transaction.delete_cache_entries(KWARGS["table_name"], None)

    assert metadatabase_transaction.table_is_up_to_date(**KWARGS) is False


def test_does_not_delete_entry_if_table_name_equal_and_exported_at_equal(
    metadatabase_transaction: MetadatabaseTransaction,
):
    metadatabase_transaction.update_table_cache_entry(**KWARGS)

    metadatabase_transaction.delete_cache_entries(KWARGS["table_name"], KWARGS["exported_at"])

    assert metadatabase_transaction.table_is_up_to_date(**KWARGS) is True


def test_does_not_delete_entry_if_table_name_not_equal_and_exported_at_equal(
    metadatabase_transaction: MetadatabaseTransaction,
):
    metadatabase_transaction.update_table_cache_entry(**KWARGS)

    metadatabase_transaction.delete_cache_entries("other_table_name", KWARGS["exported_at"])

    assert metadatabase_transaction.table_is_up_to_date(**KWARGS) is True


def test_does_not_delete_entry_if_table_name_not_equal_and_exported_at_not_equal(
    metadatabase_transaction: MetadatabaseTransaction,
):
    metadatabase_transaction.update_table_cache_entry(**KWARGS)

    metadatabase_transaction.delete_cache_entries("other_table_name", OTHER_EXPORTED_AT)

    assert metadatabase_transaction.table_is_up_to_date(**KWARGS) is True


def test_does_not_delete_entry_if_table_name_not_equal_and_exported_at_is_none(
    metadatabase_transaction: MetadatabaseTransaction,
):
    metadatabase_transaction.update_table_cache_entry(**KWARGS)

    metadatabase_transaction.delete_cache_entries("other_table_name", None)

    assert metadatabase_transaction.table_is_up_to_date(**KWARGS) is True


def test_delete_cache_entries_ensures_exported_at_is_utc(metadatabase_transaction: MetadatabaseTransaction):
    old_exported_at = datetime.datetime(2022, 1, 2, tzinfo=datetime.timezone.utc)
    new_exported_at = datetime.datetime(2022, 1, 2)
    last_modified = datetime.datetime(2022, 1, 1, tzinfo=datetime.timezone.utc)
    file_e_tag = '"297b99c94e4ac3939f84e7b74f345d72"'

    metadatabase_transaction.update_table_cache_entry(
        "some_table", old_exported_at, CsvParserRule(), last_modified, file_e_tag
    )

    metadatabase_transaction.update_table_cache_entry(
        "some_table", old_exported_at + datetime.timedelta(hours=1), CsvParserRule(), last_modified, file_e_tag
    )

    metadatabase_transaction.delete_cache_entries("some_table", new_exported_at)

    assert (
        metadatabase_transaction.table_is_up_to_date(
            "some_table", old_exported_at, CsvParserRule(), last_modified, file_e_tag
        )
        is True
    )
