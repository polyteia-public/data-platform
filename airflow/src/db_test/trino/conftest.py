import os
import subprocess
from typing import Iterable

import boto3
import pytest
import sqlalchemy.engine
from tenacity import retry, stop_after_delay, wait_fixed

from polyteia.transaction import TrinoConnection, TrinoTransaction


@pytest.fixture
def trino_transaction(trino_connection: TrinoConnection) -> Iterable[TrinoTransaction]:
    with trino_connection.transaction() as transaction:
        yield transaction


@pytest.fixture
def trino_connection(trino_sqlalchemy_engine: sqlalchemy.engine.Engine) -> Iterable[TrinoConnection]:
    with trino_sqlalchemy_engine.begin() as connection:
        connection.execute("CREATE SCHEMA some_schema WITH (location = 's3://datalake/some_schema')")
    try:
        yield TrinoConnection(
            trino_sqlalchemy_engine,
            "datalake",
            "minio",
            "minio123",
            f"http://{os.getenv('DP_TEST_TRINO_HOST', 'localhost')}:9000",
            "some_schema",
        )
    finally:
        with trino_sqlalchemy_engine.begin() as connection:
            for catalog, schema in connection.execute("SELECT * FROM information_schema.schemata").fetchall():
                if schema not in ["information_schema", "default"]:
                    # drop tables and views manually because trino DROP SCHEMA does not support CASCADE
                    drop_tables_and_views(connection, schema=schema)
                    connection.execute(f'DROP SCHEMA "{schema}"')
                    s3 = boto3.Session("minio", "minio123").resource(
                        "s3", endpoint_url=f"http://{os.getenv('DP_TEST_TRINO_HOST', 'localhost')}:9000"
                    )
                    s3.Bucket("datalake").objects.all().delete()


def drop_tables_and_views(connection: sqlalchemy.engine.Connection, schema: str) -> None:
    for table_name, table_type in connection.execute(
        f"SELECT table_name, table_type " f"FROM information_schema.tables " f"WHERE table_schema = '{schema}'"
    ).fetchall():
        if table_type == "VIEW":
            connection.execute(f'DROP VIEW "{schema}"."{table_name}"')
        else:
            connection.execute(f'DROP TABLE "{schema}"."{table_name}"')


@pytest.fixture(scope="session")
def trino_sqlalchemy_engine() -> Iterable[sqlalchemy.engine.Engine]:
    docker_compose_file = os.path.join(os.path.dirname(__file__), "../../../../ops/docker-compose-trino.yaml")
    subprocess.run(["docker", "compose", "-f", docker_compose_file, "up", "-d"])

    def log_attempt_number(retry_state):
        print(f"Retrying: {retry_state.attempt_number}...")

    @retry(wait=wait_fixed(1), stop=stop_after_delay(120), after=log_attempt_number)
    def try_to_connect_to_trino_until_available(engine):
        with engine.connect():
            pass

    engine = sqlalchemy.create_engine(f"trino://admin@{os.getenv('DP_TEST_TRINO_HOST', 'localhost')}:8080/hive")
    try:
        try_to_connect_to_trino_until_available(engine)
        yield engine
    except Exception:
        logs_process = subprocess.Popen(
            ["docker", "compose", "-f", docker_compose_file, "logs"], stdout=subprocess.PIPE, stderr=subprocess.STDOUT
        )
        for line in iter(logs_process.stdout.readline, b""):
            print(f"compose: {line.decode('utf-8').rstrip()}")
        raise
    finally:
        if not os.getenv("DP_TEST_KEEP_TRINO_RUNNING"):
            subprocess.run(["docker", "compose", "-f", docker_compose_file, "down"])
