from datetime import datetime

import pytest
import pytz
from dateutil.tz import tzoffset

from polyteia.transaction import TrinoConnection, TrinoTransaction


def test_table_exists_returns_true_if_table_exists(trino_transaction: TrinoTransaction):
    trino_transaction.connection.execute("CREATE TABLE some_schema.some_schema__some_table (some_column varchar(255))")

    assert trino_transaction.table_exists("some_schema", "some_table")


def test_table_exists_returns_false_if_table_does_not_exist(trino_transaction: TrinoTransaction):
    trino_transaction.connection.execute("CREATE TABLE some_schema.some_table (some_column varchar(255))")

    assert not trino_transaction.table_exists("some_schema", "some_other_table")


def test_table_exists_returns_false_if_view_with_table_name_exists(trino_transaction: TrinoTransaction):
    trino_transaction.connection.execute("CREATE TABLE some_schema.some_table (some_column varchar(255))")
    trino_transaction.connection.execute("CREATE VIEW some_schema.some_view AS SELECT * FROM some_schema.some_table")

    assert not trino_transaction.table_exists("some_schema", "some_view")


def test_drop_table_raises_not_implemented_error(trino_transaction: TrinoTransaction):
    with pytest.raises(NotImplementedError):
        trino_transaction.drop_table("some_schema", "some_table")


def test_ensure_schema_exists_does_not_fail_if_schema_already_exists(trino_connection: TrinoConnection):
    with trino_connection.transaction() as trino_transaction:
        trino_transaction.connection.execute("CREATE SCHEMA some_other_schema")

    trino_connection.ensure_schema_exists("some_other_schema")


def test_ensure_extract_table_exists_creates_table_if_it_does_not_exist(trino_transaction: TrinoTransaction):
    trino_transaction.ensure_extract_table_exists("some_schema", "some_table")

    assert bool(
        trino_transaction.connection.execute(
            "SELECT true FROM information_schema.tables WHERE table_schema = 'some_schema' AND table_name = 'some_schema__some_table'"
        ).fetchone()
    )


def test_ensure_extract_table_exists_creates_table_with_correct_columns(trino_transaction: TrinoTransaction):
    trino_transaction.ensure_extract_table_exists("some_schema", "some_table")

    assert trino_transaction.connection.execute(
        "SELECT column_name, data_type FROM information_schema.columns WHERE "
        "table_schema = 'some_schema' AND "
        "table_name = 'some_schema__some_table'"
    ).fetchall() == [
        ("extracted_at", "timestamp(3)"),
        ("exported_at", "timestamp(3)"),
        ("data", "varchar"),
    ]


def test_ensure_extract_table_exists_does_not_fail_if_table_already_exists(trino_transaction: TrinoTransaction):
    trino_transaction.connection.execute("CREATE TABLE some_schema.some_table (some_column varchar(255))")

    trino_transaction.ensure_extract_table_exists("some_schema", "some_table")


def test_insert_or_replace_extract_converts_exported_at_to_utc_and_removes_tz(
    trino_transaction: TrinoTransaction, mocker
):
    exported_at = datetime.fromtimestamp(0, tz=pytz.UTC)
    exported_at_with_tz = exported_at.astimezone(tzoffset(None, 3600))
    exported_at_without_tz = exported_at.replace(tzinfo=None)
    extracted_at = datetime.fromtimestamp(1)
    mocker.patch("polyteia.transaction.utcnow", return_value=extracted_at)
    trino_transaction.ensure_extract_table_exists("some_schema", "some_table")

    trino_transaction.insert_or_replace_extract_records(
        "some_schema", "some_table", ['["some_value"]'], exported_at_with_tz, incremental=False
    )

    assert trino_transaction.connection.execute("SELECT * FROM some_schema.some_schema__some_table").fetchall() == [
        (extracted_at, exported_at_without_tz, '["some_value"]')
    ]


def test_insert_or_replace_extract_records_inserts_record(trino_transaction: TrinoTransaction, mocker):
    exported_at = datetime.fromtimestamp(0)
    extracted_at = datetime.fromtimestamp(1)
    mocker.patch("polyteia.transaction.utcnow", return_value=extracted_at)
    trino_transaction.ensure_extract_table_exists("some_schema", "some_table")

    trino_transaction.insert_or_replace_extract_records(
        "some_schema", "some_table", ['["some_value"]'], exported_at, incremental=False
    )

    assert trino_transaction.connection.execute("SELECT * FROM some_schema.some_schema__some_table").fetchall() == [
        (extracted_at, exported_at, '["some_value"]')
    ]


def test_create_or_replace_view_raises_not_implemented_error(trino_transaction: TrinoTransaction):
    with pytest.raises(NotImplementedError):
        trino_transaction.create_or_replace_view("extract", "some_table", "some_view")


def test_verify_that_view_maps_to_expected_table_if_it_exists_raises_not_implemented_error(
    trino_transaction: TrinoTransaction,
):
    with pytest.raises(NotImplementedError):
        trino_transaction.verify_that_view_maps_to_expected_table_if_it_exists(
            "some_schema", "some_view", "some_other_table"
        )
