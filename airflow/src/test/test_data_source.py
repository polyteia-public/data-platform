import jsons

from polyteia.data_source import DataSource, DataSources
from polyteia.parser_rule import CsvParserRule

CSV_PARSER_RULE = CsvParserRule(pattern="*.csv", encoding="utf-8")


def test_deserialize_data_sources():
    data_sources_serialized = jsons.dump(
        [DataSource(id="id", s3prefix="some/prefix", parser_rules=[CSV_PARSER_RULE], timestamp="some_timestamp")]
    )

    assert jsons.load(data_sources_serialized, DataSources) == [
        DataSource(
            id="id",
            s3prefix="some/prefix",
            parser_rules=[
                CsvParserRule(
                    pattern="*.csv", has_headers=True, separator=";", encoding="utf-8", type="csv_parser_rule"
                )
            ],
            timestamp="some_timestamp",
        )
    ]
