from unittest.mock import ANY, call

import pytest

from polyteia.data_source import DataSource
from polyteia.dataset import DataSet, extract_files_for_data_source
from polyteia.namespace import NamespaceInfo

DUMMY_IMAGE = "needs_to_be_set_to_avoid_config_lookup"


def test_dataset_fails_with_non_unique_data_source_ids():
    with pytest.raises(ValueError) as exc:
        DataSet(
            "foo",
            "bar",
            dbt_runner_image=DUMMY_IMAGE,
            python_runner_image=DUMMY_IMAGE,
            data_sources=[DataSource(id="id1", s3prefix=""), DataSource(id="id1", s3prefix="")],
        )
    assert "DataSource ids need to be unique inside a DataSet" in str(exc.value)


def test_dataset_can_be_constructed_with_unique_data_source_ids():
    DataSet(
        "foo",
        "bar",
        dbt_runner_image=DUMMY_IMAGE,
        python_runner_image=DUMMY_IMAGE,
        data_sources=[DataSource(id="id1", s3prefix=""), DataSource(id="id2", s3prefix="")],
    )


def test_dataset_uses_customer_and_solution_as_table_namespace(mocker):
    source1 = DataSource(id="id1", s3prefix="")
    source2 = DataSource(id="id2", s3prefix="")
    extract_operator_mock = mocker.patch(
        "polyteia.dataset.extract_files_for_data_source", spec=extract_files_for_data_source
    )
    DataSet(
        "foo",
        "bar",
        dbt_runner_image=DUMMY_IMAGE,
        python_runner_image=DUMMY_IMAGE,
        data_sources=[source1, source2],
    ).make_dag()

    extract_operator_mock.assert_has_calls(
        [
            call(NamespaceInfo("foo_bar", ANY), source1, ANY, ANY, ANY),
            call(NamespaceInfo("foo_bar", ANY), source2, ANY, ANY, ANY),
        ]
    )


def test_dataset_uses_customer_solution_to_form_dag_id(mocker):
    source1 = DataSource(id="id1", s3prefix="")
    source2 = DataSource(id="id2", s3prefix="")
    extract_operator_mock = mocker.patch(
        "polyteia.dataset.extract_files_for_data_source", spec=extract_files_for_data_source
    )
    DataSet(
        "foo",
        "bar",
        dbt_runner_image=DUMMY_IMAGE,
        python_runner_image=DUMMY_IMAGE,
        data_sources=[source1, source2],
    ).make_dag()

    extract_operator_mock.assert_has_calls(
        [call(ANY, source1, ANY, "foo_bar", ANY), call(ANY, source2, ANY, "foo_bar", ANY)]
    )


def test_dataset_uses_customer_as_additional_namespace(mocker):
    source = DataSource(id="id1", s3prefix="")
    extract_operator_mock = mocker.patch(
        "polyteia.dataset.extract_files_for_data_source", spec=extract_files_for_data_source
    )
    DataSet(
        "foo",
        "bar",
        dbt_runner_image=DUMMY_IMAGE,
        python_runner_image=DUMMY_IMAGE,
        data_sources=[source],
        create_additional_namespaces=True,
    ).make_dag()

    extract_operator_mock.assert_has_calls([call(NamespaceInfo("foo_bar", ["foo"]), source, ANY, ANY, ANY)])


def test_dataset_does_not_use_customer_as_additional_namespace_if_flag_is_set(mocker):
    source = DataSource(id="id1", s3prefix="")
    extract_operator_mock = mocker.patch(
        "polyteia.dataset.extract_files_for_data_source", spec=extract_files_for_data_source
    )
    DataSet(
        "foo",
        "bar",
        dbt_runner_image=DUMMY_IMAGE,
        python_runner_image=DUMMY_IMAGE,
        data_sources=[source],
        create_additional_namespaces=False,
    ).make_dag()

    extract_operator_mock.assert_has_calls([call(NamespaceInfo("foo_bar"), source, ANY, ANY, ANY)])


def test_dataset_uses_customer_id_as_dag_id_if_no_solution_id_set():
    ds = DataSet(
        "foo",
        dbt_runner_image=DUMMY_IMAGE,
        python_runner_image=DUMMY_IMAGE,
        data_sources=[DataSource(id="id1", s3prefix="")],
    )

    assert ds.dag_id == "foo"


def test_dataset_cannot_be_constructed_without_solution_id_and_without_sources():
    with pytest.raises(ValueError, match="no explicit data sources, but also no solution id"):
        DataSet(
            "foo",
            dbt_runner_image=DUMMY_IMAGE,
            python_runner_image=DUMMY_IMAGE,
        )


def test_dataset_forwards_warehouse_id(mocker):
    extract_operator_mock = mocker.patch(
        "polyteia.dataset.extract_files_for_data_source", spec=extract_files_for_data_source
    )
    DataSet(
        "foo",
        dbt_runner_image=DUMMY_IMAGE,
        python_runner_image=DUMMY_IMAGE,
        data_sources=[DataSource(id="id1", s3prefix="")],
        warehouse="some_warehouse",
    ).make_dag()

    extract_operator_mock.assert_has_calls([call(ANY, ANY, "some_warehouse", ANY, ANY)])


def test_dataset_warehouse_id_defaults_to_postgres(mocker):
    extract_operator_mock = mocker.patch(
        "polyteia.dataset.extract_files_for_data_source", spec=extract_files_for_data_source
    )
    DataSet(
        "foo",
        dbt_runner_image=DUMMY_IMAGE,
        python_runner_image=DUMMY_IMAGE,
        data_sources=[DataSource(id="id1", s3prefix="")],
    ).make_dag()

    extract_operator_mock.assert_has_calls([call(ANY, ANY, "postgres", ANY, ANY)])
