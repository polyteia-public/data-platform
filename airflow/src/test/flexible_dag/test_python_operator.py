import pytest

from polyteia.dag import PolyteiaDAG, python
from polyteia.docker import make_docker_operator


def test_python_operator_passes_no_arguments_on_if_none_pased(mocker):
    make_docker_operator_mock = mocker.patch("polyteia.python.make_docker_operator", spec=make_docker_operator)

    with PolyteiaDAG("some_customer", python_runner_image="dummy"):
        python("some_filename")

    assert make_docker_operator_mock.call_args_list[0].kwargs["command"] == "python some_filename"


def test_python_operator_passes_arguments_on_if_passed_as_dict(mocker):
    make_docker_operator_mock = mocker.patch("polyteia.python.make_docker_operator", spec=make_docker_operator)

    with PolyteiaDAG("some_customer", python_runner_image="dummy"):
        python("some_filename", args={"some_key": "some_value"})

    assert make_docker_operator_mock.call_args_list[0].kwargs["command"] == "python some_filename --some_key some_value"


def test_python_operator_passes_arguments_on_if_passed_as_list(mocker):
    make_docker_operator_mock = mocker.patch("polyteia.python.make_docker_operator", spec=make_docker_operator)

    with PolyteiaDAG("some_customer", python_runner_image="dummy"):
        python("some_filename", args=["some_argument"])

    assert make_docker_operator_mock.call_args_list[0].kwargs["command"] == "python some_filename some_argument"


def test_python_operator_raises_if_arguments_are_not_dict_or_iterable(mocker):
    with PolyteiaDAG("some_customer", python_runner_image="dummy"):
        with pytest.raises(TypeError):
            python("some_filename", args=1)
