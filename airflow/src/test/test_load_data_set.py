import contextlib
import os
import textwrap
from tempfile import TemporaryDirectory
from typing import Iterable
from unittest import mock

from polyteia.extract_cli import (
    find_data_set_in_paths,
    find_polyteia_dag_in_paths,
)

DUMMY_IMAGE = "needs_to_be_set_to_avoid_config_lookup"


def test_find_data_set_loads_data_set_successfully():
    with temp_dag_file(
        """
        from polyteia.dataset import DataSet
        dag = DataSet("some_customer", "test").make_dag()
    """
    ) as dag_path:
        assert find_data_set_in_paths([dag_path], "some_customer", "test").dag_id == "some_customer_test"


def test_find_data_set_loads_data_set_without_solution_id():
    with temp_dag_file(
        """
        from polyteia.data_source import DataSource
        from polyteia.dataset import DataSet
        dag = DataSet("some_customer", data_sources=[DataSource(id="dummy", s3prefix="")]).make_dag()
    """
    ) as dag_path:
        assert find_data_set_in_paths([dag_path], "some_customer", None).dag_id == "some_customer"


def test_find_polyteia_dag_loads_dag_successfully():
    with temp_dag_file(
        """
        from polyteia.dag import PolyteiaDAG
        dag = PolyteiaDAG("some_customer", "test")
    """
    ) as dag_path:
        assert find_polyteia_dag_in_paths([dag_path], "some_customer", "test").dag_id == "some_customer_test"


def test_find_polyteia_dag_loads_polyteia_dag_without_solution_id():
    with temp_dag_file(
        """
        from polyteia.dag import PolyteiaDAG
        dag = PolyteiaDAG("some_customer")
    """
    ) as dag_path:
        assert find_polyteia_dag_in_paths([dag_path], "some_customer", None).dag_id == "some_customer"


@contextlib.contextmanager
def temp_dag_file(content: str) -> Iterable[str]:
    with TemporaryDirectory() as temp:
        dag_path = f"{temp}/dags/dag.py"
        os.makedirs(os.path.dirname(dag_path))
        with open(dag_path, mode="w") as f:
            f.write(textwrap.dedent(content))
        with mock.patch.dict(os.environ, {"EXTERNAL_PROJECT_DIR": temp}):
            yield dag_path
