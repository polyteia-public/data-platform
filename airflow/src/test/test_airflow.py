import os
import subprocess
from pathlib import Path

import pytest
from airflow.models import DagBag


@pytest.fixture(scope="session", autouse=True)
def do_something():
    subprocess.call("airflow db init", shell=True)


def test_import_dags():
    dagbag = DagBag(dag_folder=Path(os.getcwd()) / "airflow/src/polyteia/dags", include_examples=False, safe_mode=False)
    print(f"{dagbag.dag_ids=}")
    assert dagbag.import_errors == {}
