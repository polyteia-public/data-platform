from unittest.mock import ANY

from polyteia.dag import PolyteiaDAG, extract
from polyteia.data_source import DataSource
from polyteia.dataset import extract_files_for_data_source
from polyteia.namespace import NamespaceInfo


def test_extract_operator(mocker):
    customer = "some_customer"
    solution = "some_solution"
    data_source = DataSource(id="some_data_source", s3prefix="some_s3prefix")
    dataset_extract_mock = mocker.patch(
        "polyteia.dag.extract_files_for_data_source", spec=extract_files_for_data_source
    )

    with PolyteiaDAG(customer, solution):
        extract(data_source)

    dataset_extract_mock.assert_called_with(NamespaceInfo(f"{customer}_{solution}", ANY), data_source, ANY, ANY, ANY)


def test_extract_operator_passes_customer_as_additional_namespace(mocker):
    customer = "some_customer"
    solution = "some_solution"
    data_source = DataSource(id="some_data_source", s3prefix="some_s3prefix")
    dataset_extract_mock = mocker.patch(
        "polyteia.dag.extract_files_for_data_source", spec=extract_files_for_data_source
    )

    with PolyteiaDAG(customer, solution, create_additional_namespaces=True):
        extract(data_source)

    dataset_extract_mock.assert_called_with(
        NamespaceInfo(f"{customer}_{solution}", [customer]), data_source, ANY, ANY, ANY
    )


def test_extract_operator_does_not_pass_customer_as_additional_namespace_if_flag_is_set_to_false(mocker):
    customer = "some_customer"
    solution = "some_solution"
    data_source = DataSource(id="some_data_source", s3prefix="some_s3prefix")
    dataset_extract_mock = mocker.patch(
        "polyteia.dag.extract_files_for_data_source", spec=extract_files_for_data_source
    )

    with PolyteiaDAG(customer, solution, create_additional_namespaces=False):
        extract(data_source)

    dataset_extract_mock.assert_called_with(NamespaceInfo(f"{customer}_{solution}"), data_source, ANY, ANY, ANY)


def test_extract_operator_uses_warehouse_id_from_dag(mocker):
    customer = "some_customer"
    solution = "some_solution"
    data_source = DataSource(id="some_data_source", s3prefix="some_s3prefix")
    dataset_extract_mock = mocker.patch(
        "polyteia.dag.extract_files_for_data_source", spec=extract_files_for_data_source
    )

    with PolyteiaDAG(customer, solution, warehouse="some_warehouse"):
        extract(data_source)

    dataset_extract_mock.assert_called_with(ANY, ANY, "some_warehouse", ANY, ANY)


def test_extract_operator_defaults_warehouse_id_to_postgres(mocker):
    customer = "some_customer"
    solution = "some_solution"
    data_source = DataSource(id="some_data_source", s3prefix="some_s3prefix")
    dataset_extract_mock = mocker.patch(
        "polyteia.dag.extract_files_for_data_source", spec=extract_files_for_data_source
    )

    with PolyteiaDAG(customer, solution):
        extract(data_source)

    dataset_extract_mock.assert_called_with(ANY, ANY, "postgres", ANY, ANY)
