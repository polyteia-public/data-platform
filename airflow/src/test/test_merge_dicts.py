from polyteia.util import merge_dicts


def test_merge_dicts_uses_value_from_first_if_key_not_in_second():
    assert merge_dicts({"x": "a"}, {})["x"] == "a"


def test_merge_dicts_uses_value_from_second_if_key_not_in_first():
    assert merge_dicts({}, {"x": "b"})["x"] == "b"


def test_merge_dicts_uses_value_from_first_if_neither_are_dicts():
    assert merge_dicts({"x": "a"}, {"x": "b"})["x"] == "a"


def test_merge_dicts_uses_value_from_first_if_value_in_first_is_not_a_dict():
    assert merge_dicts({"x": "a"}, {"x": {"y": "b"}})["x"] == "a"


def test_merge_dicts_uses_value_from_first_if_value_in_second_is_not_a_dict():
    assert merge_dicts({"x": {"y": "a"}}, {"x": "b"})["x"] == {"y": "a"}


def test_merge_dicts_merges_values_if_both_are_dicts():
    assert merge_dicts({"x": {"y": "a"}}, {"x": {"z": "b"}})["x"] == {"y": "a", "z": "b"}
