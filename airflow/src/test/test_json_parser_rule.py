import json
import tempfile
from pathlib import Path

import pytest

from polyteia.parser_rule import JsonParserRule


def test_json_parser_rule_parses_dict_of_lists_of_dicts():
    with tempfile.TemporaryDirectory() as temp:
        file_name = "some_table_name.json"
        with open(f"{temp}/{file_name}", "w") as f:
            json.dump({"some_table_name": [{"some_key": "some_value"}]}, f)

        data = JsonParserRule().load_data_from_file(Path(temp), file_name)

        row = next(data)
        assert json.loads(row) == {"some_key": "some_value"}


def test_json_parser_rule_fails_if_top_level_dict_has_more_than_one_key():
    with tempfile.TemporaryDirectory() as temp:
        file_name = "foo.json"
        with open(f"{temp}/{file_name}", "w") as f:
            json.dump({"some_table_name": [], "some_other_table_name": []}, f)

        with pytest.raises(ValueError):
            # have to ask for first element to trigger lazy-loading generator
            next(JsonParserRule().load_data_from_file(Path(temp), file_name))
