import os
import tempfile
from pathlib import Path

from polyteia.store import load_data_from_csv


def test_load_data_with_header_from_csv():
    read_data = load_data_from_csv(
        Path(os.path.join("airflow", "src", "test", "testdata", "csv_with_header.csv")),
        has_headers=True,
        separator=";",
        encoding="utf-8",
    )
    assert list(read_data) == ['{"column1": "1", "column2": "2"}']


def test_load_data_with_header_from_csv_ignores_empty_leading_lines():
    read_data = load_data_from_csv(
        Path(os.path.join("airflow", "src", "test", "testdata", "csv_with_header_and_empty_starting_line.csv")),
        has_headers=True,
        separator=";",
        encoding="utf-8",
    )
    assert list(read_data) == ['{"column1": "1", "column2": "2"}']


def test_load_data_with_header_from_csv_ignores_empty_lines_after_header():
    read_data = load_data_from_csv(
        Path(os.path.join("airflow", "src", "test", "testdata", "csv_with_header_and_empty_line_after_header.csv")),
        has_headers=True,
        separator=";",
        encoding="utf-8",
    )
    assert list(read_data) == ['{"column1": "1", "column2": "2"}']


def test_load_data_without_header_from_csv():
    read_data = load_data_from_csv(
        Path(os.path.join("airflow", "src", "test", "testdata", "csv_without_header.csv")),
        has_headers=False,
        separator=";",
        encoding="utf-8",
    )
    assert list(read_data) == ['{"col1": "1", "col2": "2"}']


def test_load_data_without_header_from_csv_ignores_leading_empty_lines():
    read_data = load_data_from_csv(
        Path(os.path.join("airflow", "src", "test", "testdata", "csv_without_header_and_empty_starting_line.csv")),
        has_headers=False,
        separator=";",
        encoding="utf-8",
    )
    assert list(read_data) == ['{"col1": "1", "col2": "2"}']


def test_load_data_from_csv_keeps_umlaut_as_is_in_values_if_ensure_ascii_is_false():
    with tempfile.TemporaryDirectory() as temp:
        path = Path(temp) / "some_file.csv"
        with open(path, encoding="utf-8", mode="w") as f:
            f.write("ä")
        read_data = load_data_from_csv(
            path,
            has_headers=False,
            separator=";",
            encoding="utf-8",
            ensure_ascii=False,
        )
        assert list(read_data) == ['{"col1": "ä"}']


def test_load_data_from_csv_replaces_umlaut_with_escape_sequence_in_values_if_ensure_ascii_is_default():
    with tempfile.TemporaryDirectory() as temp:
        path = Path(temp) / "some_file.csv"
        with open(path, encoding="utf-8", mode="w") as f:
            f.write("ä")
        read_data = load_data_from_csv(
            path,
            has_headers=False,
            separator=";",
            encoding="utf-8",
        )
        assert list(read_data) == ['{"col1": "\\u00e4"}']


def test_load_data_from_csv_keeps_umlaut_as_is_in_keys_if_ensure_ascii_is_false():
    with tempfile.TemporaryDirectory() as temp:
        path = Path(temp) / "some_file.csv"
        with open(path, encoding="utf-8", mode="w") as f:
            f.writelines(["ä\n", "a\n"])
        read_data = load_data_from_csv(
            path,
            has_headers=True,
            separator=";",
            encoding="utf-8",
            ensure_ascii=False,
        )
        assert list(read_data) == ['{"ä": "a"}']


def test_load_data_from_csv_replaces_umlaut_with_escape_sequence_in_keys_if_ensure_ascii_is_default():
    with tempfile.TemporaryDirectory() as temp:
        path = Path(temp) / "some_file.csv"
        with open(path, encoding="utf-8", mode="w") as f:
            f.writelines(["ä\n", "a\n"])
        read_data = load_data_from_csv(
            path,
            has_headers=True,
            separator=";",
            encoding="utf-8",
        )
        assert list(read_data) == ['{"\\u00e4": "a"}']
