import os
from pathlib import Path
from tempfile import TemporaryDirectory
from unittest.mock import MagicMock

import yaml

from polyteia.dag import generate_dags

DAG_FILE_NAME = "some_dag"


def get_create_dag_mock():
    return MagicMock(return_value=MagicMock(dag_id="some_dag_id"))


def setup_bulk_config(config, temp):
    temp_dir = Path(temp)
    bulk_configs_dir = temp_dir / "bulk_configs"
    bulk_configs_dir.mkdir()
    os.environ["DP_BULK_CONFIGS_DIR"] = str(bulk_configs_dir)
    with open(bulk_configs_dir / f"{DAG_FILE_NAME}.yml", "w") as f:
        yaml.dump(config, f)


def test_generate_dags_works_for_minimal_case():
    create_dag_mock = get_create_dag_mock()
    with TemporaryDirectory() as temp:
        setup_bulk_config({"customers": ["some_customer"], "default": {}}, temp)

        dags = generate_dags(create_dag_mock, f"{DAG_FILE_NAME}.py")

        assert dags == {create_dag_mock.return_value.dag_id: create_dag_mock.return_value}


def test_generate_dags_uses_default_config_if_no_customer_config_provided():
    create_dag_mock = get_create_dag_mock()
    with TemporaryDirectory() as temp:
        setup_bulk_config({"customers": ["some_customer"], "default": {"x": "a"}}, temp)

        generate_dags(create_dag_mock, f"{DAG_FILE_NAME}.py")

        assert create_dag_mock.call_args_list[0].args[1] == {"x": "a"}


def test_generate_dags_uses_customer_config_if_provided():
    create_dag_mock = get_create_dag_mock()
    with TemporaryDirectory() as temp:
        setup_bulk_config({"customers": ["some_customer"], "default": {}, "some_customer": {"x": "a"}}, temp)

        generate_dags(create_dag_mock, f"{DAG_FILE_NAME}.py")

        assert create_dag_mock.call_args_list[0].args[1] == {"x": "a"}


def test_generate_dags_uses_customer_config_over_default_if_both_provided():
    create_dag_mock = get_create_dag_mock()
    with TemporaryDirectory() as temp:
        setup_bulk_config({"customers": ["some_customer"], "default": {"x": "a"}, "some_customer": {"x": "b"}}, temp)

        generate_dags(create_dag_mock, f"{DAG_FILE_NAME}.py")

        assert create_dag_mock.call_args_list[0].args[1] == {"x": "b"}


def test_generate_dags_merges_configs_if_both_provided():
    create_dag_mock = get_create_dag_mock()
    with TemporaryDirectory() as temp:
        setup_bulk_config({"customers": ["some_customer"], "default": {"x": "a"}, "some_customer": {"y": "b"}}, temp)

        generate_dags(create_dag_mock, f"{DAG_FILE_NAME}.py")

        assert create_dag_mock.call_args_list[0].args[1] == {"x": "a", "y": "b"}


def test_generate_dags_does_not_need_customer_list():
    create_dag_mock = get_create_dag_mock()
    with TemporaryDirectory() as temp:
        setup_bulk_config({"default": {}, "some_customer": {"x": "a"}}, temp)

        generate_dags(create_dag_mock, f"{DAG_FILE_NAME}.py")

        assert create_dag_mock.call_args_list[0].args[1] == {"x": "a"}


def test_generate_dags_does_not_need_default_config():
    create_dag_mock = get_create_dag_mock()
    with TemporaryDirectory() as temp:
        setup_bulk_config({"some_customer": {"x": "a"}}, temp)

        generate_dags(create_dag_mock, f"{DAG_FILE_NAME}.py")

        assert create_dag_mock.call_args_list[0].args[1] == {"x": "a"}
