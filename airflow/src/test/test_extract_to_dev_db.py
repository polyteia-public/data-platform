from datetime import datetime
from test.mocks import setup_extract_mocks
from unittest.mock import ANY

import jsons

from polyteia.data_source import DataSource
from polyteia.extract import extract_files_from_data_source
from polyteia.namespace import NamespaceInfo


def test_extract_files_to_dev_db_task(mocker):
    from polyteia.dags.extract_to_dev_db import extract_files_to_dev_db_task

    dag_namespace = "some_dag_namespace"
    warehouse = "some_warehouse"
    data_source_id = "some_data_source_id"
    s3prefix = "some_s3prefix"
    file_name = "some_file_name.csv"
    export_timestamp = datetime.fromtimestamp(0)
    data_source = DataSource(id=data_source_id, s3prefix=s3prefix)

    extract_mock = mocker.patch(
        "polyteia.dags.extract_to_dev_db.extract_files_from_data_source", spec=extract_files_from_data_source
    )
    setup_extract_mocks(mocker, s3prefix, file_name, export_timestamp)

    extract_files_to_dev_db_task(
        {
            "database": "some_database",
            "warehouse": warehouse,
            "dag_namespace": dag_namespace,
            "data_sources": jsons.dump([data_source]),
        }
    )

    extract_mock.assert_called_with(ANY, ANY, NamespaceInfo(dag_namespace, ANY), data_source)


def test_extract_files_to_dev_db_task_passes_on_additional_dag_namespaces(mocker):
    from polyteia.dags.extract_to_dev_db import extract_files_to_dev_db_task

    dag_namespace = "some_dag_namespace"
    additional_namespace = "some_additional_namespace"
    data_source_id = "some_data_source_id"
    s3prefix = "some_s3prefix"
    file_name = "some_file_name.csv"
    export_timestamp = datetime.fromtimestamp(0)
    data_source = DataSource(id=data_source_id, s3prefix=s3prefix)

    extract_mock = mocker.patch(
        "polyteia.dags.extract_to_dev_db.extract_files_from_data_source", spec=extract_files_from_data_source
    )
    setup_extract_mocks(mocker, s3prefix, file_name, export_timestamp)

    extract_files_to_dev_db_task(
        {
            "database": "some_database",
            "warehouse": "some_warehouse",
            "dag_namespace": dag_namespace,
            "data_sources": jsons.dump([data_source]),
            "additional_dag_namespaces": [additional_namespace],
        }
    )

    extract_mock.assert_called_with(ANY, ANY, NamespaceInfo(dag_namespace, [additional_namespace]), data_source)
