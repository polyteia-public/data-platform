from unittest.mock import ANY

import jsons

from polyteia.data_source import DataSource
from polyteia.extract_remotely import EXTRACT_DAG_ID, trigger_extract_dag


def test_trigger_extract_dag_works(mocker):
    database = "some_database"
    customer = "some_customer"
    dag_id = "some_dag_id"
    warehouse = "some_warehouse"
    data_source = DataSource(id="some_data_source", s3prefix="some_s3prefix")
    airflow_client_mock = mocker.MagicMock()
    airflow_client_mock.trigger_dag_run.return_value = 1

    dag_run_id = trigger_extract_dag(airflow_client_mock, database, warehouse, customer, dag_id, [data_source])

    airflow_client_mock.trigger_dag_run.assert_called_with(
        dag_id=EXTRACT_DAG_ID,
        conf={
            "database": database,
            "warehouse": warehouse,
            "dag_namespace": dag_id,
            "data_sources": (jsons.dump([data_source])),
            "additional_dag_namespaces": ANY,
        },
    )
    assert dag_run_id == 1


def test_trigger_extract_dag_passes_customer_as_additional_dag_namespace(mocker):
    database = "some_database"
    warehouse = "some_warehouse"
    customer = "some_customer"
    dag_id = "some_dag_id"
    data_source = DataSource(id="some_data_source", s3prefix="some_s3prefix")
    airflow_client_mock = mocker.MagicMock()
    airflow_client_mock.trigger_dag_run.return_value = 1

    dag_run_id = trigger_extract_dag(airflow_client_mock, database, warehouse, customer, dag_id, [data_source])

    airflow_client_mock.trigger_dag_run.assert_called_with(
        dag_id=EXTRACT_DAG_ID,
        conf={
            "database": database,
            "warehouse": warehouse,
            "dag_namespace": dag_id,
            "data_sources": (jsons.dump([data_source])),
            "additional_dag_namespaces": [customer],
        },
    )
    assert dag_run_id == 1


def test_trigger_extract_dag_does_not_pass_customer_as_additional_dag_namespace_if_flag_is_set_to_false(mocker):
    database = "some_database"
    warehouse = "some_warehouse"
    customer = "some_customer"
    dag_id = "some_dag_id"
    data_source = DataSource(id="some_data_source", s3prefix="some_s3prefix")
    airflow_client_mock = mocker.MagicMock()
    airflow_client_mock.trigger_dag_run.return_value = 1

    dag_run_id = trigger_extract_dag(
        airflow_client_mock, database, warehouse, customer, dag_id, [data_source], create_additional_namespaces=False
    )

    airflow_client_mock.trigger_dag_run.assert_called_with(
        dag_id=EXTRACT_DAG_ID,
        conf={
            "database": database,
            "warehouse": warehouse,
            "dag_namespace": dag_id,
            "data_sources": (jsons.dump([data_source])),
            "additional_dag_namespaces": (),
        },
    )
    assert dag_run_id == 1
