import boto3
import pytest
from moto import mock_s3


@pytest.fixture(autouse=True)
def prepare_environment(monkeypatch):
    monkeypatch.setenv("DP_DOCKER_NETWORK", "dummy")
    monkeypatch.setenv("DP_PYTHON_IMAGE", "dummy")
    monkeypatch.setenv("DP_REDASH_URL", "dummy")
    monkeypatch.setenv("DP_STORE_PGHOST", "dummy")
    monkeypatch.setenv("DP_STORE_PGPASSWORD", "dummy")
    monkeypatch.setenv("EXTERNAL_PROJECT_DIR", "test_projects/airflow-test-test")
    monkeypatch.setenv("DP_STORE_TRINO_HOST", "dummy")


@pytest.fixture
def aws_credentials(monkeypatch):
    monkeypatch.setenv("AWS_ACCESS_KEY_ID", "testing")
    monkeypatch.setenv("AWS_SECRET_ACCESS_KEY", "testing")
    monkeypatch.setenv("AWS_SECURITY_TOKEN", "testing")
    monkeypatch.setenv("AWS_SESSION_TOKEN", "testing")
    monkeypatch.setenv("DP_S3_BUCKET", "mybucket")
    monkeypatch.setenv("DP_S3_ENDPOINT_URL", "https://s3.test")


@pytest.fixture
def s3_client(aws_credentials):
    with mock_s3():
        conn = boto3.resource("s3", region_name="us-east-1")
        conn.create_bucket(Bucket="mybucket")
        conn.Object("mybucket", "test/test/LATEST").put(Body="test/test/2021-03-01")
        conn.Object("mybucket", "test/test/2021-02-01/test.txt").put(Body="foo bar")
        conn.Object("mybucket", "test/test/2021-03-01/test.txt").put(Body="foo bar")
        yield conn
