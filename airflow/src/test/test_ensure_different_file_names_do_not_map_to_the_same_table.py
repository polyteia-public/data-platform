import pytest

from polyteia.extract import (
    DuplicateTableNames,
    ensure_different_file_names_do_not_map_to_the_same_table,
)


def test_allows_duplicate_table_names_if_they_come_from_the_same_file_name():
    ensure_different_file_names_do_not_map_to_the_same_table(
        [""], [("a" * 63 + "a", None, None), ("a" * 63 + "a", None, None)], True
    )


def test_fails_for_duplicate_table_names_if_they_come_from_different_file_names():
    with pytest.raises(DuplicateTableNames):
        ensure_different_file_names_do_not_map_to_the_same_table(
            [""], [("a" * 63 + "a", None, None), ("a" * 63 + "b", None, None)], True
        )
