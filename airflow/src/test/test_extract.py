from datetime import datetime
from test.mocks import DEFAULT_JSON_DATA, setup_extract_mocks
from unittest.mock import ANY, MagicMock

import pytest

from polyteia.data_source import DataSource
from polyteia.extract import (
    AdditionalNameAlreadyTaken,
    DuplicateTableNames,
    extract_files_from_data_source,
)
from polyteia.namespace import NamespaceInfo
from polyteia.parser_rule import CsvParserRule
from polyteia.transaction import ViewMapsToDifferentTable


def test_extract_works_for_simple_case(mocker):
    dag_namespace = "some_dag_namespace"
    data_source_id = "some_data_source"
    s3prefix = "some_s3prefix"
    file_name = "some_file_name.csv"
    table_name = f"{dag_namespace}_{data_source_id}_{file_name[:-4]}"
    export_timestamp = datetime.fromtimestamp(0)

    _, data_to_postgres_mock, metadatabase_connection_mock = setup_extract_mocks(
        mocker, s3prefix, file_name, export_timestamp
    )

    extract_files_from_data_source(
        MagicMock(),
        metadatabase_connection_mock,
        NamespaceInfo(dag_namespace),
        DataSource(id=data_source_id, s3prefix=s3prefix),
    )

    assert data_to_postgres_mock.call_args.args[1] == export_timestamp
    assert list(data_to_postgres_mock.call_args.args[2]) == DEFAULT_JSON_DATA
    assert data_to_postgres_mock.call_args.args[3] == table_name
    assert data_to_postgres_mock.call_args.args[5] == []


def test_extract_truncates_long_table_names(mocker):
    dag_namespace = "some_dag_namespace"
    data_source_id = "some_data_source"
    s3prefix = "some_s3prefix"
    file_name = f"some_long_file_name_{'a'*8}.csv"
    truncated_table_name = f"{dag_namespace}_{data_source_id}_some_long_file_name_{'a'*7}"
    export_timestamp = datetime.fromtimestamp(0)

    _, data_to_postgres_mock, metadatabase_connection_mock = setup_extract_mocks(
        mocker, s3prefix, file_name, export_timestamp
    )

    extract_files_from_data_source(
        MagicMock(),
        metadatabase_connection_mock,
        NamespaceInfo(dag_namespace),
        DataSource(id=data_source_id, s3prefix=s3prefix),
    )

    data_to_postgres_mock.assert_called_with(ANY, export_timestamp, ANY, truncated_table_name, ANY, ANY)


def test_extract_fails_if_truncation_creates_duplicate_table_names(mocker):
    dag_namespace = "some_dag_namespace"
    data_source_id = "some_data_source"
    s3prefix = "some_s3prefix"
    file_name = f"some_long_file_name_{'a'*9}.csv"
    other_file_name = f"some_long_file_name_{'a'*9}_that_ends_differently.csv"
    export_timestamp = datetime.fromtimestamp(0)

    bucket_mock, data_to_postgres_mock, metadatabase_connection_mock = setup_extract_mocks(
        mocker, s3prefix, file_name, export_timestamp
    )
    bucket_mock.get_data_objects.return_value += [(other_file_name, export_timestamp, MagicMock())]

    with pytest.raises(DuplicateTableNames):
        extract_files_from_data_source(
            MagicMock(),
            metadatabase_connection_mock,
            NamespaceInfo(dag_namespace),
            DataSource(id=data_source_id, s3prefix=s3prefix),
        )


def test_extract_fails_if_truncation_creates_duplicate_view_names(mocker):
    dag_namespace = "some_dag_namespace"
    additional_dag_namespaces = ["some_additional_dag_namespace"]
    data_source_id = "some_data_source"
    s3prefix = "some_s3prefix"
    file_name = "some_long_file_name.csv"
    other_file_name = "some_long_file_name_that_ends_differently.csv"
    export_timestamp = datetime.fromtimestamp(0)

    bucket_mock, data_to_postgres_mock, metadatabase_connection_mock = setup_extract_mocks(
        mocker, s3prefix, file_name, export_timestamp
    )
    bucket_mock.get_data_objects.return_value += [(other_file_name, export_timestamp, MagicMock())]

    with pytest.raises(DuplicateTableNames):
        extract_files_from_data_source(
            MagicMock(),
            metadatabase_connection_mock,
            NamespaceInfo(dag_namespace, additional_dag_namespaces),
            DataSource(id=data_source_id, s3prefix=s3prefix),
        )


def test_extract_creates_views(mocker):
    dag_namespace = "some_dag_namespace"
    data_source_id = "some_data_source"
    s3prefix = "some_s3prefix"
    file_name = "some_file_name.csv"
    export_timestamp = datetime.fromtimestamp(0)
    table_name = f"{dag_namespace}_{data_source_id}_{file_name[:-4]}"
    view_namespace = "some_view_namespace"
    view_name = f"{view_namespace}_{data_source_id}_{file_name[:-4]}"

    bucket_mock, data_to_postgres_mock, metadatabase_connection_mock = setup_extract_mocks(
        mocker, s3prefix, file_name, export_timestamp
    )

    extract_files_from_data_source(
        MagicMock(),
        metadatabase_connection_mock,
        NamespaceInfo(dag_namespace, [view_namespace]),
        DataSource(id=data_source_id, s3prefix=s3prefix),
    )

    data_to_postgres_mock.assert_called_with(ANY, export_timestamp, ANY, table_name, ANY, [view_name])


def test_extract_passes_data_source_timestamp_to_bucket(mocker):
    dag_namespace = "some_dag_namespace"
    data_source_id = "some_data_source"
    s3prefix = "some_s3prefix"
    file_name = "some_file_name.csv"
    export_timestamp = datetime.fromtimestamp(0)
    s3_timestamp = "1970-01-01"

    bucket_mock, data_to_postgres_mock, metadatabase_connection_mock = setup_extract_mocks(
        mocker, s3prefix, file_name, export_timestamp
    )

    extract_files_from_data_source(
        MagicMock(),
        metadatabase_connection_mock,
        NamespaceInfo(dag_namespace),
        DataSource(id=data_source_id, s3prefix=s3prefix, timestamp=s3_timestamp),
    )

    bucket_mock.get_data_objects.assert_called_with(s3prefix, s3_timestamp, None)


def test_extract_raises_namespace_already_taken_when_exception_view_maps_to_different_table_is_caught(mocker):
    dag_namespace = "some_dag_namespace"
    additional_dag_namespaces = ["some_additional_dag_namespace"]
    data_source_id = "some_data_source"
    s3prefix = "some_s3prefix"
    file_name = "some_long_file_name.csv"
    export_timestamp = datetime.fromtimestamp(0)

    bucket_mock, data_to_postgres_mock, metadatabase_connection_mock = setup_extract_mocks(
        mocker, s3prefix, file_name, export_timestamp
    )
    data_to_postgres_mock.side_effect = ViewMapsToDifferentTable("", "")

    with pytest.raises(AdditionalNameAlreadyTaken):
        extract_files_from_data_source(
            MagicMock(),
            metadatabase_connection_mock,
            NamespaceInfo(dag_namespace, additional_dag_namespaces),
            DataSource(id=data_source_id, s3prefix=s3prefix),
        )


def test_extract_skips_file_if_no_parser_rule_matches(mocker):
    dag_namespace = "some_dag_namespace"
    data_source_id = "some_data_source"
    s3prefix = "some_s3prefix"
    file_name = "some_file_name.csv"
    export_timestamp = datetime.fromtimestamp(0)

    _, data_to_postgres_mock, metadatabase_connection_mock = setup_extract_mocks(
        mocker, s3prefix, file_name, export_timestamp
    )

    extract_files_from_data_source(
        MagicMock(),
        metadatabase_connection_mock,
        NamespaceInfo(dag_namespace),
        DataSource(id=data_source_id, s3prefix=s3prefix, parser_rules=[CsvParserRule("x")]),
    )

    data_to_postgres_mock.assert_not_called()


def test_extract_raises_exception_if_no_parser_rule_matches_and_flag_is_set(mocker):
    dag_namespace = "some_dag_namespace"
    data_source_id = "some_data_source"
    s3prefix = "some_s3prefix"
    file_name = "some_file_name.csv"
    export_timestamp = datetime.fromtimestamp(0)

    _, _, metadatabase_connection_mock = setup_extract_mocks(mocker, s3prefix, file_name, export_timestamp)

    with pytest.raises(ValueError):
        extract_files_from_data_source(
            MagicMock(),
            metadatabase_connection_mock,
            NamespaceInfo(dag_namespace),
            DataSource(
                id=data_source_id,
                s3prefix=s3prefix,
                parser_rules=[CsvParserRule("x")],
                fail_if_no_parser_rule_matches=True,
            ),
        )
