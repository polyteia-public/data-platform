from datetime import datetime
from tempfile import TemporaryDirectory

import boto3
import pytest
from dateutil.tz import tzoffset

from polyteia.bucket import Bucket, timestamp_from_prefix


@pytest.fixture
def s3_client_instance(s3_client):
    s3_client.create_bucket(Bucket="mybucket")
    s3_client_instance = Bucket()
    s3_client_instance.session = boto3.Session(
        aws_access_key_id="testing",
        aws_secret_access_key="testing",
    )
    s3_client_instance.s3 = s3_client_instance.session.resource("s3")
    s3_client_instance.bucket = s3_client_instance.s3.Bucket(name="mybucket")
    s3_client_instance.objects = s3_client_instance.bucket.objects

    return s3_client_instance


def test_get_latest_folder(s3_client_instance):
    latest = s3_client_instance.get_latest_folder("test/test")
    assert latest == ("test/test/2021-03-01/", datetime(2021, 3, 1, 0, 0))


def test_list_objects(s3_client_instance):
    objects = iter(s3_client_instance.list_objects("test/test"))
    assert next(objects).key == "test/test/2021-02-01/test.txt"


def test_objects_without_directories(s3_client_instance):
    for obj in s3_client_instance.objects_without_directories(Prefix="test/test"):
        assert obj.key[-1] != "/"


def test_get_object_contents(s3_client_instance):
    file_1_content = s3_client_instance.object_contents("test/test/2021-02-01/test.txt", encoding="utf-8")
    assert file_1_content == "foo bar"


def test_download_object(s3_client_instance):
    with TemporaryDirectory() as temp:
        with open(f"{temp}/download.txt", "wb") as data:
            s3_client_instance.download_object("test/test/2021-02-01/test.txt", data)
        with open(f"{temp}/download.txt") as f:
            assert f.read() == "foo bar"


def test_get_data_objects(s3_client_instance):
    objects = iter(s3_client_instance.get_data_objects("test/test"))
    filename, timestamp, s3_object = next(objects)
    assert filename == "test.txt"
    assert timestamp == datetime(2021, 3, 1, 0, 0)
    assert s3_object.key == "test/test/2021-03-01/test.txt"


def test_get_data_objects_with_incremental_extraction_date_range_and_open_date(s3_client_instance):
    objects = s3_client_instance.get_data_objects(
        s3prefix="test/test", fixed_timestamp=None, incremental_extract_date_range=(None, None), file_key=None
    )
    filename, timestamp, s3_object = next(objects)
    assert filename == "test.txt"
    assert timestamp == datetime(2021, 2, 1, 0, 0)
    assert s3_object.key == "test/test/2021-02-01/test.txt"


def test_get_data_objects_with_incremental_extraction_date_range_and_open_start_date(s3_client_instance):
    objects = s3_client_instance.get_data_objects(
        s3prefix="test/test", fixed_timestamp=None, incremental_extract_date_range=(None, "2023-01-01"), file_key=None
    )
    filename, timestamp, s3_object = next(objects)
    assert filename == "test.txt"
    assert timestamp == datetime(2021, 2, 1, 0, 0)
    assert s3_object.key == "test/test/2021-02-01/test.txt"


def test_get_data_objects_with_incremental_extraction_date_range_and_open_end_date(s3_client_instance):
    objects = s3_client_instance.get_data_objects(
        s3prefix="test/test", fixed_timestamp=None, incremental_extract_date_range=("2020-01-01", None), file_key=None
    )
    filename, timestamp, s3_object = next(objects)
    assert filename == "test.txt"
    assert timestamp == datetime(2021, 2, 1, 0, 0)
    assert s3_object.key == "test/test/2021-02-01/test.txt"


def test_get_data_objects_with_incremental_extraction_date_range(s3_client_instance):
    objects = s3_client_instance.get_data_objects(
        s3prefix="test/test",
        fixed_timestamp=None,
        incremental_extract_date_range=("2020-01-01", "2023-01-01"),
        file_key=None,
    )
    filename, timestamp, s3_object = next(objects)
    assert filename == "test.txt"
    assert timestamp == datetime(2021, 2, 1, 0, 0)
    assert s3_object.key == "test/test/2021-02-01/test.txt"


def test_get_data_objects_with_incremental_extraction_date_range_fails_if_date_range_values_are_not_date_string(
    s3_client_instance,
):
    with pytest.raises(ValueError):
        next(s3_client_instance.get_data_objects(s3prefix="test/test", incremental_extract_date_range=("123", "456")))


def test_get_data_objects_with_incremental_extraction_date_range_fails_if_date_range_has_only_one_element(
    s3_client_instance,
):
    with pytest.raises(ValueError):
        next(s3_client_instance.get_data_objects(s3prefix="test/test", incremental_extract_date_range=("123")))


def test_timestamp_from_prefix():
    assert timestamp_from_prefix("foo/bar/2022-01-18T06_01_29+0100") == datetime(
        2022, 1, 18, 6, 1, 29, tzinfo=tzoffset(None, 3600)
    )
