import json
from pathlib import Path
from unittest.mock import MagicMock

from polyteia.store import load_data_from_csv

DEFAULT_CSV_DATA = "a;b\n1;2"
DEFAULT_JSON_DATA = [json.dumps({"a": "1", "b": "2"})]


def setup_extract_mocks(mocker, s3prefix, file_name, export_timestamp, file_content=DEFAULT_CSV_DATA):
    connect_to_dev_db_mock = mocker.patch("polyteia.dags.extract_to_dev_db.connect_to_dev_db")
    db_connection = MagicMock()
    connect_to_dev_db_mock.return_value = db_connection
    transaction = MagicMock()
    db_connection.transaction.return_value = transaction
    transaction.table_exists.return_value = False

    connect_to_dev_metadatabase_mock = mocker.patch("polyteia.dags.extract_to_dev_db.connect_to_dev_metadatabase")
    connect_to_dev_metadatabase_mock.return_value = MagicMock()

    metadatabase_transaction_class_mock = mocker.patch("polyteia.table_cache.MetadatabaseTransaction")
    metadatabase_transaction_mock = metadatabase_transaction_class_mock.return_value
    metadatabase_transaction_mock.__enter__.return_value = metadatabase_transaction_mock
    metadatabase_transaction_mock.table_is_up_to_date.return_value = False

    metadatabase_connection_class_mock = mocker.patch("polyteia.extract.MetadatabaseConnection")
    metadatabase_connection_mock = metadatabase_connection_class_mock.return_value
    metadatabase_connection_mock.transaction.return_value = metadatabase_transaction_mock

    s3key = f"{s3prefix}/{file_name}"

    def write_file_content_to_file(key, path):
        with open(path, "w") as f:
            f.write(file_content)

    bucket_class_mock = mocker.patch("polyteia.extract.Bucket")
    bucket_mock = bucket_class_mock.return_value
    some_object = MagicMock()
    some_object.key = s3key
    bucket_mock.get_data_objects.return_value = [(file_name, export_timestamp, some_object)]
    bucket_mock.download_object.side_effect = write_file_content_to_file
    load_data_from_csv_original = load_data_from_csv

    def load_data_from_csv_wrapper(path: Path, has_headers: bool, separator: str, encoding: str, ensure_ascii: bool):
        # collapse generator early to avoid file not found error caused by leaving temp dir context manager
        return iter(list(load_data_from_csv_original(path, has_headers, separator, encoding, ensure_ascii)))

    load_data_from_csv_mock = mocker.patch("polyteia.parser_rule.load_data_from_csv")
    load_data_from_csv_mock.side_effect = load_data_from_csv_wrapper
    data_to_postgres_mock = mocker.patch("polyteia.extract.data_to_postgres")
    return bucket_mock, data_to_postgres_mock, metadatabase_connection_mock
