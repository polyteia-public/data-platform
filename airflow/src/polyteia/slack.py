import logging
from urllib.parse import urlencode

import requests

from polyteia.config import Config


def send_failure_to_slack(context):
    if not Config().SLACK_WEBHOOK_URL:
        logging.info("Config().SLACK_WEBHOOK_URL is not set, not sending alerts")
        return

    exception = context["exception"]
    ti = context["ti"]
    dag = context["dag"]
    if len(dag.tags) == 2:
        customer, standard = dag.tags
    else:
        customer, standard = [None, None]
    task_id = ti.task_id
    dag_id = ti.dag_id

    query = dict(task_id=task_id, dag_id=dag_id, execution_date=ti.execution_date.isoformat())
    url = Config().LOG_ENDPOINT + urlencode(query)

    webhook_url = Config().SLACK_WEBHOOK_URL

    message = (
        f"*Task* <{url}|{dag_id}.{task_id}> *failed*"
        f"\n*customer*: {customer}"
        f"\n*standard*: {standard}"
        f"\n*error*: {type(exception).__name__} - {exception}"
    )
    payload = {
        "attachments": [{"color": "danger", "text": message}],
    }

    try:
        requests.post(webhook_url, json=payload)
    except Exception as e:
        logging.error(e)
