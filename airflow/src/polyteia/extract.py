import tempfile
from datetime import datetime
from pathlib import Path
from typing import Any, Iterable, Optional, Sequence, Tuple, TypeVar

from polyteia.bucket import Bucket
from polyteia.data_source import DataSource
from polyteia.identifier_utils import slug
from polyteia.namespace import NamespaceInfo
from polyteia.parser_rule import CsvParserRule, JsonParserRule, ParserRule
from polyteia.store import EXTRACT_SCHEMA, data_to_postgres
from polyteia.table_cache import MetadatabaseConnection
from polyteia.transaction import (
    Connection,
    Transaction,
    ViewMapsToDifferentTable,
)

T = TypeVar("T")


def get_duplicates(input: Sequence[T]) -> Iterable[T]:
    seen = set()
    for x in input:
        if x in seen:
            yield x
        else:
            seen.add(x)


class NoDataFilesFound(Exception):
    pass


class DuplicateTableNames(Exception):
    pass


def ensure_different_file_names_do_not_map_to_the_same_table(
    namespaces: Sequence[str], data_objects: Sequence[Tuple[str, datetime, Any]], truncate_table_name: bool
) -> None:
    object_names = [file_name for file_name, _, _ in data_objects]
    object_and_table_names = [
        (object_name, _table_name_for_file_name(namespace, object_name, truncate_table_name))
        for namespace in namespaces
        for object_name in object_names
    ]

    deduplicated_object_and_table_names = set(object_and_table_names)
    partially_deduplicated_table_names = [table_name for _, table_name in deduplicated_object_and_table_names]

    if duplicate_table_names := list(get_duplicates(partially_deduplicated_table_names)):
        raise DuplicateTableNames(duplicate_table_names)


def extract_files_from_data_source(
    warehouse_connection: Connection,
    metadatabase_connection: MetadatabaseConnection,
    dag_namespaces: NamespaceInfo,
    data_source: DataSource,
):
    found_data = False
    data_objects = list(
        Bucket().get_data_objects(
            data_source.s3prefix, data_source.timestamp, data_source.incremental_extract_date_range
        )
    )
    namespaces = NamespaceInfo(
        primary_namespace=f"{dag_namespaces.primary_namespace}_{data_source.id}",
        additional_namespaces=[f"{namespace}_{data_source.id}" for namespace in dag_namespaces.additional_namespaces],
    )

    ensure_different_file_names_do_not_map_to_the_same_table(
        [namespaces.primary_namespace, *namespaces.additional_namespaces],
        data_objects,
        data_source.truncate_table_name,
    )

    for index, entry in enumerate(data_objects, 1):
        file_name, timestamp, s3_object = entry
        found_data = True

        print(f"Extracting file {file_name} ({index}/{len(data_objects)})")
        extract_file(
            warehouse_connection,
            metadatabase_connection,
            namespaces,
            file_name,
            s3_object.key,
            timestamp,
            data_source.parser_rules,
            data_source.incremental_extract_date_range,
            data_source.truncate_table_name,
            fail_if_no_parser_rule_matches=data_source.fail_if_no_parser_rule_matches,
        )

    if not found_data:
        raise NoDataFilesFound(f"No data files found for {data_source.s3prefix}.")


def extract_file(
    warehouse_connection: Connection,
    metadatabase_connection: MetadatabaseConnection,
    namespaces: NamespaceInfo,
    file_name: str,
    key: str,
    exported_at: datetime,
    parser_rules: Sequence[ParserRule],
    incremental_extract_date_range: Optional[Tuple[str, str]],
    truncate_table_name: bool,
    fail_if_no_parser_rule_matches: bool,
):
    parser_rule = next((rule for rule in parser_rules if rule.matches(file_name)), None)
    if not parser_rule:
        if fail_if_no_parser_rule_matches:
            raise ValueError(f"No parser rule matches file: {file_name}. Aborting.")
        print(f"No parser rule matches file: {file_name}. Skipping.")
        return

    if isinstance(parser_rule, CsvParserRule) or isinstance(parser_rule, JsonParserRule):
        _update_cachable_table(
            warehouse_connection,
            metadatabase_connection,
            namespaces,
            file_name,
            key,
            exported_at,
            parser_rule,
            incremental_extract_date_range,
            truncate_table_name,
        )
    else:
        _update_non_cacheable_table(warehouse_connection, namespaces, file_name, key, exported_at, parser_rule)


def _update_cachable_table(
    warehouse_connection: Connection,
    metadatabase_connection: MetadatabaseConnection,
    namespaces: NamespaceInfo,
    file_name: str,
    key: str,
    exported_at: datetime,
    parser_rule: ParserRule,
    incremental_extract_date_range: Optional[Tuple[str, str]],
    truncate_table_name: bool,
):
    table_name = _table_name_for_file_name(namespaces.primary_namespace, file_name, truncate_table_name)

    warehouse_connection.ensure_schema_exists(EXTRACT_SCHEMA)
    with warehouse_connection.transaction() as transaction:
        cache_id = transaction.get_cache_id(table_name)
        with metadatabase_connection.transaction() as metadatabase_transaction:
            file_last_modified = Bucket().last_modified(key)
            file_e_tag = Bucket().e_tag(key)
            extract_table_exists = transaction.table_exists(EXTRACT_SCHEMA, table_name)
            if not extract_table_exists:
                metadatabase_transaction.delete_cache_entries(cache_id)
            if incremental_extract_date_range is None:
                metadatabase_transaction.delete_cache_entries(cache_id, exported_at)
            if not extract_table_exists or (
                not metadatabase_transaction.table_is_up_to_date(
                    cache_id, exported_at, parser_rule, file_last_modified, file_e_tag
                )
            ):
                _update_tables_from_file(
                    transaction,
                    namespaces,
                    file_name,
                    key,
                    exported_at,
                    parser_rule,
                    truncate_table_name,
                    incremental_extract_date_range,
                )
                metadatabase_transaction.update_table_cache_entry(
                    cache_id, exported_at, parser_rule, file_last_modified, file_e_tag
                )


def _update_non_cacheable_table(
    db_connection: Connection,
    namespaces: NamespaceInfo,
    file_name: str,
    key: str,
    exported_at: datetime,
    parser_rule: ParserRule,
    truncate_table_name,
):
    with db_connection.transaction() as transaction:
        _update_tables_from_file(transaction, namespaces, file_name, key, exported_at, parser_rule, truncate_table_name)


class AdditionalNameAlreadyTaken(Exception):
    def __init__(self, name):
        self.name = name


def _update_tables_from_file(
    transaction: Transaction,
    namespaces: NamespaceInfo,
    file_name: str,
    key: str,
    exported_at: datetime,
    parser_rule: ParserRule,
    truncate_table_name: bool,
    incremental_extract_date_range: Optional[Tuple[str, str]] = None,
):
    with tempfile.TemporaryDirectory() as tmp_dir_str:
        tmp_dir = Path(tmp_dir_str)

        Bucket().download_object(key, tmp_dir / file_name)
        data = parser_rule.load_data_from_file(tmp_dir, file_name)

        full_table_name = _table_name_for_file_name(namespaces.primary_namespace, file_name, truncate_table_name)

        view_names = [
            _table_name_for_file_name(view_namespace, file_name, truncate_table_name)
            for view_namespace in namespaces.additional_namespaces
        ]
        try:
            _update_table(
                transaction,
                full_table_name,
                key,
                exported_at,
                parser_rule,
                data,
                incremental_extract_date_range,
                view_names,
            )
        except ViewMapsToDifferentTable as e:
            raise AdditionalNameAlreadyTaken(e.view_name) from e


def _update_table(
    transaction: Transaction,
    table_name: str,
    key: str,
    exported_at: datetime,
    parser_rule: ParserRule,
    data: Iterable[str],
    incremental_extract_date_range: Optional[Tuple[str, str]],
    view_names: Sequence[str] = (),
) -> None:
    print(f"Extracting '{key}' to '{table_name}'.")
    print(f"Extracting using {parser_rule}.")
    print()
    data_to_postgres(transaction, exported_at, data, table_name, incremental_extract_date_range, view_names)


def _table_name_for_file_name(namespace: str, file_name: str, truncate_table_name: bool) -> str:
    # truncate to 63 characters because the postgres table name length limit is 63
    # see https://www.postgresql.org/docs/current/sql-syntax-lexical.html#SQL-SYNTAX-IDENTIFIERS
    if truncate_table_name:
        return slug(f"{namespace}_{file_name}")[:63]
    else:
        return slug(f"{namespace}_{file_name}")
