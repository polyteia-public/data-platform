import os

import jsons
from airflow import DAG

from polyteia.config import Config
from polyteia.data_source import DataSources
from polyteia.dataset import (
    NAMESPACE_ERROR_MITIGATION_INFO,
    make_dag_init_args,
)
from polyteia.extract import (
    AdditionalNameAlreadyTaken,
    extract_files_from_data_source,
)
from polyteia.namespace import NamespaceInfo
from polyteia.python import make_python_operator
from polyteia.store import connect_to_dev_db, connect_to_dev_trino
from polyteia.table_cache import connect_to_dev_metadatabase


def extract_files_to_dev_db_task(params):
    database, dag_namespace, data_sources, additional_dag_namepsaces = (
        params["database"],
        # DEBT using customer_namespace as fallback
        # should be removed after all clients are updated, about 2 days after deployment
        params.get("dag_namespace") or params["customer_namespace"],
        jsons.load(params["data_sources"], DataSources),
        params.get("additional_dag_namespaces", []),
    )

    warehouse_type = params.get("warehouse", "postgres")
    if warehouse_type == "trino":
        warehouse_connection = connect_to_dev_trino(database)
    else:
        warehouse_connection = connect_to_dev_db(database)

    metadatabase_connection = connect_to_dev_metadatabase(database)
    dag_namespaces = NamespaceInfo(primary_namespace=dag_namespace, additional_namespaces=additional_dag_namepsaces)
    for data_source in data_sources:
        try:
            extract_files_from_data_source(warehouse_connection, metadatabase_connection, dag_namespaces, data_source)
        except AdditionalNameAlreadyTaken as e:
            raise ValueError(NAMESPACE_ERROR_MITIGATION_INFO) from e


def make_extract_to_dev_db_dag():
    if os.getenv("SKIP_MAKE_DAG", "0") == "1" or not Config().has_dev_db_access:
        return None
    dag = DAG(
        **make_dag_init_args(
            "extract_to_dev_db",
            description="Extract data into a dev-db.",
            max_active_runs=10,
            alert_slack_on_failure=False,
        ),
    )

    with dag:
        make_python_operator("extract_file_to_dev_db", extract_files_to_dev_db_task)

    return dag


extract_to_dev_db_dag = make_extract_to_dev_db_dag()
