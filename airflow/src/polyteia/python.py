from typing import Iterable, Optional, Union

from airflow.operators.python import PythonOperator

from polyteia.config import Config
from polyteia.docker import make_docker_operator


def make_python_operator(task_id, callable, cls=PythonOperator, dag=None, **kwargs):
    if callable is None:
        raise ValueError("Missing required argument callable")
    if Config().FAIL_FAST:
        kwargs["retries"] = 0
        kwargs.pop("retry_delay", None)

    if task_id == "extract_file_to_dev_db":
        kwargs["pool"] = "extract_file_to_dev_db"

    return cls(
        task_id=task_id,
        python_callable=callable,
        dag=dag,
        **kwargs,
    )


def make_dockerized_python_operator(
    task_id: str,
    path_to_python_file: str,
    python_runner_image: Optional[str],
    python_args: Optional[Union[dict, Iterable]] = None,
    **kwargs,
):
    additional_docker_args = ""

    if Config().HOST_EXTERNAL_PROJECT_DIR:
        additional_docker_args += f" -v {Config().HOST_EXTERNAL_PROJECT_DIR}/python:/src"
        python_image = Config().PYTHON_IMAGE
    else:
        additional_docker_args += " --pull always"
        python_image = python_runner_image

    env = {
        "DEV_DB_DATABASE": Config().DEV_DB_DATABASE,
        "DP_STORE_PGHOST": Config().STORE_PGHOST,
        "DP_STORE_PGPORT": Config().STORE_PGPORT,
        "DP_STORE_PGDATABASE": Config().STORE_PGDATABASE,
        "DP_STORE_PGUSER": Config().STORE_PGUSER,
        "DP_STORE_PGPASSWORD": Config().STORE_PGPASSWORD,
        "DP_STORE_TRINO_HOST": Config().STORE_TRINO_HOST,
        "DP_STORE_TRINO_PORT": Config().STORE_TRINO_PORT,
        "DP_STORE_TRINO_ICEBERG_CATALOG": Config().STORE_TRINO_ICEBERG_CATALOG,
        "DP_STORE_TRINO_HIVE_CATALOG": Config().STORE_TRINO_HIVE_CATALOG,
        "DP_STORE_TRINO_USER": Config().STORE_TRINO_USER,
        "S3_INGESTION_KEY": Config().S3_INGESTION_KEY,
        "S3_INGESTION_SECRET": Config().S3_INGESTION_SECRET,
        "COLLECT_INGESTION_KEY": Config().COLLECT_INGESTION_KEY,
        "COLLECT_INGESTION_USER": Config().COLLECT_INGESTION_USER,
        "COLLECT_INGESTION_PW": Config().COLLECT_INGESTION_PW,
        "EXTRA_INDEX_URL": Config().EXTRA_INDEX_URL,
    }

    if python_args:
        if isinstance(python_args, dict):
            python_args_list = []
            for k, v in python_args.items():
                python_args_list.extend([f"--{k}", v])
        elif isinstance(python_args, Iterable):
            python_args_list = list(python_args)
        else:
            raise TypeError("Invalid type for python_args, only 'dict' and 'Iterable' are allowed")
        python_args_str = " ".join(python_args_list)
        python_command = f"python {path_to_python_file} {python_args_str}"

    else:
        python_command = f"python {path_to_python_file}"

    return make_docker_operator(
        task_id=task_id,
        image=python_image,
        command=python_command,
        env={key: value for key, value in env.items() if value is not None},
        additional_docker_args=additional_docker_args,
        **kwargs,
    )
