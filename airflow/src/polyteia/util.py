import itertools


def merge_dicts(a: dict, b: dict) -> dict:
    all_keys = {key for key in itertools.chain(a.keys(), b.keys())}
    result = {}
    for key in all_keys:
        if key in a and key not in b:
            result[key] = a[key]
        elif key in b and key not in a:
            result[key] = b[key]
        else:
            if isinstance(a[key], dict) and isinstance(b[key], dict):
                result[key] = merge_dicts(a[key], b[key])
            else:
                result[key] = a[key]
    return result
