from dataclasses import dataclass
from typing import NewType, Optional, Sequence, Tuple

import jsons as jsons

from polyteia.parser_rule import (
    DEFAULT_PARSER_RULES,
    ParserRule,
    rule_from_dict,
)


@dataclass
class DataSource:
    id: str
    s3prefix: str
    parser_rules: Sequence[ParserRule]
    timestamp: Optional[str]
    incremental_extract_date_range: Optional[Tuple[str, str]]
    truncate_table_name: bool
    fail_if_no_parser_rule_matches: bool

    def __init__(
        self,
        *,
        id: str,
        s3prefix: str,
        parser_rules: Sequence[ParserRule] = None,
        timestamp: Optional[str] = None,
        incremental_extract_date_range: Optional[Tuple[str, str]] = None,
        truncate_table_name: bool = True,
        fail_if_no_parser_rule_matches: bool = False,
    ):
        self.id = id
        self.s3prefix = s3prefix
        # converting to a list because tuples are serialized as lists by json and then deserialized as lists
        # which breaks pytest assertions
        self.parser_rules = parser_rules or list(DEFAULT_PARSER_RULES)
        self.timestamp = timestamp
        self.incremental_extract_date_range = incremental_extract_date_range
        self.truncate_table_name = truncate_table_name
        self.fail_if_no_parser_rule_matches = fail_if_no_parser_rule_matches


DataSources = NewType("DataSources", Sequence[DataSource])


def data_sources_deserializer(obj: Sequence[dict], cls: type = DataSources, **kwargs) -> DataSources:
    return [_deserialize_data_source(data_source) for data_source in obj]


def _deserialize_data_source(obj: dict) -> DataSource:
    return DataSource(
        id=obj["id"],
        s3prefix=obj["s3prefix"],
        parser_rules=[rule_from_dict(rule) for rule in obj["parser_rules"]],
        timestamp=obj["timestamp"],
        incremental_extract_date_range=obj["incremental_extract_date_range"],
        truncate_table_name=obj["truncate_table_name"],
        fail_if_no_parser_rule_matches=obj.get("fail_if_no_parser_rule_matches", False),
    )


jsons.set_deserializer(data_sources_deserializer, DataSources)
