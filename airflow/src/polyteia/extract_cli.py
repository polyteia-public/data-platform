import datetime
import os
import sys
from dataclasses import dataclass
from enum import Enum
from importlib.util import module_from_spec, spec_from_file_location
from os.path import isdir
from typing import Any, Iterable, Mapping, Optional, Sequence

import click
from tabulate import tabulate

from polyteia.airflow_api import FAILED_STATE, SUCCESS_STATE, AirflowClient
from polyteia.dag import PolyteiaDAG
from polyteia.data_source import DataSource
from polyteia.dataset import DataSet
from polyteia.extract_remotely import (
    EXTRACT_DAG_ID,
    build_airflow_web_url_for_dag_run,
    trigger_extract_dag,
)
from polyteia.identifier_utils import customer_solution_id, slug
from polyteia.parser_rule import DEFAULT_PARSER_RULES, CsvParserRule


@click.group()
def cli():
    pass


class ConfigKey(Enum):
    AIRFLOW_TRIGGER_DAG_RUNS_USER_PASSWORD = "AIRFLOW_TRIGGER_DAG_RUNS_USER_PASSWORD"
    DP_PRODUCTION_API_BASE = "DP_PRODUCTION_API_BASE"
    DP_PRODUCTION_URL = "DP_PRODUCTION_URL"


@cli.command(
    help="""trigger remote extraction of all data sources from a customer/solution combination """
    """into a dev database."""
)
@click.argument("customer")
@click.argument("solution", required=False, default=None)
def run(customer, solution):
    if not (database := os.environ.get("DEV_DB_DATABASE")):
        print("ERROR: DEV_DB_DATABASE environment variable not set, make sure it's in your '.evnrc'")
        sys.exit(1)

    config_from_env = {}
    for key in ConfigKey:
        if not (value := os.environ.get(key.value)):
            print(
                f"ERROR: {key.value} environment variable not set,"
                f" are you running the latest version of the data stack?"
            )
            sys.exit(1)
        config_from_env[key] = value

    extract_config = dag_data_for_customer_solution(customer, solution)

    print(f"Extracting to database {database}")

    airflow_client = AirflowClient(
        base_url=config_from_env[ConfigKey.DP_PRODUCTION_API_BASE],
        username="trigger_dag_runs",
        password=config_from_env[ConfigKey.AIRFLOW_TRIGGER_DAG_RUNS_USER_PASSWORD],
    )

    dag_run = trigger_extract_dag(
        airflow_client,
        database,
        extract_config.warehouse,
        customer,
        customer_solution_id(customer, solution),
        extract_config.data_sources,
        extract_config.create_additional_namespaces,
    )
    dag_run_id = dag_run["dag_run_id"]

    dag_run_web_url = build_airflow_web_url_for_dag_run(
        config_from_env[ConfigKey.DP_PRODUCTION_URL], EXTRACT_DAG_ID, dag_run["execution_date"]
    )
    print(f"DAG run triggered, you can see the live state at {dag_run_web_url}.")

    dag_state_after_finish_waiting = airflow_client.wait_for_dag_run(EXTRACT_DAG_ID, dag_run_id)

    if dag_state_after_finish_waiting is None:
        print("DAG is still running but will not wait further.")
        print(f"You can check the state of the run at {dag_run_web_url}")

    dag_name = f"{customer} {solution}" if solution else customer
    if dag_state_after_finish_waiting == FAILED_STATE:
        print("==================")
        print("LOG OUTPUT FOLLOWS")
        print("==================")
        print(airflow_client.get_logs_of_dag_run(EXTRACT_DAG_ID, dag_run_id))
        print(f"ERROR: Remote extract run failed for {dag_name}.")
    if dag_state_after_finish_waiting == SUCCESS_STATE:
        print(f"Remote extract run succeeded for {dag_name}.")


def validate_date(date_str):
    try:
        datetime.datetime.strptime(date_str, "%Y-%m-%d")
    except ValueError:
        raise ValueError("Incorrect data format, should be YYYY-MM-DD")


@dataclass
class ExtractConfig:
    data_sources: Sequence[DataSource]
    warehouse: str
    create_additional_namespaces: bool = False


def dag_data_for_customer_solution(customer: str, solution: Optional[str]) -> ExtractConfig:
    if data_set := find_data_set(customer, solution):
        return ExtractConfig(data_set.data_sources, data_set.warehouse, data_set.create_additional_namespaces)

    elif polyteia_dag := find_polyteia_dag(customer, solution):
        return ExtractConfig(
            polyteia_dag.data_sources, polyteia_dag.warehouse, polyteia_dag.create_additional_namespaces
        )

    else:
        if os.getenv("FAIL_IF_NO_DAG_FOUND") == "1":
            print("No existing solution DAG, exiting")
            sys.exit(1)
        print("No existing solution DAG, building synthetic DataSource from user input.")
        example = """
        Example:
        DataSource(id="solution",
                       s3prefix="customer/solution",
                       parser_rules=[CsvParserRule(pattern=".*\\\\.csv", separator=";", encoding="utf-8-sig", has_headers=False)],
                       incremental_extract_date_range=("2022-08-01", "2022-08-10"))
        """
        print(example)

        id = input("Please enter Datasource id:")
        s3prefix = input("Please enter an s3prefix:")

        input_parser_rules = []
        input_parser_rule = ""
        runs = 0
        while input_parser_rule != "n":
            if runs == 0:
                input_parser_rule = input("Add a CsvParserRule parser rule? [y/n]:")
            else:
                input_parser_rule = ""
                input_parser_rule = input("Any other CsvParserRule parser rule? [y/n]:")
            if input_parser_rule == "y":
                runs += 1
                input_pattern_rule = input(r"Please enter  the pattern rule ( e.g .*\.csv ):")
                print(input_pattern_rule)
                input_separator = input("Please enter the separator rule ( e.g  ; ):")
                input_encoding_rule = input("Please enter  the encoding rule ( e.g  utf-8-sig ):")
                input_has_headers = input("Has headers? [True/False]:")
                input_parser_rules.append(
                    (input_parser_rule, input_pattern_rule, input_separator, input_encoding_rule, input_has_headers)
                )
            else:
                print("")

        output_parser_rule = []
        for item in input_parser_rules:
            has_headers = ""
            if item[4] == "False":
                has_headers = False
            elif item[4] == "True":
                has_headers = True
            output_parser_rule.append(
                CsvParserRule(
                    has_headers=has_headers,
                    separator=item[2],
                    encoding=item[3],
                    pattern=item[1],
                )
            )

        while True:
            enable_timeseries_extraction = input("Timeseries extraction? [y/n]:")
            if enable_timeseries_extraction == "y" or enable_timeseries_extraction == "n":
                break
            else:
                print('Timeseries extraction should be "y"or "n"')

        if enable_timeseries_extraction == "y":
            print("Please enter a start date in the format YYYY-MM-DD:")
            incremental_extract_start_date = input()
            print("Please enter an end date in the format YYYY-MM-DD:")
            incremental_extract_end_date = input()
            if incremental_extract_start_date == "":
                incremental_extract_start_date = None
            if incremental_extract_end_date == "":
                incremental_extract_end_date = None
            validate_date(incremental_extract_start_date)
            validate_date(incremental_extract_end_date)
            incremental_extract_date_range = (incremental_extract_start_date, incremental_extract_end_date)
        else:
            incremental_extract_date_range = None

        warehouse = input("Warehouse (default: postgres):") or "postgres"

        return ExtractConfig(
            [
                DataSource(
                    id=id,
                    s3prefix=s3prefix,
                    parser_rules=output_parser_rule or DEFAULT_PARSER_RULES,
                    incremental_extract_date_range=incremental_extract_date_range,
                )
            ],
            warehouse=warehouse,
        )


def find_data_set(customer: str, solution: Optional[str]) -> Optional[DataSet]:
    return find_data_set_in_paths(get_dag_paths(), customer, solution)


def find_polyteia_dag(customer: str, solution: Optional[str]) -> Optional[PolyteiaDAG]:
    return find_polyteia_dag_in_paths(get_dag_paths(), customer, solution)


def get_dag_paths():
    return files_in_dir("/opt/dp/airflow/src/polyteia/dags")


def files_in_dir(dag_base_dir: str, excluded_names: Sequence[str] = ()):
    return [
        os.path.join(dag_base_dir, file)
        for file in os.listdir(dag_base_dir)
        if not isdir(os.path.join(dag_base_dir, file)) and file not in excluded_names
    ]


def find_data_set_in_paths(dag_paths: Iterable[str], customer: str, solution: Optional[str]) -> Optional[DataSet]:
    for path in dag_paths:
        for value in get_vars_from_file(path).values():
            if isinstance(value, DataSet) and value.customer == customer and value.solution == solution:
                print(f"using config from {path}")
                return value


def find_polyteia_dag_in_paths(dag_paths: Sequence[str], customer, solution: Optional[str]) -> Optional[PolyteiaDAG]:
    for path in dag_paths:
        for value in get_vars_from_file(path).values():
            if isinstance(value, PolyteiaDAG) and value.customer == customer and value.solution == solution:
                print(f"using config from {path}")
                return value


def get_vars_from_file(full_path: str) -> Mapping[str, Any]:
    spec = spec_from_file_location("dag", full_path)
    mod = module_from_spec(spec)
    os.environ["SKIP_MAKE_DAG"] = "1"
    try:
        spec.loader.exec_module(mod)
        return mod.__dict__
    except Exception as e:
        print(f'DAG config at {full_path} failed with {type(e).__name__}: "{e}"')
        return {}


@cli.command(help="""List files for customer and standard.""")
@click.argument("customer", required=False)
@click.argument("standard", required=False)
def ls(customer, standard):
    from polyteia.bucket import Bucket

    bucket = Bucket()

    if not customer:
        customers = {obj.key.split("/")[0] for obj in bucket.objects_without_directories("")}
        print(f"customers: {sorted(customers)}")
        return

    if not standard:
        standards = {obj.key.split("/")[1] for obj in bucket.objects_without_directories(customer)}
        print(f"standards: {sorted(standards)}")
        return

    tables = {}
    for file_name, date_time, s3_object in bucket.get_data_objects(customer):
        table_prefix = slug(customer + "_" + standard)
        full_table_name = slug(table_prefix + "_" + file_name)
        tables[full_table_name] = dict(
            table_name=full_table_name,
            file=f"{file_name}",
            exists=None,
        )

    print(render_ls(tables))


def render_ls(tables):
    from polyteia.store import tables_exist

    for table_name, exists in tables_exist(tables.keys(), schema_name="extract").items():
        tables[table_name]["exists"] = exists
    res = [[t["file"], t["table_name"], t["exists"]] for t in tables.values()]
    return tabulate(res, headers=["file.key", "table name", "table exists"])


if __name__ == "__main__":
    cli()
