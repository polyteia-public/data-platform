import re
from typing import Optional


def slug(path):
    path = re.sub("[.][a-z]+$", "", path)
    path = re.sub("^([0-9]+)", r"_\1", path)
    path = re.sub("[^a-zA-Z0-9_]+", "_", path)
    path = path.lower()
    return path


def customer_solution_id(customer: str, solution: Optional[str]) -> str:
    if not solution:
        return customer
    return f"{customer}_{solution}"


def customer_standard_id(customer: str, standard: str) -> str:
    return customer_solution_id(customer, standard)
