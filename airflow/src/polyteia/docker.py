from typing import Mapping

from airflow.operators.bash import BashOperator

from polyteia.config import Config


def make_docker_operator(
    task_id: str, image: str, command: str, env: Mapping[str, str] = None, additional_docker_args: str = "", **kwargs
):
    if not env:
        env = {}

    if Config().FAIL_FAST:
        kwargs["retries"] = 0
        kwargs.pop("retry_delay", None)

    bash_command = f"""
        echo "Running with image: {image}" &&
        docker images {image} &&
        echo -n "$REGISTRY_PASSWORD" | docker login -u {Config().REGISTRY_USER} --password-stdin registry.gitlab.com &&
        docker run --rm
            --network "${{DP_DOCKER_NETWORK}}"
            {" ".join(f"-e {key}" for key in env.keys())}
            {additional_docker_args}
            '{image}'
            {command}
    """

    bash_operator_kwargs = {k: v for k, v in kwargs.items() if k not in ["customer", "solution"]}

    bash_command = bash_command.replace("\n", "")
    return BashOperator(
        task_id=task_id,
        bash_command=bash_command,
        # env values are passed here to not reveal secrets in airflow logs
        env={
            **env,
            "REGISTRY_PASSWORD": Config().REGISTRY_PASSWORD,
            "DP_DOCKER_NETWORK": Config().DOCKER_NETWORK,
        },
        **bash_operator_kwargs,
    )


def project_runner_image(dag_id: str, suffix: str) -> str:
    base_url = Config().REGISTRY_BASE_URL
    project_name = dag_id.replace("_", "-")
    return f"{base_url}/{project_name}/{suffix}"
