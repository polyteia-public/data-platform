from dataclasses import dataclass
from typing import Sequence


@dataclass
class NamespaceInfo:
    primary_namespace: str
    additional_namespaces: [Sequence[str]] = ()
