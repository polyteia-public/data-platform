import re
from abc import ABC, abstractmethod
from dataclasses import dataclass
from datetime import datetime
from typing import Any, Iterable

import pandas as pd
import psycopg2
import pytz
import s3fs
import sqlalchemy
from sqlalchemy import MetaData


class Transaction(ABC):
    @abstractmethod
    def __enter__(self) -> "Transaction":
        pass

    @abstractmethod
    def __exit__(self, exc_type, exc_val, exc_tb) -> None:
        pass

    @abstractmethod
    def get_cache_id(self, table_name: str) -> str:
        pass

    @abstractmethod
    def create_or_replace_view(self, schema: str, table_name: str, view_name: str):
        pass

    @abstractmethod
    def drop_table(self, schema: str, table_name: str):
        pass

    @abstractmethod
    def ensure_extract_table_exists(self, schema: str, table_name: str):
        pass

    @abstractmethod
    def insert_or_replace_extract_records(
        self, schema: str, table_name: str, data: Iterable[str], exported_at: datetime, incremental: bool
    ) -> None:
        pass

    @abstractmethod
    def table_exists(self, schema: str, table_name: str):
        pass

    @abstractmethod
    def verify_that_view_maps_to_expected_table_if_it_exists(self, schema: str, view_name: str, table_name: str):
        pass


@dataclass
class PsycopgTransaction(Transaction):
    _connection: Any

    def __enter__(self) -> "Transaction":
        self._connection.__enter__()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb) -> None:
        self._connection.__exit__(exc_type, exc_val, exc_tb)

    def cursor(self, **kwargs):
        return self._connection.cursor(**kwargs)

    def get_cache_id(self, table_name: str) -> None:
        return table_name

    def create_or_replace_view(self, schema: str, table_name: str, view_name: str):
        with self.cursor() as cur:
            cur.execute(f"CREATE OR REPLACE VIEW {schema}.{view_name} " f"AS SELECT * FROM {schema}.{table_name}")

    def delete_records(self, schema: str, table_name: str, condition: str):
        with self.cursor() as cur:
            cur.execute(f"DELETE FROM {schema}.{table_name} {condition}")

    def drop_table(self, schema: str, table_name: str):
        with self.cursor() as cur:
            cur.execute(f"DROP TABLE {schema}.{table_name}")

    def ensure_extract_table_exists(self, schema: str, table_name: str):
        with self.cursor() as cur:
            cur.execute(
                f"CREATE TABLE IF NOT EXISTS {schema}.{table_name} "
                f"(extracted_at timestamptz, exported_at timestamptz, data jsonb)"
            )

    def table_exists(self, schema: str, table_name: str):
        with self.cursor() as cur:
            cur.execute(
                "SELECT true FROM information_schema.tables WHERE "
                "table_schema = %s AND "
                "table_name = %s AND "
                "table_type = 'BASE TABLE'",
                [schema, table_name],
            )

            return bool(cur.fetchone())

    def get_view_definition(self, schema: str, view_name: str):
        with self.cursor() as cur:
            cur.execute(f"SELECT pg_get_viewdef(to_regclass('{schema}.{view_name}'))")
            return cur.fetchone()[0]

    def insert_extract_records(self, schema: str, table_name: str, data: Iterable[str], exported_at: datetime) -> None:
        extracted_at = datetime.utcnow().isoformat() + "+00:00"
        stmt = f"INSERT INTO {schema}.{table_name} (extracted_at, exported_at, data) VALUES (%s::timestamptz, %s::timestamptz, %s::jsonb)"
        with self.cursor() as cur:
            psycopg2.extras.execute_batch(cur, stmt, ([extracted_at, exported_at, row] for row in data))

    def insert_or_replace_extract_records(
        self, schema: str, table_name: str, data: Iterable[str], exported_at: datetime, incremental: bool
    ) -> None:
        if incremental:
            self.delete_records(schema, table_name, f"WHERE exported_at='{exported_at}'")
        else:
            self.truncate_table(schema, table_name)

        self.insert_extract_records(schema, table_name, data, exported_at)

    def truncate_table(self, schema: str, table_name):
        with self.cursor() as cur:
            cur.execute(f"TRUNCATE TABLE {schema}.{table_name};")

    def verify_that_view_maps_to_expected_table_if_it_exists(self, schema: str, view_name: str, table_name: str):
        existing_view_definition = self.get_view_definition(schema, view_name)
        if existing_view_definition:
            if not (
                re.search(f'FROM "{schema}".{table_name};', existing_view_definition)
                or re.search(f"FROM {table_name};", existing_view_definition)
            ):
                raise ViewMapsToDifferentTable(
                    f"Existing view definition for {schema}.{view_name} does not map to table "
                    f"{schema}.{table_name}. "
                    f'Existing view definition: "{existing_view_definition}"',
                    f"{schema}.{view_name}",
                )


class Connection(ABC):
    @abstractmethod
    def transaction(self) -> Transaction:
        pass

    @abstractmethod
    def close(self) -> None:
        pass

    @abstractmethod
    def ensure_schema_exists(self, schema: str):
        pass


@dataclass
class PsycopgConnection(Connection):
    _connection: Any

    def transaction(self) -> Transaction:
        return PsycopgTransaction(self._connection)

    def close(self) -> None:
        self._connection.close()

    def ensure_schema_exists(self, schema: str):
        """
        This method exists at the Connection level since it needs to always run in its own transaction. This is required
        since all actions on the transaction after a UniqueViolation will fail. Since the UniqueViolation is expected in
        certain non-error scenarios (see comment below), nothing else should run in the same transaction as this.
        """
        with self.transaction() as transaction:
            try:
                with transaction.cursor() as cur:
                    cur.execute(f"SELECT schema_name FROM information_schema.schemata WHERE schema_name = '{schema}'")
                    schema_exists = bool(cur.fetchone())
                if not schema_exists:
                    with transaction.cursor() as cur:
                        cur.execute(f"CREATE SCHEMA IF NOT EXISTS {schema};")
            except psycopg2.errors.UniqueViolation:
                # https://stackoverflow.com/questions/29900845/create-schema-if-not-exists-raises-duplicate-key-error
                pass


def utcnow():
    return datetime.utcnow()


@dataclass
class TrinoTransaction(Transaction):
    s3_bucket: str
    _trans_ctx: Any
    _s3_fs: s3fs.S3FileSystem
    hive_schema: str
    _connection: sqlalchemy.engine.Connection = None

    def __enter__(self) -> "TrinoTransaction":
        self._connection = self._trans_ctx.__enter__()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb) -> None:
        self._connection.__exit__(exc_type, exc_val, exc_tb)

    @property
    def connection(self) -> sqlalchemy.engine.Connection:
        return self._connection

    def get_cache_id(self, table_name: str) -> str:
        # DEBT: prepending trino cache entries with a special character to enable the distinction of
        # trino vs postgres cache entries. should be removed after all solutions are updated.
        return f"*{table_name}"

    def create_or_replace_view(self, schema: str, table_name: str, view_name: str):
        raise NotImplementedError("Trino+Hive does not support views at the moment")

    def drop_table(self, schema: str, table_name: str):
        raise NotImplementedError("Trino+Hive does not support views at the moment")

    def ensure_extract_table_exists(self, schema: str, table_name: str):
        self._connection.execute(
            f"""
            CREATE TABLE IF NOT EXISTS {self.hive_schema}.{schema}__{table_name} (
                extracted_at TIMESTAMP(3),
                exported_at TIMESTAMP(3),
                data VARCHAR
            )
            WITH (
                format = 'PARQUET',
                external_location = 's3://{self.s3_bucket}/{self.s3_bucket}/{self.hive_schema}/{schema}__{table_name}/'
            )
            """
        )

    def insert_or_replace_extract_records(
        self, schema: str, table_name: str, data: Iterable[str], exported_at: datetime, incremental: bool
    ) -> None:
        df = pd.DataFrame(data, columns=["data"])
        df = df.assign(exported_at=exported_at.astimezone(pytz.UTC))
        df = df.assign(extracted_at=utcnow())

        exported_at_iso8601 = exported_at.isoformat()

        print(
            f"Saving parquet to: '{self.s3_bucket}/{self.hive_schema}/{schema}__{table_name}/{exported_at_iso8601}/{table_name}.parquet'"
        )

        with self._s3_fs.open(
            f"s3://{self.s3_bucket}/{self.hive_schema}/{schema}__{table_name}/{exported_at_iso8601}/{table_name}.parquet",
            "wb",
        ) as f:
            df.to_parquet(f, engine="fastparquet")

    def table_exists(self, schema: str, table_name: str):
        # sqlalchemy Table.exists has no way to distinguish tables from views,
        # so we still need to query information_schema.tables
        meta_data = MetaData(schema="information_schema", bind=self._connection)
        meta_data.reflect()
        tables = meta_data.tables["information_schema.tables"]
        select = (
            tables.select()
            .where(tables.c.table_schema == self.hive_schema)
            .where(tables.c.table_name == f"{schema}__{table_name}")
            .where(tables.c.table_type == "BASE TABLE")
        )
        return bool(self._connection.execute(select).fetchone())

    def verify_that_view_maps_to_expected_table_if_it_exists(self, schema: str, view_name: str, table_name: str):
        if self.view_exists(schema, view_name):
            existing_view_definition = self._connection.execute(
                f'SHOW CREATE VIEW "{schema}"."{view_name}"'
            ).fetchone()[0]
            if not re.match(
                f'CREATE VIEW [a-z_]+."?{schema}"?."?{view_name}"? SECURITY DEFINER AS\\s+'
                f'SELECT \\*\\s+FROM\\s+("?{schema}"?.)?"?{table_name}"?',
                existing_view_definition,
            ):
                raise ViewMapsToDifferentTable(
                    f"Existing view definition for {schema}.{view_name} does not map to table "
                    f"{schema}.{table_name}. "
                    f'Existing view definition: "{existing_view_definition}"',
                    f"{schema}.{view_name}",
                )

    def view_exists(self, schema: str, table_name: str):
        raise NotImplementedError("Trino+Hive does not support views at the moment")


@dataclass
class TrinoConnection(Connection):
    _engine: sqlalchemy.engine.Engine
    s3_bucket: str
    s3_access_key: str
    s3_secret_key: str
    s3_endpoint_url: str
    hive_schema: str

    def transaction(self) -> TrinoTransaction:
        return TrinoTransaction(
            self.s3_bucket,
            self._engine.begin(),
            _s3_fs=s3fs.S3FileSystem(
                anon=False,
                use_ssl=True,
                key=self.s3_access_key,
                secret=self.s3_secret_key,
                client_kwargs={
                    "region_name": "eu-de",
                    "endpoint_url": f"{self.s3_endpoint_url}/{self.s3_bucket}",
                    "verify": True,
                },
            ),
            hive_schema=self.hive_schema,
        )

    def close(self) -> None:
        pass

    def ensure_schema_exists(self, schema: str):
        # Trino+Hive does not support the automated creation of schemas
        pass


class ViewMapsToDifferentTable(Exception):
    def __init__(self, message: str, view_name: str):
        super().__init__(message)
        self.view_name = view_name
