import io
import re
from datetime import datetime
from pathlib import Path
from typing import Any, Iterable, Tuple

import boto3
import dateutil

from polyteia.config import Config


def timestamp_from_prefix(prefix: str) -> datetime:
    try:
        return dateutil.parser.parse(prefix.replace("_", ":").split("/")[-1])
    except ValueError as e:
        raise ValueError(f"Failed to parse date from {prefix}: {e}")


def is_open_date_range(incremental_extract_date_range: Tuple[str, str]) -> bool:
    return not incremental_extract_date_range[0] and not incremental_extract_date_range[1]


def is_start_open_date_range(incremental_extract_date_range: Tuple[str, str]) -> bool:
    return not incremental_extract_date_range[0] and incremental_extract_date_range[1]


def is_end_open_date_range(incremental_extract_date_range: Tuple[str, str]) -> bool:
    return incremental_extract_date_range[0] and not incremental_extract_date_range[1]


class NoLatestFileFound(Exception):
    pass


class Bucket:
    def __init__(self):
        self.session = boto3.Session(
            aws_access_key_id=Config().AWS_ACCESS_KEY_ID,
            aws_secret_access_key=Config().AWS_SECRET_ACCESS_KEY,
        )
        self.s3 = self.session.resource("s3", endpoint_url=Config().S3_ENDPOINT_URL)
        self.bucket = self.s3.Bucket(Config().S3_BUCKET)
        self.objects = self.bucket.objects

    def objects_without_directories(self, Prefix):
        for obj in self.objects.filter(Prefix=Prefix):
            if obj.key[-1] != "/":
                yield obj

    def object_contents(self, key, encoding="utf-8"):
        stream = io.BytesIO()
        self.bucket.Object(key).download_fileobj(stream)
        value = stream.getvalue()
        if encoding is not None:
            value = value.decode(encoding)
        return value

    def download_object(self, key, file):
        if isinstance(file, Path):
            file = file.open("wb")
        self.bucket.Object(key).download_fileobj(file)

    def e_tag(self, key) -> datetime:
        return self.bucket.Object(key).e_tag

    def last_modified(self, key) -> datetime:
        return self.bucket.Object(key).last_modified

    def list_objects(self, Prefix, suffix=None):
        if suffix:
            return (s3object for s3object in self.bucket.objects.filter(Prefix=Prefix) if s3object.key.endswith(suffix))
        else:
            return self.bucket.objects.filter(Prefix=Prefix)

    def list_timestamp_directories(self, Prefix):
        timestamped_directories = []
        pattern = rf"{Prefix}/\d{{4}}-\d{{2}}-\d{{2}}"
        for s3object in self.bucket.objects.filter(Prefix=f"{Prefix}/"):
            if re.search(pattern, s3object.key):
                obj_key = s3object.key.split("/")[2]
                dateutil.parser.parse(obj_key[:10])
                timestamped_directories.append(obj_key)

        return list(set(timestamped_directories))

    def get_in_range_timestamp_dirs(self, s3prefix, incremental_extract_date_range):
        timestamped_directories = self.list_timestamp_directories(Prefix=s3prefix)
        timestamped_directories.sort()
        if is_open_date_range(incremental_extract_date_range):
            in_range_timestamped_directories = timestamped_directories
            return in_range_timestamped_directories
        elif is_start_open_date_range(incremental_extract_date_range):
            date_start, date_end = (
                dateutil.parser.parse(timestamped_directories[0]).date(),
                datetime.strptime(incremental_extract_date_range[1], "%Y-%m-%d").date(),
            )
            in_range_timestamped_directories = [
                dir
                for dir in timestamped_directories
                if date_start <= dateutil.parser.parse(dir[:10]).date() <= date_end
            ]
            return in_range_timestamped_directories
        elif is_end_open_date_range(incremental_extract_date_range):
            path, timestamp = self.get_latest_folder(s3prefix)
            latest = timestamp.date()
            date_start, date_end = datetime.strptime(incremental_extract_date_range[0], "%Y-%m-%d").date(), latest
            in_range_timestamped_directories = [
                dir
                for dir in timestamped_directories
                if date_start <= dateutil.parser.parse(dir[:10]).date() <= date_end
            ]
            return in_range_timestamped_directories
        else:
            date_start, date_end = (
                datetime.strptime(incremental_extract_date_range[0], "%Y-%m-%d").date(),
                datetime.strptime(incremental_extract_date_range[1], "%Y-%m-%d").date(),
            )
            in_range_timestamped_directories = [
                dir
                for dir in timestamped_directories
                if date_start <= dateutil.parser.parse(dir[:10]).date() <= date_end
            ]
            return in_range_timestamped_directories

    def get_data_objects(
        self, s3prefix, fixed_timestamp=None, incremental_extract_date_range=None, file_key=None
    ) -> Iterable[Tuple[str, datetime, Any]]:
        if incremental_extract_date_range:
            if len(incremental_extract_date_range) != 2:
                raise ValueError("Extract date ranges unknown.")
            in_range_timestamped_directories = self.get_in_range_timestamp_dirs(
                s3prefix, incremental_extract_date_range
            )
            for dir_item in in_range_timestamped_directories:
                for data_object in self.list_objects(Prefix=f"{s3prefix}/{dir_item}/", suffix=file_key):
                    file_name = data_object.key[len(f"{s3prefix}/{str(dir_item)}/") :]
                    if not file_name or file_name == "/":
                        continue
                    dir = timestamp_from_prefix(f"{s3prefix}/{dir_item}")
                    yield file_name, dir, data_object
        else:
            if fixed_timestamp:
                full_prefix = f"{s3prefix}/{fixed_timestamp}"
                timestamp = timestamp_from_prefix(full_prefix)
                path = full_prefix + "/"
            else:
                path, timestamp = self.get_latest_folder(s3prefix)
            for data_object in self.list_objects(Prefix=path, suffix=file_key):
                file_name = data_object.key[len(path) :]
                # for some reason objects.filter also returns a directory matching the prefix
                if not file_name:
                    continue
                yield file_name, timestamp, data_object

    def get_latest_folder(self, s3prefix) -> (str, datetime):
        for file in self.list_objects(Prefix=s3prefix):
            path = Path(file.key)
            if path.name == "LATEST":
                latest_path = self.object_contents(file.key)
                latest_timestamp = timestamp_from_prefix(latest_path)
                return latest_path.strip() + "/", latest_timestamp
        raise NoLatestFileFound(f"No LATEST file found for {s3prefix}")
