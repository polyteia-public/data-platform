import os
from datetime import datetime
from typing import Optional

from airflow import DAG
from airflow.models import XCom
from airflow.utils.db import provide_session

from polyteia.docker import project_runner_image
from polyteia.identifier_utils import customer_solution_id
from polyteia.python import (
    make_dockerized_python_operator,
    make_python_operator,
)
from polyteia.sftp_ingest import sftp_to_s3
from polyteia.slack import send_failure_to_slack


@provide_session
def cleanup_xcom(context, session=None):
    session.query(XCom).filter(XCom.dag_id == context["ti"].dag_id).delete()


# NOTE you might be tempted to try to write this a generator function
# (so placing the DAG constructor call directly in
# here). don't. there's a lot of airflow magic going on associate DAGs
# with their files and, sadly, it all breaks down if the pipeline
# files don't import the DAG module and they the constructor's caller
# isn't in the same file. code is being too clever :( 20210427:mb
def make_dag_init_args(dag_id, description="", schedule_interval=None, alert_slack_on_failure=True):
    return dict(
        dag_id=dag_id,
        description=description,
        default_args=dict(
            owner="polyteia",
            depends_on_past=False,
            email_on_failure=False,
            email_on_retry=False,
            on_failure_callback=send_failure_to_slack if alert_slack_on_failure is True else None,
            retries=0,
        ),
        schedule_interval=schedule_interval,
        start_date=datetime(2020, 8, 1),
        max_active_runs=1,
        catchup=False,
        on_success_callback=cleanup_xcom,
    )


def move_files_sftp_to_s3(ingest_config, customer, solution):
    def task():
        sftp_to_s3(ingest_config, customer, solution)

    return task


class Ingestion:
    def __init__(
        self,
        customer,
        solution,
        /,
        *,
        ingest_config: Optional[object] = None,
        python_runner_image: Optional[str] = None,
        schedule_interval: str = "@daily",
        parser_customer_specific: Optional[str] = None,
    ):
        self.customer = customer
        self.solution = solution
        self.ingest_config = ingest_config
        self.python_runner_image = python_runner_image or project_runner_image(
            customer_solution_id(customer, solution), "python"
        )
        self.schedule_interval = schedule_interval
        self.parser_customer_specific = parser_customer_specific or "parser_customer_specific.py"

    @property
    def dag_id(self):
        return f"{customer_solution_id(self.customer, self.solution)}_ingest"

    def make_dag(self):
        if os.getenv("SKIP_MAKE_DAG", "0") == "1":
            return self
        dag = DAG(
            **make_dag_init_args(
                self.dag_id,
                description=f"Ingest {self.customer}/{self.solution} data.",
                schedule_interval=self.schedule_interval,
            ),
            tags=[self.customer, self.solution, "ingestion"],
        )

        with dag:
            sftp_ingest = make_python_operator(
                f"sftp_to_s3.{self.customer}",
                move_files_sftp_to_s3(ingest_config=self.ingest_config, customer=self.customer, solution=self.solution),
            )

            (
                sftp_ingest
                >> make_dockerized_python_operator(
                    "python.parse_customer_specific",
                    self.parser_customer_specific,
                    self.python_runner_image,
                )
            )

        return dag
