import os
from datetime import datetime, timedelta
from random import randint
from typing import Callable, Mapping, Optional, Sequence, Union

from airflow import DAG
from airflow.models import Operator, XCom
from airflow.utils.db import provide_session

from polyteia.config import Config
from polyteia.data_source import DataSource
from polyteia.dbt import (
    make_dbt_operator,
    make_dbt_operator_with_select,
    make_dbt_seed_operator,
)
from polyteia.docker import make_docker_operator, project_runner_image
from polyteia.extract import (
    AdditionalNameAlreadyTaken,
    extract_files_from_data_source,
)
from polyteia.extract_remotely import trigger_extract_dag_and_wait
from polyteia.identifier_utils import customer_solution_id
from polyteia.namespace import NamespaceInfo
from polyteia.parser_rule import DEFAULT_PARSER_RULES
from polyteia.python import (
    make_dockerized_python_operator,
    make_python_operator,
)
from polyteia.slack import send_failure_to_slack
from polyteia.store import (
    connect_to_dev_trino,
    connect_to_main_db,
    connect_to_trino,
)
from polyteia.table_cache import connect_to_main_metadatabase

NAMESPACE_ERROR_MITIGATION_INFO = (
    "Failed to create additional table names for compatibility. If you are sure that your DAG is using "
    "the latest naming convention you can disable the creation of additional table names by setting "
    "create_additional_namespaces to False: DataSet(..., create_additional_namespaces=False)."
)


@provide_session
def cleanup_xcom(context, session=None):
    session.query(XCom).filter(XCom.dag_id == context["ti"].dag_id).delete()


# NOTE you might be tempted to try to write this a generator function
# (so placing the DAG constructor call directly in
# here). don't. there's a lot of airflow magic going on associate DAGs
# with their files and, sadly, it all breaks down if the pipeline
# files don't import the DAG module and they the constructor's caller
# isn't in the same file. code is being too clever :( 20210427:mb
def make_dag_init_args(
    dag_id, description="", schedule_interval=None, alert_slack_on_failure=True, max_active_runs: Optional[int] = 1
):
    return dict(
        dag_id=dag_id,
        description=description,
        default_args=dict(
            owner="polyteia",
            depends_on_past=False,
            email_on_failure=False,
            email_on_retry=False,
            retries=5,
            retry_delay=timedelta(seconds=randint(120, 180)),
            retry_exponential_backoff=True,
            max_retry_delay=timedelta(seconds=60),
            on_failure_callback=send_failure_to_slack if alert_slack_on_failure is True else None,
        ),
        schedule_interval=schedule_interval,
        start_date=datetime(2020, 8, 1),
        max_active_runs=max_active_runs,
        catchup=False,
        on_success_callback=cleanup_xcom,
    )


def extract_files_for_data_source(
    dag_namespaces: NamespaceInfo,
    data_source: DataSource,
    warehouse: str,
    dag_id: str,
    namespace_error_mitigation_info: str = "",
):
    def task():
        if warehouse == "postgres":
            warehouse_connection = connect_to_main_db()
        elif warehouse == "trino":
            if Config().DEV_DB_DATABASE:
                warehouse_connection = connect_to_dev_trino(Config().DEV_DB_DATABASE)
            else:
                warehouse_connection = connect_to_trino(dag_id)
        else:
            raise ValueError(f"unknown warehouse '{warehouse}'")

        metadatabase_connection = connect_to_main_metadatabase()
        try:
            extract_files_from_data_source(warehouse_connection, metadatabase_connection, dag_namespaces, data_source)
        except AdditionalNameAlreadyTaken as e:
            raise ValueError(
                f'A conflict occured while trying to create additional name "{e.name}". {namespace_error_mitigation_info}'
            ) from e
        finally:
            warehouse_connection.close()
            metadatabase_connection.close()

    return task


class DataSet:
    def __init__(
        self,
        customer,
        solution: Optional[str] = None,
        /,
        *,
        parser_rules=DEFAULT_PARSER_RULES,
        use_project_dbt_image=False,
        python_runner_image: Optional[str] = None,
        dbt_runner_image: Optional[str] = None,
        data_sources: Optional[Sequence[DataSource]] = None,
        schedule_interval: str = "@daily",
        create_additional_namespaces: bool = False,
        warehouse: str = "postgres",
    ):
        self.customer = customer
        self.solution = solution
        self.parser_rules = parser_rules
        self.python_runner_image = python_runner_image or project_runner_image(self.dag_id, "python")
        self.dbt_runner_image = dbt_runner_image or project_runner_image(self.dag_id, "dbt")
        if not data_sources:
            if not self.solution:
                raise ValueError(
                    "DataSet has no explicit data sources, but also no solution id to generate data source from."
                )
            self.data_sources = [
                DataSource(
                    id=self.solution,
                    s3prefix=f"{self.customer}/{self.solution}",
                    parser_rules=parser_rules,
                )
            ]
        else:
            data_source_ids = [data_source.id for data_source in data_sources]
            if not len(data_source_ids) == len(set(data_source_ids)):
                raise ValueError(f"DataSource ids need to be unique inside a DataSet, got {data_source_ids}")
            self.data_sources = data_sources
        self.schedule_interval = schedule_interval
        self.create_additional_namespaces = create_additional_namespaces
        self.warehouse = warehouse

    @property
    def dag_id(self):
        return customer_solution_id(self.customer, self.solution)

    def make_dag(self, additional_tasks: Sequence[Callable[["DataSet"], Operator]] = ()):
        if os.getenv("SKIP_MAKE_DAG", "0") == "1":
            return self
        dag = DAG(
            **make_dag_init_args(
                self.dag_id,
                description=f"Load {self.dag_id} data.",
                schedule_interval=self.schedule_interval,
            ),
            tags=[self.customer, self.solution] if self.solution else [self.customer],
        )

        with dag:
            dbt_seed = make_dbt_seed_operator(
                dbt_runner_image=self.dbt_runner_image,
                trigger_rule="none_failed",
            )
            dbt_run = make_dbt_operator(
                "dbt.run",
                command="run",
                dbt_runner_image=self.dbt_runner_image,
                trigger_rule="none_failed",
            )
            dbt_freshness = make_dbt_operator(
                "dbt.freshness",
                command="source freshness",
                dbt_runner_image=self.dbt_runner_image,
                warnings_to_errors=False,
                trigger_rule="none_failed",
            )
            dbt_test = make_dbt_operator(
                "dbt.test",
                command="test",
                dbt_runner_image=self.dbt_runner_image,
                warnings_to_errors=False,
                trigger_rule="none_failed",
            )
            if Config().RUN_LOCAL_AIRFLOW_EXTRACT_REMOTELY:

                def task():
                    trigger_extract_dag_and_wait(
                        self.customer,
                        self.dag_id,
                        self.data_sources,
                        self.create_additional_namespaces,
                        self.warehouse,
                    )

                extract_operator = make_python_operator("extract.remote", task)
                dbt_freshness << extract_operator

            else:
                for data_source in self.data_sources:
                    additional_namespaces = [self.customer] if self.create_additional_namespaces else ()
                    extract_operator = make_python_operator(
                        f"extract.{self.dag_id}_{data_source.id}",
                        extract_files_for_data_source(
                            NamespaceInfo(self.dag_id, additional_namespaces),
                            data_source,
                            self.warehouse,
                            self.dag_id,
                            NAMESPACE_ERROR_MITIGATION_INFO,
                        ),
                    )
                    dbt_freshness << extract_operator

            (dbt_freshness >> dbt_seed >> dbt_run >> dbt_test)
            previous_task = dbt_test
            if additional_tasks:
                for current_task in [current_task_generator(self) for current_task_generator in additional_tasks]:
                    previous_task >> current_task
                    previous_task = current_task

        return dag


def additional_dbt_run_task(identifier: str, select: str) -> Callable[[DataSet], Operator]:
    return lambda data_set: make_dbt_operator_with_select(
        f"dbt.run_{identifier}",
        command="run",
        select_exp=select,
        dbt_runner_image=data_set.dbt_runner_image,
        trigger_rule="none_failed",
    )


def additional_python_task(suffix: str, callable: Union[Callable[[], None], str]) -> Callable[[DataSet], Operator]:
    def result(data_set: DataSet):
        if isinstance(callable, str):
            return make_dockerized_python_operator(f"python.{suffix}", callable, data_set.python_runner_image)
        return make_python_operator(f"python.{suffix}", callable)

    return result


def additional_docker_task(
    suffix: str,
    image: str,
    command: Optional[str] = None,
    env: Mapping[str, str] = None,
) -> Callable[[DataSet], Operator]:
    def result(data_set: DataSet):
        return make_docker_operator(f"docker.{suffix}", image, command, env)

    return result
