from typing import Sequence
from urllib.parse import urlencode

import jsons

from polyteia.airflow_api import FAILED_STATE, SUCCESS_STATE, AirflowClient
from polyteia.config import Config
from polyteia.data_source import DataSource


def trigger_extract_dag(
    airflow_client, database, warehouse, customer, dag_id, data_sources, create_additional_namespaces=True
):
    additional_dag_namespaces = [customer] if create_additional_namespaces else ()
    conf = {
        "database": database,
        "warehouse": warehouse,
        "dag_namespace": dag_id,
        "data_sources": (jsons.dump(data_sources)),
        "additional_dag_namespaces": additional_dag_namespaces,
    }
    dag_run = airflow_client.trigger_dag_run(dag_id=EXTRACT_DAG_ID, conf=conf)
    return dag_run


def build_airflow_web_url_for_dag_run(host_name: str, dag_id: str, execution_date: str) -> str:
    query_params = {"dag_id": dag_id, "execution_date": execution_date}

    query_params_encoded = urlencode(query_params)
    return f"{host_name}/graph?{query_params_encoded}"


def build_airflow_web_url_for_task_run_logs(host_name: str, dag_id: str, execution_date: str) -> str:
    # the order of these dict elements matters for some reason
    query_params = {"dag_id": dag_id, "task_id": "extract_file_to_dev_db", "execution_date": execution_date}

    query_params_encoded = urlencode(query_params)
    return f"{host_name}/log?{query_params_encoded}"


EXTRACT_DAG_ID = "extract_to_dev_db"


class RemoteExtractTookTooLong(Exception):
    pass


class RemoteExtractFailed(Exception):
    pass


def trigger_extract_dag_and_wait(
    customer: str, dag_id: str, data_sources: Sequence[DataSource], create_additional_namespaces: bool, warehouse
) -> None:
    airflow_client = AirflowClient(
        Config().PRODUCTION_API_BASE,
        "trigger_dag_runs",
        Config().AIRFLOW_TRIGGER_DAG_RUNS_USER_PASSWORD,
    )

    dag_run = trigger_extract_dag(
        airflow_client,
        Config().STORE_PGDATABASE,
        warehouse,
        customer,
        dag_id,
        data_sources,
        create_additional_namespaces,
    )
    dag_run_id = dag_run["dag_run_id"]
    task_log_web_url = build_airflow_web_url_for_task_run_logs(
        Config().PRODUCTION_URL, EXTRACT_DAG_ID, dag_run["execution_date"]
    )
    print(f"DAG run triggered, you can see the task logs at {task_log_web_url}.")
    dag_state_after_finish_waiting = airflow_client.wait_for_dag_run(EXTRACT_DAG_ID, dag_run_id, 360, 10)
    print("==================================")
    print("Start of remote DAG run log output")
    print("==================================")
    for line in airflow_client.get_logs_of_dag_run(EXTRACT_DAG_ID, dag_run_id).splitlines():
        print(f"==== {line}")
    print("==================================")
    print("End of remote DAG run log output")
    print("==================================")
    if dag_state_after_finish_waiting is None:
        raise RemoteExtractTookTooLong()
    if dag_state_after_finish_waiting == FAILED_STATE:
        raise RemoteExtractFailed()
    if dag_state_after_finish_waiting == SUCCESS_STATE:
        print("Remote extract run succeeded.")
