import os
from pathlib import Path


def _from_env(key, default, required, parser=lambda x: x):
    v = os.environ.get(key)
    if v is None:
        if required:
            raise ValueError(f"Missing required config variable {key}")
        else:
            return default
    else:
        return parser(v)


def as_dirpath(key, default=None, required=False):
    return _from_env(key, default, required, parser=lambda s: Path(s.rstrip("/")))


def as_str(key, default=None, required=False):
    return _from_env(key, default, required)


def as_bool(key, default=False, required=False):
    return _from_env(key, default, required, parser=lambda s: s.lower() in ["yes", "1", "true"])


def as_list(key, default=None, required=False):
    if default is None:
        default = []
    return _from_env(key, default, required, parser=lambda s: [elem.strip() for elem in s.split(",")])


class Config:
    def __init__(self):
        self.LOCAL_TESTING = as_str("LOCAL_TESTING")
        self.SFTP_AUTH_CONFIGS = as_str("DP_SFTP_AUTH_CONFIGS", default="{}")
        self.S3_INGESTION_KEY = as_str("S3_INGESTION_KEY", default="")
        self.S3_INGESTION_SECRET = as_str("S3_INGESTION_SECRET", default="")
        self.COLLECT_INGESTION_KEY = as_str("COLLECT_INGESTION_KEY", default="")
        self.COLLECT_INGESTION_USER = as_str("COLLECT_INGESTION_USER", default="")
        self.COLLECT_INGESTION_PW = as_str("COLLECT_INGESTION_PW", default="")

        self.BASE_DIR = as_str("DP_BASE_DIR")
        self.DP_ROOT = as_dirpath("DP_ROOT")
        self.SLACK_WEBHOOK_URL = as_str("DP_SLACK_WEBHOOK_URL")

        self.LOG_ENDPOINT = as_str("DP_LOG_ENDPOINT")
        self.FAIL_FAST = as_bool("DP_FAIL_FAST")

        self.DBT_IMAGE = as_str("DP_DBT_IMAGE")
        self.HOST_DBT_DIR = as_str("DP_HOST_DBT_DIR")
        self.HOST_EXTERNAL_PROJECT_DIR = as_str("DP_HOST_EXTERNAL_PROJECT_DIR")

        self.PYTHON_IMAGE = as_str("DP_PYTHON_IMAGE")

        self.AWS_ACCESS_KEY_ID = as_str("AWS_ACCESS_KEY_ID")
        self.AWS_SECRET_ACCESS_KEY = as_str("AWS_SECRET_ACCESS_KEY")
        self.S3_ENDPOINT_URL = as_str("DP_S3_ENDPOINT_URL")
        self.S3_BUCKET = as_str("DP_S3_BUCKET")

        self.STORE_PGHOST = as_str("DP_STORE_PGHOST", required=True)
        self.STORE_PGPORT = as_str("DP_STORE_PGPORT", default=5432)
        self.STORE_PGDATABASE = as_str("DP_STORE_PGDATABASE", default="postgres")
        self.STORE_PGUSER = as_str("DP_STORE_PGUSER", default="postgres")
        self.STORE_PGPASSWORD = as_str("DP_STORE_PGPASSWORD", required=True)

        self.DEV_DB_PGHOST = as_str("DP_DEV_DB_PGHOST")
        self.DEV_DB_EXTRACT_PGUSER = as_str("DP_DEV_DB_EXTRACT_PGUSER")
        self.DEV_DB_EXTRACT_PGPASSWORD = as_str("DP_DEV_DB_EXTRACT_PGPASSWORD")
        self.DEV_DB_DATABASE = as_str("DEV_DB_DATABASE")

        self.DEV_STORE_TRINO_USER = as_str("DP_DEV_STORE_TRINO_USER")
        self.DEV_STORE_TRINO_HOST = as_str("DP_DEV_STORE_TRINO_HOST")
        self.DEV_STORE_TRINO_PORT = as_str("DP_DEV_STORE_TRINO_PORT")
        self.DEV_STORE_TRINO_ICEBERG_CATALOG = as_str("DP_DEV_STORE_TRINO_ICEBERG_CATALOG", default="lakehouse")
        self.DEV_STORE_TRINO_HIVE_CATALOG = as_str("DP_DEV_STORE_TRINO_HIVE_CATALOG", default="hive")
        self.DEV_STORE_TRINO_S3_BUCKET = as_str("DP_DEV_STORE_TRINO_S3_BUCKET")
        self.DEV_STORE_TRINO_S3_ACCESS_KEY = as_str("DP_DEV_STORE_TRINO_S3_ACCESS_KEY")
        self.DEV_STORE_TRINO_S3_SECRET_ACCESS_KEY = as_str("DP_DEV_STORE_TRINO_S3_SECRET_ACCESS_KEY")
        self.DEV_STORE_TRINO_S3_ENDPOINT_URL = as_str("DP_DEV_STORE_TRINO_S3_ENDPOINT_URL")

        self.STORE_TRINO_USER = as_str("DP_STORE_TRINO_USER", default="admin")
        self.STORE_TRINO_HOST = as_str("DP_STORE_TRINO_HOST", required=True)
        self.STORE_TRINO_PORT = as_str("DP_STORE_TRINO_PORT")
        self.STORE_TRINO_ICEBERG_CATALOG = as_str("DP_STORE_TRINO_ICEBERG_CATALOG", default="lakehouse")
        self.STORE_TRINO_HIVE_CATALOG = as_str("DP_STORE_TRINO_HIVE_CATALOG", default="hive")
        self.STORE_TRINO_SCHEMA = as_str("DP_STORE_TRINO_SCHEMA")
        self.STORE_TRINO_S3_BUCKET = as_str("DP_STORE_TRINO_S3_BUCKET")
        self.STORE_TRINO_S3_ACCESS_KEY = as_str("DP_STORE_TRINO_S3_ACCESS_KEY")
        self.STORE_TRINO_S3_SECRET_ACCESS_KEY = as_str("DP_STORE_TRINO_S3_SECRET_ACCESS_KEY")
        self.STORE_TRINO_S3_ENDPOINT_URL = as_str("DP_STORE_TRINO_S3_ENDPOINT_URL")

        self.DOCKER_NETWORK = as_str("DP_DOCKER_NETWORK", required=True)

        self.REGISTRY_USER = as_str("DP_REGISTRY_USER")
        self.REGISTRY_PASSWORD = as_str("DP_REGISTRY_PASSWORD")
        self.GITLAB_REPO_USER = as_str("DP_GITLAB_REPO_USER")
        self.GITLAB_REPO_PASSWORD = as_str("DP_GITLAB_REPO_PASSWORD")
        self.EXTRA_INDEX_URL = as_str("DP_EXTRA_INDEX_URL_GITLAB_PACKAGE", default="")

        self.REGISTRY_BASE_URL = as_str("DP_REGISTRY_BASE_URL", default="registry.gitlab.com/polyteia/data-pipelines")

        self.PRODUCTION_API_BASE = as_str("DP_PRODUCTION_API_BASE")
        self.PRODUCTION_URL = as_str("DP_PRODUCTION_URL")
        self.AIRFLOW_TRIGGER_DAG_RUNS_USER_PASSWORD = as_str("AIRFLOW_TRIGGER_DAG_RUNS_USER_PASSWORD")
        self.RUN_LOCAL_AIRFLOW_EXTRACT_REMOTELY = as_bool("RUN_LOCAL_AIRFLOW_EXTRACT_REMOTELY")

    @property
    def has_dev_db_access(self):
        if self.DEV_DB_EXTRACT_PGPASSWORD:
            return True
        return False
