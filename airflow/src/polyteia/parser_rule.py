import abc
import re
from dataclasses import dataclass
from pathlib import Path
from typing import Iterator

from polyteia.store import load_data_from_csv, load_data_from_json

JSON_PARSER_RULE_TYPE = "json_parser_rule"
CSV_PARSER_RULE_TYPE = "csv_parser_rule"


# For some reason, making this a @classmethod makes jsons.dump
# include the method in the dump, asdict does not
def rule_from_dict(d: dict) -> "ParserRule":
    if d["type"] == CSV_PARSER_RULE_TYPE:
        return CsvParserRule(**d)
    if d["type"] == JSON_PARSER_RULE_TYPE:
        return JsonParserRule(**d)
    else:
        raise ValueError(f"Cannot deserialize unknown type {d['type']}")


@dataclass
class ParserRule(abc.ABC):
    pattern: str = ".*"

    def matches(self, file_name) -> bool:
        return bool(re.match(self.pattern, file_name))

    @abc.abstractmethod
    def load_data_from_file(self, directory: Path, file_name: str) -> Iterator[str]:
        pass


@dataclass
class CsvParserRule(ParserRule):
    has_headers: bool = True
    separator: str = ";"
    encoding: str = "utf-8"
    type: str = CSV_PARSER_RULE_TYPE
    ensure_ascii: bool = True

    def load_data_from_file(self, directory: Path, file_name: str) -> Iterator[str]:
        return load_data_from_csv(
            directory / file_name, self.has_headers, self.separator, self.encoding, self.ensure_ascii
        )


@dataclass
class JsonParserRule(ParserRule):
    type: str = JSON_PARSER_RULE_TYPE

    def load_data_from_file(self, directory: Path, file_name: str) -> Iterator[str]:
        return load_data_from_json(directory / file_name)


DEFAULT_PARSER_RULES = (
    CsvParserRule(pattern=".*\\.csv"),
    JsonParserRule(pattern=".*\\.json"),
)
