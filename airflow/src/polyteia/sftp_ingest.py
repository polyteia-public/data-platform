import datetime as dt
import os
import re
import tempfile
from stat import S_ISDIR
from typing import Any

import boto3
import yaml
from paramiko import (
    AuthenticationException,
    AutoAddPolicy,
    BadHostKeyException,
    SSHClient,
    SSHException,
)

from polyteia.config import Config

SFTP_AUTH_CONFIGS = yaml.safe_load(Config().SFTP_AUTH_CONFIGS)
S3_KEY = Config().S3_INGESTION_KEY
S3_SECRET = Config().S3_INGESTION_SECRET
S3_ENDPOINT = Config().S3_ENDPOINT_URL
S3_BUCKET = Config().S3_BUCKET


def get_run_environment() -> str:
    return "dev" if Config().HOST_EXTERNAL_PROJECT_DIR else "production"


def create_tmp_download_directory(tmp_dir_str: str, save_file_destination: str) -> None:
    if not os.path.isdir(f"{tmp_dir_str}/{save_file_destination}"):
        os.makedirs(f"{tmp_dir_str}/{save_file_destination}", exist_ok=True)


def strip_forbidden_characters(string_item: str) -> str:
    regex_find = re.findall(r"[0-9a-zA-Z\-:+/_.]", string_item)
    return "".join(regex_find)


def is_directory(sftp_file: Any) -> bool:
    file_mode = sftp_file.st_mode
    return S_ISDIR(file_mode)


def recursive_download(run_environment: str, sftp_source_path: str, sftp_client: Any, save_file_destination: str):
    sftp_item_list = sftp_client.listdir_attr(path=sftp_source_path)

    for sftp_source_file in sftp_item_list:
        full_sftp_source_path = sftp_source_path + sftp_source_file.filename
        filename_cleaned = strip_forbidden_characters(sftp_source_file.filename)
        download_destination_cleaned = strip_forbidden_characters(save_file_destination)
        if is_directory(sftp_source_file):
            recursive_download(
                run_environment,
                f"{full_sftp_source_path}/",
                sftp_client,
                f"{download_destination_cleaned}/{filename_cleaned}",
            )

        else:
            with tempfile.TemporaryDirectory() as tmp_dir_str:
                filename_cleaned = strip_forbidden_characters(sftp_source_file.filename)
                download_destination_cleaned = strip_forbidden_characters(save_file_destination)
                create_tmp_download_directory(tmp_dir_str, download_destination_cleaned)
                local_download_directory = f"{tmp_dir_str}/{download_destination_cleaned}/{filename_cleaned}"

                print(f"Downloading {sftp_source_file}")
                sftp_client.get(f"{sftp_source_path}/{sftp_source_file.filename}", local_download_directory)

                upload_file_to_s3(run_environment, local_download_directory)


def remove_old_source_files(sftp_source_path: str, sftp_client: Any):
    sftp_item_list = sftp_client.listdir_attr(path=sftp_source_path)
    for sftp_source_file in sftp_item_list:
        full_sftp_source_path = sftp_source_path + sftp_source_file.filename
        if is_directory(sftp_source_file):
            remove_old_source_files(f"{full_sftp_source_path}/", sftp_client)

        else:
            file_last_modified = sftp_source_file.st_mtime
            now = dt.datetime.today()
            days_delta = now - dt.datetime.fromtimestamp(file_last_modified)
            if days_delta.days >= 14:
                print(f"Deleting sftp source file {sftp_source_path}{sftp_source_file.filename}")
                print(f"The file is {days_delta.days} days old.")
                sftp_client.remove(f"{sftp_source_path}{sftp_source_file.filename}")


def upload_file_to_s3(run_environment: str, local_download_directory: str) -> None:
    s3_upload_destination = local_download_directory.split("/")
    s3_upload_destination = "/".join(s3_upload_destination[3:])

    if run_environment == "dev":
        s3_destination = f"dev/{s3_upload_destination}"
    elif run_environment == "production":
        s3_destination = f"{s3_upload_destination}"

    print(f"Uploading to s3 destination: {s3_destination}")

    session = boto3.Session(
        aws_access_key_id=S3_KEY,
        aws_secret_access_key=S3_SECRET,
    )
    s3 = session.resource("s3", endpoint_url=S3_ENDPOINT)
    s3.meta.client.upload_file(local_download_directory, S3_BUCKET, s3_destination)


def sftp_authenticate(ingest_config: dict) -> Any:
    temp = tempfile.NamedTemporaryFile()
    try:
        host = ingest_config["host"]
        sftp_auth_config = SFTP_AUTH_CONFIGS.get(host)
        if not sftp_auth_config:
            raise ValueError(f"No auth config registered for '{host}'. Registered hosts: {SFTP_AUTH_CONFIGS.keys()}")
        temp.write(bytes(sftp_auth_config["ssh_key"], encoding="utf-8"))
        temp.seek(0)
        ssh_client = SSHClient()
        ssh_client.set_missing_host_key_policy(AutoAddPolicy())
        ssh_client.connect(
            host,
            username=sftp_auth_config["username"],
            key_filename=temp.name,
            password=sftp_auth_config.get("password"),
            port=sftp_auth_config.get("port", 22),
        )
    except AuthenticationException:
        print("Authentication failed, please verify your credentials: %s")
    except SSHException as sshException:
        print(f"Unable to establish SSH connection: {sshException}")
    except BadHostKeyException as badHostKeyException:
        print("Unable to verify server's host key: %s" % badHostKeyException)
        print("Unable to connect to paramiko using ssh key")
    finally:
        temp.close()
    return ssh_client.open_sftp()


def sftp_to_s3(ingest_config: dict, customer: str, solution: str) -> None:
    run_environment = get_run_environment()
    timestamp = dt.datetime.now().isoformat()
    sftp_client = sftp_authenticate(ingest_config)
    s3_destination = f"{customer}/{solution}/landing/{timestamp}"
    recursive_download(run_environment, ingest_config["sftp_path"], sftp_client, s3_destination)
    if "remove_old_source_files" in ingest_config and ingest_config["remove_old_source_files"] is True:
        remove_old_source_files(ingest_config["sftp_path"], sftp_client)
