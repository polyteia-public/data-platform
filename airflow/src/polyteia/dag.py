import json
import os
import re
from pathlib import Path
from typing import Callable, Iterable, Mapping, Optional, Union

import yaml
from airflow import DAG
from airflow.models import Operator
from airflow.models.dag import DagContext

from polyteia.config import Config
from polyteia.data_source import DataSource
from polyteia.dataset import extract_files_for_data_source, make_dag_init_args
from polyteia.dbt import make_dbt_operator
from polyteia.docker import project_runner_image
from polyteia.extract_remotely import trigger_extract_dag_and_wait
from polyteia.identifier_utils import customer_solution_id
from polyteia.namespace import NamespaceInfo
from polyteia.python import (
    make_dockerized_python_operator,
    make_python_operator,
)
from polyteia.util import merge_dicts


class PolyteiaDAG(DAG):
    def __init__(
        self,
        customer: str,
        solution: Optional[str] = None,
        description: str = "",
        schedule_interval: str = "@daily",
        dbt_runner_image: Optional[str] = None,
        python_runner_image: Optional[str] = None,
        create_additional_namespaces: bool = False,
        warehouse: str = "postgres",
    ):
        super().__init__(
            **make_dag_init_args(
                customer_solution_id(customer, solution),
                description=description,
                schedule_interval=schedule_interval,
            ),
            tags=[],
        )
        self.customer = customer
        self.solution = solution
        self.dbt_runner_image = dbt_runner_image or project_runner_image(self.dag_id, "dbt")
        self.python_runner_image = python_runner_image or project_runner_image(self.dag_id, "python")
        self.create_additional_namespaces = create_additional_namespaces
        self.warehouse = warehouse
        self.data_sources = []


def extract(data_source: DataSource) -> Operator:
    dag = DagContext.get_current_dag()
    if dag.get_latest_execution_date():
        execution_date = dag.get_latest_execution_date()
        conf = dag.get_dagrun(execution_date=execution_date).conf
    else:
        conf = None

    # example conf: {"incremental_extract_date_range":["2021-06-23", "2021-06-25"]}
    if conf and "incremental_extract_date_range" in conf:
        date_range = (conf["incremental_extract_date_range"][0], conf["incremental_extract_date_range"][1])
        data_source.incremental_extract_date_range = date_range

    if not isinstance(dag, PolyteiaDAG):
        raise ValueError(f"invalid dag type: {dag}")
    additional_namespaces = [dag.customer] if dag.create_additional_namespaces else ()
    namespaces = NamespaceInfo(
        primary_namespace=dag.dag_id,
        additional_namespaces=additional_namespaces,
    )

    dag.data_sources.append(data_source)

    namespace_error_mitigation_info = (
        "Failed to create additional table names for compatibility. If you are sure that your DAG is using the latest "
        "naming convention you can disable the creation of additional table names by setting "
        "create_additional_namespaces to False: extract(..., create_additional_namespaces=False)."
    )
    if Config().RUN_LOCAL_AIRFLOW_EXTRACT_REMOTELY:

        def task():
            trigger_extract_dag_and_wait(
                dag.customer, dag.dag_id, [data_source], dag.create_additional_namespaces, dag.warehouse
            )

        return make_python_operator(f"extract.{data_source.id}.remote", task)
    else:
        return make_python_operator(
            f"extract.{data_source.id}",
            extract_files_for_data_source(
                namespaces, data_source, dag.warehouse, dag.dag_id, namespace_error_mitigation_info
            ),
        )


def dbt(command: str, id: Optional[str] = None, dbt_vars: dict = None, warnings_to_errors: bool = False) -> Operator:
    dag = DagContext.get_current_dag()
    if not isinstance(dag, PolyteiaDAG):
        raise ValueError(f"invalid dag type: {dag}")
    id = id or re.sub(r"[^\w.-]", "_", command)

    dbt_vars = dbt_vars or {}

    dbt_vars["dag_id"] = dag.dag_id
    dbt_vars_json = json.dumps(dbt_vars)

    if any([c in dbt_vars_json for c in "`'\\"]):
        raise ValueError("dbt_vars contains forbidden character (', `, \\).")

    command += f" --vars '{dbt_vars_json}'"

    return make_dbt_operator(
        f"dbt.{id}",
        command,
        dbt_runner_image=dag.dbt_runner_image,
        trigger_rule="none_failed",
        warnings_to_errors=warnings_to_errors,
    )


def python(filename: str, id: Optional[str] = None, args: Optional[Union[dict, Iterable]] = None) -> Operator:
    dag = DagContext.get_current_dag()
    if not isinstance(dag, PolyteiaDAG):
        raise ValueError(f"invalid dag type: {dag}")
    filename_without_extension = filename[0:-3] if filename.endswith(".py") else filename
    id = id or re.sub(r"[^\w.-]", "_", filename_without_extension)
    return make_dockerized_python_operator(
        f"python.{id}", filename, python_runner_image=dag.python_runner_image, python_args=args
    )


def generate_dags(create_dag: Callable[[str, dict], DAG], dag_file_path: str) -> Mapping[str, DAG]:
    config_file_name = "customers.yml" if Config().HOST_EXTERNAL_PROJECT_DIR else f"{Path(dag_file_path).stem}.yml"
    CONFIG_FILE = Path(os.getenv("DP_BULK_CONFIGS_DIR")) / config_file_name

    with open(CONFIG_FILE, "r") as f:
        config = yaml.safe_load(f)

    default_config = config.get("default", {})

    dags = {}
    customers_with_custom_config = [x for x in config.keys() if x not in ["customers", "default"]]
    customers = set(customers_with_custom_config + config.get("customers", []))
    for customer_name in customers:
        customer_config = config.get(customer_name)
        final_config = merge_dicts(customer_config, default_config) if customer_config else default_config
        dag = create_dag(customer_name, final_config)
        dags[dag.dag_id] = dag

    return dags
