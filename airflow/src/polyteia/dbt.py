from typing import Optional

from polyteia.config import Config
from polyteia.docker import make_docker_operator

DBT_COMMAND = "dbt --no-use-colors"


def make_dbt_seed_operator(dbt_runner_image: str = None, **kwargs):
    seeds_directory = "seeds"
    # This looks like multi-line shell, but it will be passed to bash as a one-line string, so you need ";"s
    full_command = f"""
        if [ ! -d '{seeds_directory}' ];
        then
            echo 'No seeds directory ({seeds_directory}) found, skipping!';
            exit 0;
        fi;
        {DBT_COMMAND} seed;
        """

    return make_dbt_operator(
        "dbt.automatic-seed",
        command="",
        full_command=full_command,
        warnings_to_errors=True,
        dbt_runner_image=dbt_runner_image,
        **kwargs,
    )


def make_dbt_operator_with_select(task_id: str, command: str, select_exp: str, dbt_runner_image: str, **kwargs):
    return make_dbt_operator(
        task_id,
        command="",
        full_command=f"{DBT_COMMAND} {command} --select {select_exp}",
        dbt_runner_image=dbt_runner_image,
        **kwargs,
    )


def make_dbt_operator(
    task_id: str,
    command: str,
    full_command: Optional[str] = None,
    dbt_runner_image: str = None,
    warnings_to_errors: bool = True,
    **kwargs,
):
    additional_docker_args = ""

    if command and full_command:
        raise ValueError(f"Cannot use command and full_command together. ({command=}, {full_command=})")

    if Config().HOST_EXTERNAL_PROJECT_DIR:
        # running in local data platform with external project
        dbt_image = Config().DBT_IMAGE
        external_project_dir = Config().HOST_EXTERNAL_PROJECT_DIR
        additional_docker_args += f" -v {external_project_dir}/dbt:/opt/dp/dbt"
        additional_docker_args += f" -v {external_project_dir}/.dbt/dbt_modules:/opt/dp/dbt/dbt_modules"
        additional_docker_args += f" -v {external_project_dir}/.dbt/target:/opt/dp/dbt/target"

    else:
        # running in deployed data platform
        dbt_image = dbt_runner_image
        additional_docker_args += " --pull always"

    warnings_to_errors_option = "--warn-error" if warnings_to_errors else ""
    if full_command:
        effective_command = f'bash -c "{DBT_COMMAND} deps && {full_command}"'
    else:
        effective_command = f'bash -c "{DBT_COMMAND} deps && {DBT_COMMAND} {warnings_to_errors_option} {command}"'

    env = {
        "DP_STORE_PGHOST": Config().STORE_PGHOST,
        "DP_STORE_PGPORT": Config().STORE_PGPORT,
        "DP_STORE_PGDATABASE": Config().STORE_PGDATABASE,
        "DP_STORE_PGUSER": Config().STORE_PGUSER,
        "DP_STORE_PGPASSWORD": Config().STORE_PGPASSWORD,
        "DEV_DB_DATABASE": Config().DEV_DB_DATABASE,
        "DBT_ENV_SECRET_GITLAB_REPO_CREDENTIALS": f"{Config().GITLAB_REPO_USER}:{Config().GITLAB_REPO_PASSWORD}",
        "DP_STORE_TRINO_HOST": Config().STORE_TRINO_HOST,
        "DP_STORE_TRINO_PORT": Config().STORE_TRINO_PORT,
        "DP_STORE_TRINO_ICEBERG_CATALOG": Config().STORE_TRINO_ICEBERG_CATALOG,
        "DP_STORE_TRINO_HIVE_CATALOG": Config().STORE_TRINO_HIVE_CATALOG,
        "DP_STORE_TRINO_USER": Config().STORE_TRINO_USER,
        "DP_STORE_TRINO_SCHEMA": Config().STORE_TRINO_SCHEMA,
    }

    return make_docker_operator(
        task_id=task_id,
        image=dbt_image,
        command=effective_command,
        env={key: value for key, value in env.items() if value is not None},
        additional_docker_args=additional_docker_args,
        **kwargs,
    )
