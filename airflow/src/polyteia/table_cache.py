from dataclasses import dataclass
from datetime import datetime, timezone
from typing import Optional

import jsons as jsons
import sqlalchemy
from sqlalchemy import (
    Column,
    DateTime,
    MetaData,
    PrimaryKeyConstraint,
    String,
    delete,
    select,
)
from sqlalchemy.dialects.postgresql import JSONB
from sqlalchemy.orm import declarative_base, sessionmaker

from polyteia.config import Config
from polyteia.parser_rule import ParserRule

MARKER_SCHEMA = "extract"
MARKER_TABLE_NAME = "_table_last_updated"
FILE_E_TAG_COLUMN_NAME = "file_e_tag"

Base = declarative_base(metadata=MetaData(schema=MARKER_SCHEMA, quote_schema=True))


class CacheTableEntry(Base):
    __tablename__ = MARKER_TABLE_NAME

    __table_args__ = (PrimaryKeyConstraint("table_name", "exported_at"),)

    table_name = Column(String())
    exported_at = Column(DateTime(timezone=True))
    parser_rule = Column(JSONB)
    file_last_modified = Column(DateTime(timezone=True))
    file_e_tag = Column(String(length=64))


@dataclass
class MetadatabaseTransaction:
    session: sqlalchemy.orm.Session
    session_transaction: sqlalchemy.orm.SessionTransaction = None

    def __enter__(self) -> "MetadatabaseTransaction":
        self.session_transaction = self.session.begin().__enter__()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb) -> None:
        self.session_transaction.__exit__(exc_type, exc_val, exc_tb)

    def delete_cache_entries(self, table_name: str, exported_at: Optional[str] = None):
        if exported_at:
            statement = (
                delete(CacheTableEntry)
                .where(CacheTableEntry.table_name == table_name)
                .where(CacheTableEntry.exported_at != to_utc(exported_at))
            )
        else:
            statement = delete(CacheTableEntry).where(CacheTableEntry.table_name == table_name)
        self.session.execute(statement)

    def table_is_up_to_date(
        self,
        table_name: str,
        exported_at: datetime,
        parser_rule: ParserRule,
        file_last_modified: datetime,
        file_e_tag: str,
    ) -> bool:
        query = (
            select(CacheTableEntry)
            .where(CacheTableEntry.table_name == table_name)
            .where(CacheTableEntry.exported_at == to_utc(exported_at))
            .where(CacheTableEntry.parser_rule == jsons.dump(parser_rule))
            .where(
                ((CacheTableEntry.file_e_tag.is_not(None)) & (CacheTableEntry.file_e_tag == file_e_tag))
                | ((CacheTableEntry.file_e_tag.is_(None)) & (CacheTableEntry.file_last_modified >= file_last_modified))
            )
        )
        result = self.session.execute(query).fetchone()

        print(
            f"Looking for cache entry for {table_name=}, {to_utc(exported_at)=}, parser_rule={jsons.dump(parser_rule)}, {file_last_modified=}, {file_e_tag=}"
        )
        if result is None:
            print("No up-to-date cache entry exists. Needs update.")
            return False
        print("Found up-to-date cache entry. No need to update.")
        return True

    def update_table_cache_entry(
        self,
        table_name: str,
        exported_at: datetime,
        parser_rule: ParserRule,
        file_last_modified: datetime,
        file_e_tag: str,
    ) -> None:
        print(
            f"""Updating table cache entry for {table_name=}, {to_utc(exported_at)=}, parser_rule={jsons.dump(parser_rule)}, {file_last_modified=}, {file_e_tag=}"""
        )
        entry = CacheTableEntry(
            table_name=table_name,
            exported_at=to_utc(exported_at),
            parser_rule=jsons.dump(parser_rule),
            file_last_modified=file_last_modified,
            file_e_tag=file_e_tag,
        )
        entry = self.session.merge(entry)
        self.session.add(entry)


class MetadatabaseConnection:
    def __init__(
        self, database: str, host: str, user: str, password: Optional[str] = None, port: Optional[str] = "5432"
    ):
        if password:
            connection_string = f"postgresql+psycopg2://{user}:{password}@{host}:{port}/{database}"
        else:
            connection_string = f"postgresql+psycopg2://{user}@{host}:{port}/{database}"

        self._engine: sqlalchemy.engine.Engine = sqlalchemy.create_engine(connection_string)

    def transaction(self) -> MetadatabaseTransaction:
        return MetadatabaseTransaction(sessionmaker(self._engine)())

    def close(self):
        pass


def to_utc(d: datetime) -> datetime:
    if d.tzinfo:
        return d.astimezone(timezone.utc)
    else:
        return d.replace(tzinfo=timezone.utc)


def connect_to_main_metadatabase() -> MetadatabaseConnection:
    return MetadatabaseConnection(
        host=Config().STORE_PGHOST,
        database=Config().STORE_PGDATABASE,
        user=Config().STORE_PGUSER,
        password=Config().STORE_PGPASSWORD,
        port=Config().STORE_PGPORT,
    )


def connect_to_dev_metadatabase(database: str) -> MetadatabaseConnection:
    return MetadatabaseConnection(
        database=database,
        host=Config().DEV_DB_PGHOST,
        user=Config().DEV_DB_EXTRACT_PGUSER,
        password=Config().DEV_DB_EXTRACT_PGPASSWORD,
    )
