import time
from dataclasses import InitVar, dataclass
from json import JSONDecodeError
from typing import Literal, Optional, Sequence

import requests
from requests import Response
from requests.auth import HTTPBasicAuth

FAILED_STATE = "failed"
SUCCESS_STATE = "success"


class NoDagRunsFound(Exception):
    pass


@dataclass
class AirflowClient:
    base_url: str
    username: InitVar[str]
    password: InitVar[str]

    def __post_init__(self, username, password):
        self.basic_auth = HTTPBasicAuth(username, password)

    def _request(self, verb: str, path: str, params: dict = None, json: dict = None) -> Response:
        params = params or {}
        response = requests.request(
            verb,
            f"{self.base_url}{path}",
            auth=self.basic_auth,
            headers={"Content-type": "application/json"},
            params=params,
            json=json,
        )
        response.raise_for_status()
        return response

    def _get(self, path: str, params: dict = None) -> dict:
        try:
            response = self._request(verb="GET", path=path, params=params)
            return response.json()
        except JSONDecodeError:
            raise ValueError(f"airflow returned unexpected non-json value: {response.text}")

    def _get_plain(self, path: str, params: dict = None) -> str:
        return self._request(verb="GET", path=path, params=params).text

    def _post(self, path: str, json: dict = None) -> dict:
        try:
            response = self._request(verb="POST", path=path, json=json)
            return response.json()
        except JSONDecodeError:
            raise ValueError(f"airflow returned unexpected non-json value: {response.text}")

    def trigger_dag_run(self, dag_id: str, conf: dict = None) -> dict:
        conf = conf or {}
        trigger_result = self._post(f"/dags/{dag_id}/dagRuns", json={"conf": conf})
        if trigger_result["state"] not in ("running", "queued", "success", "failed"):
            raise ValueError(f"unexpected JSON response: {trigger_result}")
        else:
            return trigger_result

    def wait_for_dag_run(
        self, dag_id: str, dag_run_id: str, check_times: int = 30, check_interval: int = 10
    ) -> Optional[Literal[SUCCESS_STATE, FAILED_STATE]]:
        print(
            f"Waiting a maximum of {check_times*check_interval} seconds for remote extraction DAG run {dag_id}, {dag_run_id} to complete"
        )
        for i in range(1, check_times + 1):
            try:
                print(f"Checking, try {i}/{check_times}... ", end="")
                dag_state = self.get_dag_run(dag_id, dag_run_id)["state"]
                if dag_state == "running":
                    print("still running...")
                elif dag_state == "success":
                    print("succeeded!")
                else:
                    print(f"Unknown state: {dag_state}")
                if dag_state not in ("running", "queued"):
                    return dag_state
            except NoDagRunsFound:
                pass
            time.sleep(check_interval)

    def get_dag_run(self, dag_id: str, dag_run_id: str) -> dict:
        return self._get(f"/dags/{dag_id}/dagRuns/{dag_run_id}")

    def get_task_instances(self, dag_id: str, dag_run_id: str) -> Sequence[dict]:
        return self._get(f"/dags/{dag_id}/dagRuns/{dag_run_id}/taskInstances")["task_instances"]

    def get_task_logs(self, dag_id: str, dag_run_id: str, task_instance: dict) -> str:
        task_id = task_instance["task_id"]
        try_number = task_instance["try_number"]
        url = f"/dags/{dag_id}/dagRuns/{dag_run_id}/taskInstances/{task_id}/logs/{try_number}"
        return self._get_plain(url, params={"full_content": True})

    def get_logs_of_dag_run(self, dag_id: str, dag_run_id: str) -> str:
        task_instances = self.get_task_instances(dag_id, dag_run_id)
        full_log = ""
        for task_instance in task_instances:
            full_log += self.get_task_logs(dag_id, dag_run_id, task_instance)
        return full_log
