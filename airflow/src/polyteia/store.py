import csv
import json
from datetime import datetime
from pathlib import Path
from typing import (
    Generator,
    Iterable,
    Iterator,
    List,
    Optional,
    Sequence,
    TextIO,
    Tuple,
)

import psycopg2
import psycopg2.extras
import sqlalchemy

from polyteia.config import Config
from polyteia.transaction import (
    PsycopgConnection,
    Transaction,
    TrinoConnection,
)

EXTRACT_SCHEMA = "extract"


def connect_to_main_db() -> PsycopgConnection:
    return PsycopgConnection(
        psycopg2.connect(
            dbname=Config().STORE_PGDATABASE,
            host=Config().STORE_PGHOST,
            port=Config().STORE_PGPORT,
            user=Config().STORE_PGUSER,
            password=Config().STORE_PGPASSWORD,
            application_name="airflow_extract",
        )
    )


def connect_to_dev_db(database: str) -> PsycopgConnection:
    return PsycopgConnection(
        psycopg2.connect(
            dbname=database,
            host=Config().DEV_DB_PGHOST,
            user=Config().DEV_DB_EXTRACT_PGUSER,
            password=Config().DEV_DB_EXTRACT_PGPASSWORD,
            application_name="airflow_extract",
        )
    )


def connect_to_trino(hive_schema: str) -> TrinoConnection:
    return TrinoConnection(
        sqlalchemy.create_engine(
            f"trino://"
            f"{Config().STORE_TRINO_USER}"
            f"@{Config().STORE_TRINO_HOST}"
            f":{Config().STORE_TRINO_PORT}"
            f"/{Config().STORE_TRINO_HIVE_CATALOG}"
        ),
        Config().STORE_TRINO_S3_BUCKET,
        Config().STORE_TRINO_S3_ACCESS_KEY,
        Config().STORE_TRINO_S3_SECRET_ACCESS_KEY,
        Config().STORE_TRINO_S3_ENDPOINT_URL,
        hive_schema=hive_schema,
    )


def connect_to_dev_trino(hive_schema: str) -> TrinoConnection:
    return TrinoConnection(
        sqlalchemy.create_engine(
            f"trino://"
            f"{Config().DEV_STORE_TRINO_USER}"
            f"@{Config().DEV_STORE_TRINO_HOST}"
            f":{Config().DEV_STORE_TRINO_PORT}"
            f"/{Config().DEV_STORE_TRINO_HIVE_CATALOG}"
        ),
        Config().DEV_STORE_TRINO_S3_BUCKET,
        Config().DEV_STORE_TRINO_S3_ACCESS_KEY,
        Config().DEV_STORE_TRINO_S3_SECRET_ACCESS_KEY,
        Config().DEV_STORE_TRINO_S3_ENDPOINT_URL,
        hive_schema=hive_schema,
    )


def tables_exist(table_names, schema_name="public"):
    conn = connect_to_main_db()
    try:
        with conn.transaction() as transaction:
            results = {}
            for table_name in table_names:
                parts = table_name.split(".")
                if len(parts) == 1:
                    schema_name = schema_name
                    table_name = parts[0]
                else:
                    schema_name = parts[0]
                    table_name = parts[1]
                results[table_name] = transaction.table_exists(schema_name, table_name)
            return results
    finally:
        conn.close()


def enriched_errors_reader(iterator, source: Path = None):
    line = 1
    while True:
        try:
            yield next(iterator)
            line = line + 1
        except StopIteration:
            return
        except Exception as e:
            raise Exception(f"Error in line {line} while parsing {source}") from e


def load_data_from_json(path: Path) -> Iterator[str]:
    with open(path) as file:
        data = json.load(file)
    if len(data) > 1:
        raise ValueError("Top-level element has more than one key. JSON data has to be a single-element dict of lists.")
    data = next(iter(data.values()))
    for row in data:
        yield json.dumps(row, ensure_ascii=False)


def remove_bom_from_csv_row(row: List):
    return row[0].replace("\ufeff", "")


def load_data_from_csv(
    path: Path, has_headers: bool, separator: str, encoding: str, ensure_ascii: bool = True
) -> Iterator[str]:
    with path.open("r", newline="", encoding=encoding) as file:
        reader = filter(lambda la: la, _reader(file, path, separator))
        empty_line_skipping_reader = filter(lambda la: la, reader)
        if has_headers:
            fieldnames = next(empty_line_skipping_reader)
            fieldnames[0] = remove_bom_from_csv_row(fieldnames)
        else:
            first_row = next(empty_line_skipping_reader)
            first_row[0] = remove_bom_from_csv_row(first_row)
            fieldnames = [f"col{i+1}" for i in range(0, len(first_row))]
            yield json.dumps(dict(zip(fieldnames, first_row)), ensure_ascii=ensure_ascii)

        for row in empty_line_skipping_reader:
            yield json.dumps(dict(zip(fieldnames, row)), ensure_ascii=ensure_ascii)


def _reader(file: TextIO, path: Path, separator: str) -> Generator[List[str], None, None]:
    return enriched_errors_reader(
        csv.reader(
            file,
            delimiter=separator,
            quotechar='"',
            doublequote=True,
            strict=True,
            skipinitialspace=True,
        ),
        path,
    )


def data_to_postgres(
    transaction: Transaction,
    exported_at: datetime,
    data: Iterable[str],
    table_name: str,
    incremental_extract_date_range: Optional[Tuple[str, str]],
    view_names: Sequence[str] = (),
) -> None:
    transaction.ensure_extract_table_exists(EXTRACT_SCHEMA, table_name)

    for view_name in view_names:
        if transaction.table_exists(EXTRACT_SCHEMA, view_name):
            transaction.drop_table(EXTRACT_SCHEMA, view_name)
        transaction.verify_that_view_maps_to_expected_table_if_it_exists(EXTRACT_SCHEMA, view_name, table_name)
        transaction.create_or_replace_view(EXTRACT_SCHEMA, table_name, view_name)

    transaction.insert_or_replace_extract_records(
        EXTRACT_SCHEMA, table_name, data, exported_at, incremental=incremental_extract_date_range is not None
    )
