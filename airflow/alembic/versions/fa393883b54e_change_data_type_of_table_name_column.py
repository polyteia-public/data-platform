"""Change data type of table_name column

Revision ID: fa393883b54e
Revises: a90c400bf4e6
Create Date: 2023-08-25 14:29:33.411961

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "fa393883b54e"
down_revision = "a90c400bf4e6"
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.alter_column(
        "_table_last_updated",
        "table_name",
        existing_type=sa.VARCHAR(length=64),
        type_=sa.String(),
        existing_nullable=False,
        schema="extract",
    )


def downgrade() -> None:
    pass
