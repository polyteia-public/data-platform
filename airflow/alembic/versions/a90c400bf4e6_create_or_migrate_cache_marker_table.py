"""create or migrate cache marker table

Revision ID: a90c400bf4e6
Revises:
Create Date: 2023-07-10 15:16:21.377535

"""
import sqlalchemy as sa
from alembic import op
from sqlalchemy.dialects import postgresql
from sqlalchemy.engine import Inspector

# revision identifiers, used by Alembic.
revision = "a90c400bf4e6"
down_revision = None
branch_labels = None
depends_on = None


def upgrade() -> None:
    if not schema_exists("extract"):
        op.execute('CREATE SCHEMA IF NOT EXISTS "extract"')
    if table_exists("extract", "_table_last_updated"):
        replace_unique_constraint_with_primary_key_constraint(
            "extract", "_table_last_updated", ["table_name", "exported_at"], "ux_name", "_implicit_generated"
        )
        # https://stackoverflow.com/a/68679549
        convert_accidental_json_string_jsonb_column_to_actual_json("extract", "_table_last_updated", "parser_rule")
    else:
        op.create_table(
            "_table_last_updated",
            sa.Column("table_name", sa.String(length=64), nullable=False),
            sa.Column("exported_at", sa.DateTime(timezone=True), nullable=False),
            sa.Column("parser_rule", postgresql.JSONB(astext_type=sa.Text()), nullable=True),
            sa.Column("file_last_modified", sa.DateTime(timezone=True), nullable=True),
            sa.Column("file_e_tag", sa.String(length=64), nullable=True),
            sa.PrimaryKeyConstraint("table_name", "exported_at"),
            schema="extract",
        )
    if op.get_bind().execute("SELECT 1 FROM pg_catalog.pg_user WHERE usename='extract'").fetchone():
        op.execute('GRANT ALL PRIVILEGES ON TABLE "extract"._table_last_updated TO extract')


def replace_unique_constraint_with_primary_key_constraint(
    schema, table_name, columns, old_constraint_name, new_constraint_name
):
    op.drop_constraint(old_constraint_name, table_name, type_="unique", schema=schema)
    op.create_primary_key(new_constraint_name, table_name, columns=columns, schema=schema)


def convert_accidental_json_string_jsonb_column_to_actual_json(schema, table_name, column_name):
    # https://stackoverflow.com/a/68679549
    op.execute(f"UPDATE {schema}.{table_name} SET {column_name}=({column_name} #>> '{{}}')::jsonb")


def schema_exists(schema_name):
    inspector = Inspector.from_engine(op.get_bind())
    schemas = inspector.get_schema_names()
    return schema_name in schemas


def table_exists(schema, table_name):
    inspector = Inspector.from_engine(op.get_bind())
    tables = inspector.get_table_names(schema=schema)
    table_exists_ = table_name in tables
    return table_exists_


def downgrade() -> None:
    pass
