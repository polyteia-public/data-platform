from logging.config import fileConfig

from alembic import context
from sqlalchemy import engine_from_config, pool

from polyteia import table_cache
from polyteia.config import Config

config = context.config

if config.config_file_name is not None:
    fileConfig(config.config_file_name)

config.set_main_option(
    "sqlalchemy.url",
    (
        f"postgresql+psycopg2://{Config().STORE_PGUSER}"
        f":{Config().STORE_PGPASSWORD}"
        f"@{Config().STORE_PGHOST}"
        f":{Config().STORE_PGPORT}"
        f"/{Config().STORE_PGDATABASE}"
    ),
)

target_metadata = table_cache.Base.metadata

connectable = engine_from_config(
    config.get_section(config.config_ini_section, {}),
    prefix="sqlalchemy.",
    poolclass=pool.NullPool,
)

with connectable.connect() as connection:
    context.configure(
        connection=connection,
        target_metadata=target_metadata,
        include_schemas=True,
    )

    with context.begin_transaction():
        context.run_migrations()
