from polyteia.dag import PolyteiaDAG, extract
from polyteia.data_source import DataSource
from polyteia.parser_rule import DEFAULT_PARSER_RULES, CsvParserRule

dag = PolyteiaDAG("airflow_test", "test", warehouse="trino")

with dag:
    extract_test = extract(
        DataSource(
            id="test",
            s3prefix="airflow_test/test",
            parser_rules=[
                CsvParserRule(pattern="no_headers.csv", has_headers=False),
                CsvParserRule(pattern="comma_separated.csv", separator=","),
                CsvParserRule(pattern="iso8859-1.csv", encoding="iso-8859-1"),
                *DEFAULT_PARSER_RULES,
            ],
        )
    )
