SELECT a as c,
       b as d
FROM {{ ref('airflow_test_base_one')}}
UNION ALL
SELECT a as c,
       b as d
FROM {{ ref('airflow_test_base_two')}}
