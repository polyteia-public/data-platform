from polyteia.dag import PolyteiaDAG, dbt, extract, python
from polyteia.data_source import DataSource
from polyteia.parser_rule import CsvParserRule

dag = PolyteiaDAG("airflow_test", "test")

with dag:
    extract_test = extract(
        DataSource(
            id="test",
            s3prefix="airflow_test/test",
            parser_rules=[
                CsvParserRule(pattern="no_headers.csv", has_headers=False),
                CsvParserRule(pattern="comma_separated.csv", separator=","),
                CsvParserRule(pattern="iso8859-1.csv", encoding="iso-8859-1"),
                CsvParserRule(pattern="dummy.csv"),
                CsvParserRule(pattern="additional_table.csv"),
            ],
        )
    )
    extract_meso = extract(DataSource(id="meso", s3prefix="airflow_test/data_source_one"))
    extract_loga = extract(DataSource(id="loga", s3prefix="airflow_test/data_source_two"))

    dbt_run = dbt("run --select airflow_test")
    dbt_run << [extract_test, extract_meso, extract_loga]

    python_op = python("hello.py", args=["world"])
    python_op << dbt_run

    dbt_run_additional = dbt("run --select base")
    dbt_run_additional << python_op
