from polyteia.data_source import DataSource
from polyteia.dataset import (
    DataSet,
    additional_dbt_run_task,
    additional_docker_task,
    additional_python_task,
)
from polyteia.parser_rule import DEFAULT_PARSER_RULES, CsvParserRule

parser_rules = [
    CsvParserRule(pattern="no_headers.csv", has_headers=False),
    CsvParserRule(pattern="comma_separated.csv", separator=","),
    CsvParserRule(pattern="iso8859-1.csv", encoding="iso-8859-1"),
    *DEFAULT_PARSER_RULES,
]
aset = DataSet(
    "airflow_test",
    "test",
    # specifying the docker image urls is necessary for this example code because of its special location
    # it's not needed for normal data projects, the defaults work out of the box for those
    dbt_runner_image="registry.gitlab.com/polyteia/data-platform/airflow-test-test/dbt",
    python_runner_image="registry.gitlab.com/polyteia/data-platform/airflow-test-test/python",
    data_sources=[
        DataSource(id="test", s3prefix="airflow_test/test", parser_rules=parser_rules, timestamp="2021-06-22"),
        DataSource(id="meso", s3prefix="airflow_test/data_source_one"),
        DataSource(id="loga", s3prefix="airflow_test/data_source_two"),
    ],
).make_dag(
    additional_tasks=[
        additional_python_task("some.additional.python.task", "hello.py"),
        additional_dbt_run_task("something_additional", select="base/aset_base"),
        # security risk: do not run untrusted docker containers here as it may expose secrets or data
        additional_docker_task("some_additional_docker_task", image="alpine", command="echo hello world"),
    ],
)
