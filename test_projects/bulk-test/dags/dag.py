from polyteia.dag import PolyteiaDAG, dbt, extract, generate_dags
from polyteia.data_source import DataSource
from polyteia.parser_rule import DEFAULT_PARSER_RULES, CsvParserRule


def create_dag(customer_name: str, config: dict):
    dag = PolyteiaDAG(customer_name, warehouse="trino")

    with dag:
        if config["extract"]["enabled"]:
            extract_test = extract(
                DataSource(
                    id="test",
                    s3prefix="airflow_test/test",
                    parser_rules=[
                        CsvParserRule(pattern="no_headers.csv", has_headers=False),
                        CsvParserRule(pattern="comma_separated.csv", separator=","),
                        CsvParserRule(pattern="iso8859-1.csv", encoding="iso-8859-1"),
                        *DEFAULT_PARSER_RULES,
                    ],
                )
            )
            extract_meso = extract(DataSource(id="meso", s3prefix="airflow_test/data_source_one"))
            extract_loga = extract(DataSource(id="loga", s3prefix="airflow_test/data_source_two"))

        if config["dbt_task_1"]["enabled"]:
            dbt_task = dbt("run", "task_1", dbt_vars=config["dbt_task_1"]["vars"])

            if config["extract"]["enabled"]:
                dbt_task << [extract_test, extract_meso, extract_loga]

        if config["dbt_test"]["enabled"]:
            dbt_test = dbt("test", "test", dbt_vars=config["dbt_test"]["vars"])

            if config["dbt_task_1"]["enabled"]:
                dbt_test << dbt_task

    return dag


globals().update(generate_dags(create_dag, __file__))
