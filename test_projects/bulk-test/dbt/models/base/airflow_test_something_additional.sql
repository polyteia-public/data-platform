SELECT json_value(data, 'strict $.x') as x,
       json_value(data, 'strict $.y') as y
FROM {{ source('airflow_test_test_something_additional','extract__test_additional_table')}}
