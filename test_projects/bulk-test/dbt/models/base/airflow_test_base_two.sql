SELECT json_value(data, 'strict $.a') as a,
       json_value(data, 'strict $.b') as b
FROM {{ source('airflow_test_loga','extract__loga_dummy')}}
