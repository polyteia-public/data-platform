import argparse


def main():
    parser = argparse.ArgumentParser(description="Process arguments.")
    parser.add_argument("--arg1", required=True, help="Value for arg1")
    parser.add_argument("--arg2", required=True, help="Value for arg2")
    parser.add_argument("--arg3", required=True, help="Value for arg3")

    args = parser.parse_args()

    arg1 = args.arg1
    arg2 = args.arg2
    arg3 = args.arg3

    print("arg1:", arg1)
    print("arg2:", arg2)
    print("arg3:", arg3)


if __name__ == "__main__":
    main()
