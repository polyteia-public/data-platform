#!/usr/bin/env bash
set -euo pipefail; cd "$(dirname "${BASH_SOURCE[0]}")"
print_failure() { echo "${BASH_SOURCE[1]}:$1 command failed with exit status $2: $3" ; }
set -E; trap 'print_failure "$LINENO" "$?" "$BASH_COMMAND"' ERR

message=$1

export DP_STORE_PGHOST="${PGHOST:-localhost}"
export DP_STORE_PGPASSWORD=postgres
export DP_STORE_PGPORT=12345
export DP_STORE_TRINO_HOST=dummy
export DP_DOCKER_NETWORK=dummy

container_name="${PG_CONTAINER_NAME:-alembic-generate-migrations-db}"

trap "{ docker stop \"$container_name\" >/dev/null; }" EXIT

# postgres version should match the one used on production
docker run --rm -d \
    --name "$container_name" \
    -e "POSTGRES_PASSWORD=$DP_STORE_PGPASSWORD" \
    -p "$DP_STORE_PGPORT:5432" \
    postgres:12.6

while ! docker exec "$container_name" pg_isready >/dev/null; do sleep 1; done

function generate_migrations_if_needed {
    alembic upgrade head
    if alembic check; then
        echo "migrations are up to date"
    else
        if [[ ${CHECK_ONLY:-0} == "1" ]]; then
            echo "migrations are not up to date"
            exit 1
        fi
        echo "generating revision '$message'"
        alembic revision --autogenerate -m "$message"
    fi
}

(cd airflow && generate_migrations_if_needed)
