import json
import os
import sys
import time
from datetime import datetime
from json import JSONDecodeError
from typing import Sequence

import requests
import sqlalchemy
from requests import Response
from requests.auth import HTTPBasicAuth
from sqlalchemy.sql.expression import text
from tenacity import retry, stop_after_delay, wait_fixed

API_BASE_URL = "http://air-srvr:8080/api/v1"
DAG_ID = "airflow_test_test"
DAG_URL = f"{API_BASE_URL}/dags/{DAG_ID}"

TRINO_START_MAX_WAIT = os.getenv("TRINO_START_MAX_WAIT", 120)


def airflow_request(verb: str, url: str, params: dict = None, json: dict = None) -> Response:
    params = params or {}
    response = requests.request(
        verb,
        url,
        auth=HTTPBasicAuth("admin", "admin"),
        headers={"Content-type": "application/json"},
        params=params,
        json=json,
    )
    response.raise_for_status()
    return response


def airflow_get(url: str, params: dict = None) -> dict:
    try:
        response = airflow_request(verb="GET", url=url, params=params)
        return response.json()
    except JSONDecodeError:
        raise ValueError(f"airflow returned unexpected non-json value: {response.text}")


def airflow_get_plain(url: str, params: dict = None) -> str:
    return airflow_request(verb="GET", url=url, params=params).text


def airflow_post(url: str, json: dict = None) -> dict:
    try:
        response = airflow_request(verb="POST", url=url, json=json)
        return response.json()
    except JSONDecodeError:
        raise ValueError(f"airflow returned unexpected non-json value: {response.text}")


def get_dag_runs() -> dict:
    return airflow_get(f"{DAG_URL}/dagRuns")


def wait_for_airflow() -> None:
    print("waiting for airflow to be ready")
    for i in range(1, 10 + 1):
        print(f"attempting to trigger dag run for {DAG_ID} {i}/10")
        try:
            trigger_dag_run()
            return
        except Exception as e:
            print(f'failed with "{e}"')
            last_exception = e
        time.sleep(5)
    raise last_exception from None


@retry(wait=wait_fixed(1), stop=stop_after_delay(TRINO_START_MAX_WAIT))
def wait_for_trino() -> None:
    engine = sqlalchemy.create_engine(
        f"trino://"
        f'{os.environ["DP_STORE_TRINO_USER"]}'
        f'@{os.environ["DP_STORE_TRINO_HOST"]}'
        f':{os.environ["DP_STORE_TRINO_PORT"]}'
        f'/{os.environ["DP_STORE_TRINO_HIVE_CATALOG"]}',
    )

    with engine.connect() as con:
        con.execute(text(f"CREATE schema IF NOT EXISTS hive.{DAG_ID} with (location = 's3://datalake/{DAG_ID}')"))


def trigger_dag_run() -> None:
    trigger_result = airflow_post(f"{DAG_URL}/dagRuns", json={"conf": {}})
    if trigger_result["state"] not in ("running", "queued", "success", "failed"):
        raise ValueError(f"unexpected JSON response: {trigger_result}")


class NoDagRunsFound(Exception):
    pass


def get_latest_test_dag_run() -> dict:
    dag_runs = get_dag_runs()
    if not dag_runs or not dag_runs.get("dag_runs", None):
        raise NoDagRunsFound()
    return dag_runs["dag_runs"][-1]


def wait_for_test_dag() -> None:
    print("waiting for test dag to complete")
    for i in range(1, 50 + 1):
        print(f"checking if dag is still queued or running {i}/50")
        try:
            if get_latest_test_dag_run()["state"] not in ("running", "queued"):
                return
        except NoDagRunsFound:
            pass
        time.sleep(5)


def get_task_instances(dag_run_id: str) -> Sequence[dict]:
    return airflow_get(f"{DAG_URL}/dagRuns/{dag_run_id}/taskInstances")["task_instances"]


def get_task_logs(dag_run_id: str, task_instance: dict) -> str:
    task_id = task_instance["task_id"]
    try_number = task_instance["try_number"]
    url = f"{DAG_URL}/dagRuns/{dag_run_id}/taskInstances/{task_id}/logs/{try_number}"
    return airflow_get_plain(url, params={"full_content": True})


def print_logs_of_latest_run() -> None:
    dag_run_id = get_latest_test_dag_run()["dag_run_id"]
    task_instances = get_task_instances(dag_run_id)
    print("logs of all tasks:")
    for task_instance in task_instances:
        print(f"logs of task {task_instance['task_id']}:")
        print(get_task_logs(dag_run_id, task_instance))
        print()


if __name__ == "__main__":
    try:
        start = datetime.now()
        wait_for_trino()
        print(f"trino available after {datetime.now() - start}")
        wait_for_airflow()
        wait_for_test_dag()
        if get_latest_test_dag_run()["state"] != "success":
            print("DAG run failed")
            print_logs_of_latest_run()
            sys.exit(1)
    finally:
        print("dags before exit:")
        dags = airflow_get(f"{API_BASE_URL}/dags")["dags"]
        dag_ids = [dag["dag_id"] for dag in dags]
        print(json.dumps(dag_ids, indent=4, sort_keys=True))
        print()

        print("import errors before exit")
        print(json.dumps(airflow_get(f"{API_BASE_URL}/importErrors"), indent=4, sort_keys=True))
        print()

        print("dag runs before exit:")
        print(json.dumps(airflow_get(f"{DAG_URL}/dagRuns"), indent=4, sort_keys=True))
        print()
