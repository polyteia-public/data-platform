#!/usr/bin/env bash
set -euo pipefail; cd "$(dirname "${BASH_SOURCE[0]}")"; cd ../..
set -E
print_failure() {
  line_number=$1
  exit_status=$2
  command=$3
  echo "${BASH_SOURCE[1]}:${line_number} command failed with exit status ${exit_status}: $command"
}
trap 'print_failure "$LINENO" "$?" "$BASH_COMMAND"' ERR


SSH_KEY_LOCATION=ops/ansible/.ssh/data-platform-test
exit_trap() {
    if [[ "${DESTROY_ON_EXIT:-1}" == "1" ]]; then
        TERRAFORM_DESTROY_AUTO_APPROVE=1 ./ops/ansible/ecs_test destroy
    fi
}
trap exit_trap EXIT

./ops/ansible/ecs_test up
ecs_host=$(cat ops/ansible/terraform/terraform_output_ECS_IP.txt |tr -d '"')
rsync -e "ssh -i $SSH_KEY_LOCATION -o UserKnownHostsFile=$(dirname $SSH_KEY_LOCATION)/known_hosts" \
    "ops/cicd/run_test_dag.py" \
    "ops/cicd/run-test-dag-py-in-docker" \
    "ubuntu@$ecs_host:/home/ubuntu/"

if ! ssh \
    -i $SSH_KEY_LOCATION \
    -o "UserKnownHostsFile=$(dirname $SSH_KEY_LOCATION)/known_hosts" \
    "ubuntu@$ecs_host" \
    "\
        DP_STORE_TRINO_HOST=trino \
        DP_STORE_TRINO_PORT=8080 \
        DP_STORE_TRINO_ICEBERG_CATALOG=iceberg \
        DP_STORE_TRINO_HIVE_CATALOG=hive \
        DP_STORE_TRINO_USER=admin \
        /home/ubuntu/run-test-dag-py-in-docker \
        dp_test_default \
    "
then
    echo "DAG run failed"
    if [[ "${PAUSE_ON_FAILURE:-0}" == "1" ]]; then
        read
    fi
    exit 1
fi
