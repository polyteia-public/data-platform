#!/usr/bin/env bash
set -euo pipefail; cd "$(dirname "${BASH_SOURCE[0]}")" && cd ..;

IMAGE_TAG=${IMAGE_TAG:-latest}

echo "$DP_REGISTRY_PASSWORD" | docker login -u "$DP_REGISTRY_USER" --password-stdin  registry.gitlab.com

function docker_build_cached(){
    IMAGE_NAME=$1
    DOCKERFILE=$2
    INTERMEDIATE_STAGES=("${@:3}")

    additional_build_args=()
    if [[ -n "$CACHE_REGISTRY_URL" ]]; then
        for stage in ${INTERMEDIATE_STAGES:-}; do
            STAGE_CACHE_IMAGE_NAME="$CACHE_REGISTRY_URL/$IMAGE_NAME-$stage:latest"
            docker pull "$STAGE_CACHE_IMAGE_NAME" || true
            additional_build_args=("${additional_build_args[@]}" "--cache-from" "$STAGE_CACHE_IMAGE_NAME")
        done

        CACHE_IMAGE_NAME="$CACHE_REGISTRY_URL/$IMAGE_NAME:latest"
        docker pull "$CACHE_IMAGE_NAME" || true
    fi
    docker build --pull \
        -t "$IMAGE_NAME:$IMAGE_TAG" \
        --cache-from "$CACHE_IMAGE_NAME" \
        "${additional_build_args[@]}" \
        -f "$DOCKERFILE" \
        .
}

case $DP_STAGE in
    lcl)
        docker build --pull -t "dp-airflow:${IMAGE_TAG}" -f ./airflow/docker/Dockerfile .
        docker build --pull -t "dp-dbt:${IMAGE_TAG}" -f ./dbt/Dockerfile .
        docker build --pull -t "dp-python:${IMAGE_TAG}" -f ./python/Dockerfile .
        ;;
    cicd)
        docker build --pull -t "dp-airflow:${IMAGE_TAG}" -f ./airflow/docker/Dockerfile .
        docker build --pull -t "dp-dbt:${IMAGE_TAG}" -f ./dbt/Dockerfile .
        docker build --pull -t "dp-python:${IMAGE_TAG}" -f ./python/Dockerfile .
        docker build --pull -t "dp-store:${IMAGE_TAG}" -f ./store/Dockerfile .
        ;;
    prd | stg)
        docker build --pull -t "dp-airflow:${IMAGE_TAG}" -f ./airflow/docker/Dockerfile .
        docker build --pull -t "dp-ingress:${IMAGE_TAG}" -f ./ingress/Dockerfile .
        docker build --pull -t "dp-ingress-no-jwt:${IMAGE_TAG}" -f ./ingress/Dockerfile-no-jwt .
        ;;
    test)
        docker_build_cached "dp-airflow" ./airflow/docker/Dockerfile
        docker_build_cached "dp-ingress" ./ingress/Dockerfile build
        docker_build_cached "dp-ingress-no-jwt" ./ingress/Dockerfile-no-jwt
        docker_build_cached "dp-store" ./store/Dockerfile
        ;;
esac
