#!/usr/bin/env bash
set -euo pipefail

SSH_DESTINATION=$1
SSH_KEY_LOCATION=$2
KNOWN_HOSTS_FILE_LOCATION=$3

echo "Starting SSH tunnel"
ssh -N \
    -o ExitOnForwardFailure=yes \
    -L 0.0.0.0:19010:127.0.0.1:19010 \
    -L 0.0.0.0:19011:127.0.0.1:19011 \
    -L 0.0.0.0:19012:127.0.0.1:19012 \
    -L 0.0.0.0:19015:127.0.0.1:19015 \
    -L 0.0.0.0:19024:127.0.0.1:8081 \
    -L 0.0.0.0:8080:127.0.0.1:80 \
    -L 0.0.0.0:8443:127.0.0.1:443 \
    -i "$SSH_KEY_LOCATION" \
    -o "UserKnownHostsFile=$KNOWN_HOSTS_FILE_LOCATION" \
    "$SSH_DESTINATION"