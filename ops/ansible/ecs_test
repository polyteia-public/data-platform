#!/usr/bin/env bash
set -euo pipefail; cd "$(dirname "${BASH_SOURCE[0]}")"; cd ../..
set -E
print_failure() {
  line_number=$1
  exit_status=$2
  command=$3
  echo "${BASH_SOURCE[1]}:${line_number} command failed with exit status ${exit_status}: $command"
}
trap 'print_failure "$LINENO" "$?" "$BASH_COMMAND"' ERR

main(){
    ensure_env_var_exists TF_VAR_access_key
    ensure_env_var_exists TF_VAR_secret_key
    ensure_env_var_exists TF_VAR_user_id

    SSH_DIR="ops/ansible/.ssh"
    KNOWN_HOSTS_FILE=$SSH_DIR/known_hosts
    SSH_KEY_LOCATION=$SSH_DIR/data-platform-test
    ensure_ssh_key_exists "$SSH_KEY_LOCATION"
    export TF_VAR_ssh_public_key=$(cat "${SSH_KEY_LOCATION}.pub")

    SSH_HOST_PRIVATE_KEY_LOCATION=$SSH_KEY_LOCATION
    SSH_HOST_PUBLIC_KEY_LOCATION="${SSH_HOST_PRIVATE_KEY_LOCATION}.pub"
    ensure_ssh_key_exists "$SSH_HOST_PRIVATE_KEY_LOCATION"
    export TF_VAR_ssh_host_private_key=$(cat "$SSH_HOST_PRIVATE_KEY_LOCATION")
    export TF_VAR_ssh_host_public_key=$(cat "$SSH_HOST_PUBLIC_KEY_LOCATION")

    case $1 in
        up)
            terraform_plan_and_apply
            ECS_IP=$(cat ops/ansible/terraform/terraform_output_ECS_IP.txt |tr -d '"')
            echo "$ECS_IP $TF_VAR_ssh_host_public_key" >$KNOWN_HOSTS_FILE
            export SSH_DESTINATION="ubuntu@$ECS_IP"
            export ADDITIONAL_SSH_ARGS="-o UserKnownHostsFile=$KNOWN_HOSTS_FILE -i $SSH_KEY_LOCATION"
            wait_for_ssh_connection
            echo "[test]" > ops/ansible/inventory.ini
            echo "$ECS_IP ansible_user=ubuntu" >> ops/ansible/inventory.ini

            export ANSIBLE_SSH_ARGS="-o UserKnownHostsFile=$KNOWN_HOSTS_FILE"
            export ANSIBLE_STDOUT_CALLBACK=yaml
            ansible-playbook \
                -i ops/ansible/inventory.ini \
                --private-key $SSH_KEY_LOCATION \
                ops/ansible/data_platform.yml \
                --extra-vars "additional_ssh_args=\"$ADDITIONAL_SSH_ARGS\" ssh_destination=$SSH_DESTINATION"
            ;;
        tunnel)
            ECS_IP=$(cat ops/ansible/terraform/terraform_output_ECS_IP.txt |tr -d '"')
            ./ops/ansible/forward_ecs_ports.sh "ubuntu@$ECS_IP" "$SSH_KEY_LOCATION" "$KNOWN_HOSTS_FILE"
            ;;
        destroy)
            (
                cd ops/ansible/terraform
                if [[ "${TERRAFORM_DESTROY_AUTO_APPROVE:-0}" == "1" ]]; then
                    terraform destroy -auto-approve
                else
                    terraform destroy
                fi
            )
            ;;
        ssh)
            ECS_IP=$(cat ops/ansible/terraform/terraform_output_ECS_IP.txt |tr -d '"')
            ssh -o UserKnownHostsFile=$KNOWN_HOSTS_FILE -i $SSH_KEY_LOCATION ubuntu@$ECS_IP
            ;;
        *)
            echo "unknown subcommand \"$1\""
            exit 1
            ;;
    esac
}

wait_for_ssh_connection() {
  MAX_RETRIES=10
  RETRY_DELAY=5
  for ((i=1; i<=$((MAX_RETRIES)); i++)); do
    if ssh $SSH_DESTINATION $ADDITIONAL_SSH_ARGS exit; then
      echo "SSH Connection via $SSH_DESTINATION successful!"
      break
    else
      echo "SSH Connection via $SSH_DESTINATION failed. Retrying in $RETRY_DELAY seconds..."
      sleep $RETRY_DELAY
    fi
  done
}

ensure_env_var_exists() {
    if [[ -z ${!1+set} ]]; then
       echo "Error: Define $1 in .envrc"
       exit 1
    fi
}

ensure_ssh_key_exists() {
    SSH_KEY_LOCATION=$1
    if ! [ -f "$SSH_KEY_LOCATION" ]; then
        echo "$SSH_KEY_LOCATION does not exist. Generating key..."
        mkdir -p "$(dirname "$SSH_KEY_LOCATION")"
        ssh-keygen -t ecdsa -N "" -b 521 -f "$SSH_KEY_LOCATION"
    fi
}

terraform_plan_and_apply(){
    (
        cd ops/ansible/terraform

        (
            export PROJECT_ID=$PROJECT_ID_TEST_INFRA_STATE
            export STATE_ID=$TF_VAR_user_id
            export GITLAB_USERNAME=$TF_TEST_STATE_GITLAB_USERNAME
            export GITLAB_TOKEN=$TF_TEST_STATE_GITLAB_TOKEN
            ./terraform-init.sh
        )
        terraform plan -out=update-plan

        if terraform show update-plan | grep -E "[0-9]+ to add, [0-9]+ to change, [1-9][0-9]* to destroy" >/dev/null; then
            echo "Terraform is trying to destroy existing resources. ABORTING!"
            echo "To see the unexpected changes, run: terraform show update-plan"
            exit 1;
        fi

        if terraform show update-plan | grep -E "[0-9]+ to add, [1-9][0-9]* to change, [0-9]+ to destroy" >/dev/null; then
            echo "Terraform is trying to change existing resources. ABORTING!"
            echo "To see the unexpected changes, run: terraform show update-plan"
            exit 1;
        fi

        terraform apply update-plan
        terraform output server_external_ip_address > terraform_output_ECS_IP.txt
    )
}

main "$@"
