resource "opentelekomcloud_vpc_v1" "data-platform" {
  cidr = "192.168.0.0/16"
  name = local.instance_id
}

resource "opentelekomcloud_vpc_subnet_v1" "data-platform" {
  cidr = "192.168.10.0/24"
  gateway_ip = "192.168.10.1"
  name = local.instance_id
  # Use OTC secondary DNS instead of default 1.1.1.1 to avoid issues with internal DNS entries (see https://polyteia.atlassian.net/browse/SN-2087)
  dns_list = ["100.125.4.25","100.125.129.199"]
  vpc_id = opentelekomcloud_vpc_v1.data-platform.id
}
