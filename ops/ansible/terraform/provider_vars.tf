variable "domain_name" {
  default = "OTC-EU-DE-00000000001000031681"
}

variable "tenant_name" {
  default = "eu-de_platform_tests"
}

variable "access_key" {
  type = string
}

variable "secret_key" {
  type = string
}
