variable "user_id" {
  type= string
  description = "user id (e.g. 'max_mustermann')"
}

variable "ssh_public_key" {
  type = string
  description = "VM public SSH system key"
}

variable "vm_ecs_flavor" {
  type = string
  description = "OTC ECS instance flavor"
  default = "s3.large.4"
}

variable "vm_image_id" {
  type = string
  description = "VM image ID (default: image_Standard_Ubuntu_20_04_latest)"
  default = "a635553a-46ca-435b-b256-ea79fba5534f"
}

variable "ssh_host_private_key" {
  type = string
  description = "SSH private key to use as the servers SSHD host private key"
}

variable "ssh_host_public_key" {
  type = string
  description = "SSH public key to use as the servers SSHD host public key"
}

locals {
  instance_id = "data-platform_test_${var.user_id}"
}
