resource "opentelekomcloud_ecs_instance_v1" "vm_instance" {
  availability_zone = "eu-de-01"
  flavor = var.vm_ecs_flavor
  image_id = var.vm_image_id
  name = local.instance_id
  vpc_id = opentelekomcloud_vpc_v1.data-platform.id
  system_disk_size = 500
  system_disk_type = "SSD"
  security_groups = [opentelekomcloud_networking_secgroup_v2.data-platform.id]
  key_name = local.instance_id
  user_data = templatefile("cloud-config.yml", {
    ssh_host_private_key = var.ssh_host_private_key
    ssh_host_public_key = var.ssh_host_public_key
  })
  nics {
    network_id = opentelekomcloud_vpc_subnet_v1.data-platform.id
  }
}

# Quote from OTC support: In Openstack the KeyPair belongs to a single user.
# If multiple user want to use the same keypair the have to import the same public
# key with the exact same name for all users.
resource "opentelekomcloud_compute_keypair_v2" "keypair" {
  name       = local.instance_id
  public_key = var.ssh_public_key
}

resource "opentelekomcloud_vpc_eip_v1" "external_ip" {
  publicip {
    type = "5_bgp"
    port_id = opentelekomcloud_ecs_instance_v1.vm_instance.nics.0.port_id
  }
  bandwidth {
    name = local.instance_id
    share_type = "PER"
    size = 500
  }
}

output "server_external_ip_address" {
  value = opentelekomcloud_vpc_eip_v1.external_ip.publicip[0].ip_address
}
