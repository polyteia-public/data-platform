#!/usr/bin/env bash
set -euo pipefail

terraform init \
        -backend-config="address=https://gitlab.com/api/v4/projects/${PROJECT_ID}/terraform/state/${STATE_ID}" \
        -backend-config="lock_address=https://gitlab.com/api/v4/projects/${PROJECT_ID}/terraform/state/${STATE_ID}/lock" \
        -backend-config="unlock_address=https://gitlab.com/api/v4/projects/${PROJECT_ID}/terraform/state/${STATE_ID}/lock" \
        -backend-config="username=${GITLAB_USERNAME}" \
        -backend-config="password=${GITLAB_TOKEN}" \
        -backend-config="lock_method=POST" \
        -backend-config="unlock_method=DELETE"
