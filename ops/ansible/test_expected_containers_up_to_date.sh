#!/usr/bin/env bash

# Naive production smoke test from the aftermath of https://polyteia.atlassian.net/browse/SN-2202
# Replace at will if it's not helpful anymore!
set -euo pipefail

source "$(dirname "${BASH_SOURCE[0]}")"/run_on_remote_docker.sh

ENV=$1
EXPECTED_GIT_HASH=$2

EXPECTED_UPDATED_CONTAINERS="
  dp_${ENV}-air-schd-1
  dp_${ENV}-air-srvr-1
  dp_${ENV}-ingress-1
  dp_${ENV}-ingress-no-jwt-1"

temp_file=$(mktemp)
# shellcheck disable=SC2064
trap "rm -f ${temp_file}" 0 INT HUP QUIT TERM # trap regular and trappable error exits to always clean up
docker ps --format "{{.Names}} {{.Image}}" > "${temp_file}"

containers_missing=0
for container in ${EXPECTED_UPDATED_CONTAINERS}; do
  if ! grep -e "${container}" "${temp_file}" >/dev/null; then
    echo "Expected container ${container} not running."
    containers_missing=1
  fi
  if ! grep -e "${container}.*${EXPECTED_GIT_HASH}$" "${temp_file}" >/dev/null; then
    echo "Expected container ${container} not up to date."
    containers_missing=1
  fi
done
if [ $containers_missing -eq 1 ]; then
  echo
  echo "Found containers not running up-to-date versions."
  echo "Expected '${EXPECTED_GIT_HASH}' in container version tag."
  echo
  echo "Actually running containers with images:"
  docker ps --format "{{.Names}} {{.Image}}"
  exit 1
fi
