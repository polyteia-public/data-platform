# source this for commands that should run against remote docker
#
# Env:
# SSH_DESTINATION: SSH connection string like "user@host" (required)
# ADDITIONAL_SSH_ARGS (additional args to pass to SSH like "-i some_key_file" (optional)

set -euo pipefail

PID=$$
DOCKER_SSH_TUNNEL_DIR="/tmp/docker-ssh-tunnel-${PID}"
DOCKER_SSH_TUNNEL_SOCK="${DOCKER_SSH_TUNNEL_DIR}/docker.sock"
DOCKER_SSH_TUNNEL_CONTROL="/tmp/docker-ssh-tunnel-control-${PID}"

function finish {
  echo "Finished. Cleaning up"
  ssh -O exit -S "$DOCKER_SSH_TUNNEL_CONTROL" "$SSH_DESTINATION"
  rm -rf "$DOCKER_SSH_TUNNEL_SOCK"
  rm -rf "$DOCKER_SSH_TUNNEL_DIR"
}
trap finish EXIT
mkdir -p "$DOCKER_SSH_TUNNEL_DIR"
ssh -o ExitOnForwardFailure=yes -f -L "${DOCKER_SSH_TUNNEL_SOCK}:/var/run/docker.sock" -N -M -S "$DOCKER_SSH_TUNNEL_CONTROL" $ADDITIONAL_SSH_ARGS "$SSH_DESTINATION"

export DOCKER_HOST="unix://${DOCKER_SSH_TUNNEL_SOCK}"
