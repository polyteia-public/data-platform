.PHONY=precommit-hook-install
precommit-hook-install:
	pre-commit install

.PHONY=precommit-check
precommit-check:
	pre-commit run --all-files

.PHONY=test
test:
	pytest airflow/src/test

.PHONY=postgres-test
postgres-test:
	pytest airflow/src/db_test/postgres

.PHONY=trino-test
trino-test:
	pytest airflow/src/db_test/trino

.PHONY=pip-compile
pip-compile:
	cd airflow && pip-compile requirements.in --resolver=backtracking

.PHONY=pip-compile-upgrade
pip-compile-upgrade:
	cd airflow && pip-compile requirements.in --resolver=backtracking --upgrade

.PHONY=check-migrations
check-migrations:
	CHECK_ONLY=1 ./generate_alembic_migrations.sh dummy

