# Data Platform

## Setup
### Requirements
* python 3.8+ (for Macs, use the version you get from [homebrew](https://brew.sh/) as
  the Apple version has issues with
  [Airflow](https://airflow.apache.org/docs/apache-airflow/stable/installation.html#symbol-not-found-py-getargcargv)).
* docker https://docs.docker.com/get-docker/
  * additionally on linux: [docker compose plugin](https://docs.docker.com/compose/install/linux/)
* bash 5+
  * default on recent Ubuntu 
  * install on Mac using `brew install bash`
  * check version with `/usr/bin/env bash --version`
  * if you get the error `additional_compose_args[@]: unbound variable`, you need to upgrade bash to version 5 or higher

### Python version management
We use [pyenv](https://github.com/pyenv/pyenv) to manage python version specific to the project in the `.python-version` file, it helps to keep the same python version in the Dockerfile as the local dev environment.

Install pyenv in Macos : `brew install pyenv`

Following script to activate pyenv only need to be done once:
```sh
pyenv install #This will install version in .python-version, do it again when version updates
# add following line, the pyenv activation to your shell configuration  ~/.zshrc or  ~/.bashrc, next time it will set python version to .python-version when cd to project dir.
eval "$(pyenv init -)"
python --version # output should be the same as from .python-version
```
Next, create a python virtualenv and activate it:
```sh
python3 -m venv .venv
source ".venv/bin/activate"
```
Last, install required dependencies: `pip install -U pip && pip install -r requirements_dev.txt && pip install -r airflow/requirements.txt`

### Code style sanity and Pre-commit hook
We use [pre-commit-hook](https://github.com/pre-commit/pre-commit-hooks) as pre-commit hooks manager, once the pre-commit hook is installed, it will run everytime you make a commit, it keeps our code clean. To install pre-commit hook, do: `make precommit-hook-install`

You can skip pre-commit check by preceed commit with : `git commit -m "..." --no-verify`

### direnv
`direnv` automatically sets up the secrets and other configuration required by the Data Platform.
* install direnv https://direnv.net/docs/installation.html
* create a file called `.envrc` in the repository root with the following content, replacing the placeholders
```
# cicd, lcl, test, unit test
export AWS_ACCESS_KEY_ID=<your S3 access key id>
export AWS_SECRET_ACCESS_KEY=<your S3 secret access key>
export DP_S3_BUCKET=<your S3 bucket name>
export DP_S3_ENDPOINT_URL=<S3 endpoint of the bucket configured in DP_S3_BUCKET>
export DP_EXTRA_INDEX_URL_GITLAB_PACKAGE="https://__token__:<access_token>@gitlab.com/api/v4/projects/<project_id>/packages/pypi/simple"

# cicd, lcl, test
export DP_REGISTRY_PASSWORD=<gitlab token for access to internal docker images>
export DP_REGISTRY_USER=<gitlab token name for access to internal docker images>

# lcl
export DEV_DB_DATABASE=<name of your dev db>
export DEV_DB_USER=$DEV_DB_DATABASE
export DEV_DB_PASSWORD=<password for your dev db>
export AIRFLOW_TRIGGER_DAG_RUNS_USER_PASSWORD=<production airflow user password to use for remote extract>
export DP_PRODUCTION_API_BASE=<api base url to use for remote extract>
export DP_PRODUCTION_URL=<base url to use for remote extract>
export DP_REDASH_URL=<url of a redash instance to show in portal page>
export DP_SSH_TUNNEL_HOST=<db ssh tunnel host>
export DP_LOAD_BALANCER_TRINO_LETO=<edit load balancer host here>
export DP_DEV_STORE_TRINO_HOST=<name of host>
export DP_DEV_STORE_TRINO_PORT=<port of host>
export DP_DEV_STORE_TRINO_USER=<user name for trino>
export DP_DEV_STORE_TRINO_S3_BUCKET=<bucket name used as object storage>
export DP_DEV_STORE_TRINO_S3_ENDPOINT_URL=<object storage endpoint url>
export DP_DEV_STORE_TRINO_S3_ACCESS_KEY=<object storage access key>
export DP_DEV_STORE_TRINO_S3_SECRET_ACCESS_KEY=<object storage secret access key>

# test
export DATADOG_API_KEY=<your datadog api key>

# test if using cloud VM
export TF_VAR_access_key=<otc test infra user access key>
export TF_VAR_secret_key=<otc test infra user secret key>
export TF_VAR_user_id=<firstname_lastname>
export PROJECT_ID_TEST_INFRA_STATE=<project id of the gitlab project for infra state>
export TF_TEST_STATE_GITLAB_USERNAME=<gitlab username or name of token with api access>
export TF_TEST_STATE_GITLAB_TOKEN=<gitlab password or token with api access>
export CACHE_REGISTRY_URL=<url of registry with images to use as docker build cache>
```
* run `direnv allow` in the project root once to activate the `.envrc` you just created

### Database SSH key
* place the ssh private key for the SSH tunnel host in a file called `db-ssh-key`.

---

## Running the platform locally for the included test project
```
EXTERNAL_PROJECT_DIR="$(pwd)/test_projects/airflow-test-test" ./ops/lcl/up
```

## Running unit tests
```
make test
```

## Running db tests
```
pytest airflow/src/db_test
```
This needs postgres in the PATH, see https://pypi.org/project/testing.postgresql/.

## Running the ci dataset test locally
```
IMAGE_TAG=latest TEST_PROJECT_DIR=test_projects/airflow-test-test ./ops/cicd/run-test-dag
```

## Running the ci flexible dag test locally

### Postgres based Project
```
IMAGE_TAG=latest TEST_PROJECT_DIR=test_projects/airflow-test-test-flexible ./ops/cicd/run-test-dag
```

### Trino based Project
```
IMAGE_TAG=latest TEST_PROJECT_DIR=test_projects/trino ./ops/cicd/run-test-dag
```

---

## Testing the Ansible Deploymeny
### 1. Using vagrant
⚠️ Not supported on Mac M1/M2. Please see the section below about testing with ad-hoc cloud VM.

#### Prerequisites
* vagrant
* ansible
* docker and compose V2
* timeout (part of coreutils, `brew install coreutils` on Mac)

#### Running
`./ops/ansible/vagrant_provision` (sets up the complete datastack inside a local Vagrant VM)

### 2. Using an ad-hoc cloud VM
#### Prerequisites
* terraform
* ansible
* docker and compose V2
* timeout (part of coreutils, `brew install coreutils` on Mac)

#### Create VM if not exists and run ansible playbook
```
./ops/ansible/ecs_test up
```

#### Create SSH tunnel
```
./ops/ansible/ecs_test tunnel
```
Makes deployed airflow available at <http://localhost:19010>

#### Open SSH session
```
./ops/ansible/ecs_test ssh
```

#### Destroy VM and associated resources
```
./ops/ansible/ecs_test destroy
```

