import re
import sys
from typing import Dict, List, Union

import yaml


def main() -> None:
    enforce_rules()


def contains_forbidden_chars(data: Union[str, Dict, List]) -> bool:
    if isinstance(data, dict):
        for key, value in data.items():
            if isinstance(key, str) and re.search(r"[‘`\\]", key):
                return True
            if isinstance(value, str) and re.search(r"[‘`\\]", value):
                return True
            if contains_forbidden_chars(key) or contains_forbidden_chars(value):
                return True
    elif isinstance(data, list):
        for item in data:
            if contains_forbidden_chars(item):
                return True
    return False


def enforce_rules() -> None:
    with open("bulk_configs/customers.yml", "r") as file:
        yaml_data = yaml.safe_load(file)

    # Rule 1: 'default' must be defined.
    # if "default" not in yaml_data:
    #     print("YAML validation failed: 'default' is not defined.")
    #     exit(1)

    # Schema Rule 2: Ensure only one or less customer named 'default' exists.
    default_count = sum(1 for customer_name in yaml_data if customer_name == "default")
    if default_count > 1:
        print("YAML validation failed: There should be exactly one customer named 'default'.")
        sys.exit(1)

    # Schema Rule 3: Customer names must be unique and different from 'default'.
    customer_names = set(yaml_data.keys())
    customer_names.discard("default")  # Remove 'default' from the set
    if len(customer_names) != len(yaml_data) - 1:
        print("YAML validation failed: Customer names must be unique and different from 'default'.")
        sys.exit(1)

    # Schema Rule 4: Customer names cannot contain dashes '-'
    for customer_name in customer_names:
        if "-" in customer_name:
            print(f"YAML validation failed: Customer name '{customer_name}' contains a dash.")
            sys.exit(1)

    # Schema Rule 5: If a customer does not have specific variables, it should be defined as an empty dictionary.
    for customer_name, customer_data in yaml_data.items():
        if not isinstance(customer_data, dict):
            print(f"YAML validation failed: Customer '{customer_name}' should be defined as an empty dictionary.")
            sys.exit(1)

    # Schema Rule 6: Check for forbidden characters (‘ ` \)
    if contains_forbidden_chars(yaml_data):
        print("YAML validation failed: Forbidden character found in the YAML data.")
        sys.exit(1)


if __name__ == "__main__":
    main()
