import os
import re
import sys
import textwrap
from glob import glob

import yaml


def main(check_type: str, dag_id: str) -> None:
    if check_type == "seeds":
        check_seeds(dag_id)
    elif check_type == "models":
        check_models(dag_id)
    elif check_type == "schemas":
        check_schemas()
    elif check_type == "packages":
        check_packages()
    else:
        raise ValueError


def check_seeds(dag_id: str) -> None:
    if not seed_table_uniqueness_is_guaranteed(dag_id):
        print(
            textwrap.dedent(
                f"""\
                Seed table uniqueness is not guaranteed with the current dbt setup.
                You need to either:
                    a) prefix all seed file names with {dag_id}_

                        Example:
                            seeds/{dag_id}_dummy_seed.csv

                    b) set the dbt project/package name to {dag_id} and enable automatic prefixing for seeds.

                        Example:
                            dbt_project.yml:
                                name: '{dag_id}'
                                version: '1.0.0'
                                config-version: 2
                                ...
                                seed-paths: ["seeds"]
                                ...
                                macro-paths: ["dbt_packages/polyteia_dbt_utils/macros", "macros"]
                                ...
                                seeds:
                                  +schema: seeds
                                  +tags: prefix_table_names
                                ...

                            packages.yml:
                                packages:
                                  - git: "https://{{env_var('DBT_ENV_SECRET_GITLAB_REPO_CREDENTIALS')}}@gitlab.com/polyteia/data-pipelines/polyteia-dbt-utils.git"
                                    revision: main
                                ...
                """
            )
        )
        sys.exit(1)
    print("Seed table uniqueness is guaranteed.")


def check_models(dag_id: str) -> None:
    if not model_table_uniqueness_is_guaranteed(dag_id):
        print(
            textwrap.dedent(
                f"""\
                Model table uniqueness is not guaranteed with the current dbt setup.
                You need to either:
                    a) prefix all model file names with {dag_id}_

                        Example:
                            models/base/{dag_id}_example.sql

                    b) set the dbt project/package name to {dag_id} and enable automatic prefixing for models.
                        Example:
                            dbt_project.yml:
                                name: '{dag_id}'
                                ...
                                macro-paths: ["dbt_packages/polyteia_dbt_utils/macros", "macros"]
                                ...
                                models:
                                  {dag_id}:
                                  +tags: prefix_table_names
                                ...

                            packages.yml:
                                packages:
                                  - git: "https://{{env_var('DBT_ENV_SECRET_GITLAB_REPO_CREDENTIALS')}}@gitlab.com/polyteia/data-pipelines/polyteia-dbt-utils.git"
                                    revision: main
                                ...
                """
            )
        )
        sys.exit(1)
    print("Model table uniqueness is guaranteed.")


def check_schemas() -> None:
    if not custom_schemas_are_guaranteed():
        print(
            textwrap.dedent(
                """\
                Standard schema use is not guaranteed with the current dbt setup.
                You need to enable the polyteia get_custom_schema macro by importing polyteia-dbt-utils.
                    Example:
                        dbt_project.yml:
                            ...
                            macro-paths: ["dbt_packages/polyteia_dbt_utils/macros", "macros"]
                            ...

                        packages.yml:
                            packages:
                              - git: "https://{{env_var('DBT_ENV_SECRET_GITLAB_REPO_CREDENTIALS')}}@gitlab.com/polyteia/data-pipelines/polyteia-dbt-utils.git"
                                revision: main
                            ...
                """
            )
        )
        sys.exit(1)
    print("Standard schema use is guaranteed.")


def check_packages():
    if not are_polyteia_packages_on_main():
        print(
            textwrap.dedent(
                """\
                Uniformity of our standard solutions is not guaranteed with the current dbt setup.
                You need to switch all polyteia packages to revision main.
                    Example:
                        packages.yml:
                            packages:
                              - git: "https://{{env_var('DBT_ENV_SECRET_GITLAB_REPO_CREDENTIALS')}}@gitlab.com/polyteia/data-pipelines/polyteia-dbt-utils.git"
                                revision: main
                            ...
                """
            )
        )
        sys.exit(1)
    print("All polyteia packages are on revision main.")


def seed_table_uniqueness_is_guaranteed(dag_id: str) -> bool:
    if has_no_seed_files():
        return True
    if all_seed_file_names_start_with(f"{dag_id}_"):
        return True
    if (
        is_bulk_project()
        and package_name_matches('^\\{\\{ *var\\("dag_id", *"[^"]*"\\) *\\}\\}$')
        and polyteia_macros_are_available()
    ):
        return True
    if is_trino_project() and package_name_is(dag_id) and polyteia_macros_are_available():
        return True
    if package_name_is(dag_id) and polyteia_macros_are_available() and automatic_prefixing_is_enabled_for_seeds():
        return True
    return False


def model_table_uniqueness_is_guaranteed(dag_id: str) -> bool:
    if all_model_file_names_start_with(f"{dag_id}_"):
        print(f"All model file names start with {dag_id}_")
        return True
    if (
        is_bulk_project()
        and package_name_matches('^\\{\\{ *var\\("dag_id", *"[^"]*"\\) *\\}\\}$')
        and polyteia_macros_are_available()
    ):
        return True
    if is_trino_project() and package_name_is(dag_id) and polyteia_macros_are_available():
        return True
    if package_name_is(dag_id) and polyteia_macros_are_available() and automatic_prefixing_is_enabled_for_models():
        print(f"dbt package names is {dag_id} and automatic prefixing is enabled for models.")
        return True
    return False


def custom_schemas_are_guaranteed():
    return polyteia_macros_are_available()


def has_no_seed_files():
    seed_paths = get_project_yaml().get("seed-paths")
    if not seed_paths:
        return True
    seed_files = [
        path for seed_path in seed_paths for path in glob(f"dbt/{seed_path}/**", recursive=True) if os.path.isfile(path)
    ]
    return not seed_files


def are_polyteia_packages_on_main():
    packages = get_packages_yaml().get("packages", [])
    pattern = re.compile(r"https:.*/polyteia/.*\.git", re.IGNORECASE)

    for package in packages:
        if "git" in package.keys():
            if re.match(pattern, package.get("git")) and package.get("revision") != "main":
                return False

    return True


def polyteia_macros_are_available():
    if "dbt_packages/polyteia_dbt_utils/macros" not in get_project_yaml().get("macro-paths", []):
        return False

    macro_repo_url = "https://{{env_var('DBT_ENV_SECRET_GITLAB_REPO_CREDENTIALS')}}@gitlab.com/polyteia/data-pipelines/polyteia-dbt-utils.git"

    packages = get_packages_yaml().get("packages", [])

    if any(package.get("git") == macro_repo_url and package.get("revision") == "main" for package in packages):
        return True
    else:
        False


def all_seed_file_names_start_with(prefix: str) -> bool:
    return all(
        filename.startswith(prefix)
        for seed_path in get_project_yaml()["seed-paths"]
        for filename in os.listdir(f"dbt/{seed_path}")
    )


def all_model_file_names_start_with(prefix: str) -> bool:
    return all(
        filename.startswith(prefix)
        for path in glob("dbt/models/**/*.sql", recursive=True)
        if (filename := os.path.basename(path)) not in ("properties.yml", "properties.yaml")
    )


def package_name_is(dag_id: str) -> bool:
    return get_project_yaml()["name"] == dag_id


def package_name_matches(dag_id: str) -> bool:
    pattern = re.compile(dag_id)
    return pattern.match(get_project_yaml()["name"])


def automatic_prefixing_is_enabled_for_seeds() -> bool:
    return "prefix_table_names" in get_project_yaml()["seeds"]["+tags"]


def automatic_prefixing_is_enabled_for_models() -> bool:
    model_config = next(iter(get_project_yaml()["models"].values()))
    return "prefix_table_names" in model_config["+tags"]


def get_project_yaml():
    with open("dbt/dbt_project.yml") as f:
        return yaml.safe_load(f)


def get_packages_yaml():
    with open("dbt/packages.yml") as f:
        return yaml.safe_load(f)


def is_bulk_project() -> bool:
    return os.path.isdir("bulk_configs")


def is_trino_project() -> bool:
    return get_project_yaml().get("profile") == "trino"


if __name__ == "__main__":
    main(*sys.argv[1:])
