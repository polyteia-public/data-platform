#!/bin/bash

if [[ -f "/src/requirements.txt" && -n "${EXTRA_INDEX_URL}" ]]; then
    pip install -r /src/requirements.txt --extra-index-url="$EXTRA_INDEX_URL"
elif [[ -f "/src/requirements.txt" ]]; then
    pip install -r /src/requirements.txt
fi

exec "$@"
